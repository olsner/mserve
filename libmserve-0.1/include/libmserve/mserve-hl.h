/*
 * This file is part of libmserve.
 * 
 * libmserve is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * libmserve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with libmserve; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _libmserve_mserve_hl_H
#define _libmserve_mserve_hl_H

#include <libmserve/mserve.h>

/**
	The complete definition of this structure should not be included
	in applications. All data in the struct is private and should only 
	be accessed through	API fuctions
*/
struct mserve_hl;
typedef struct mserve_hl mserve_hl;

typedef enum
{
	STATUS_TYPE_ALL=-1,
	STATUS_TYPE_PLAYBACK,
	STATUS_TYPE_URL,
	STATUS_TYPE_QN,
	STATUS_TYPE_MEDIA_LENGTH,
	STATUS_TYPE_LAST,
	
} StatusUpdateType;

/**
	All pointers handed to callbacks are allocated and freed by the caller.
*/
typedef void (*PlaybackUpdateCallback)(mserve_hl *, void *, const Status0 *);
typedef void (*MediaLengthUpdateCallback)(mserve_hl *, void *, const Status3 *);
typedef void (*URLUpdateCallback)(mserve_hl *, void *, const char *);
typedef void (*QueueNumberUpdateCallback)(mserve_hl *, void *, uint32 qn);
typedef void (*MediaInfoUpdateCallback)(mserve_hl *, void *, const char *key, const char *value);

BEGIN_DECLS

extern mserve_hl *mserve_hl_init();
extern void mserve_hl_set_client_info(mserve_hl *, const char *name, uint32 version);
/**
	Set the client library verbosity. true (non-zero) means verbose, 
	false (zero) means quiet	
*/
extern void mserve_hl_set_verbosity(mserve_hl *, int verbosity);
extern mserve_error mserve_hl_connect(mserve_hl *, const char *host, int port);
extern void mserve_hl_disconnect(mserve_hl *);
/**
	Attempt to restore connection after disconnection. Using this instead of
	mserve_hl_connect enables data that is still valid to be cached
*/
//extern mserve_error mserve_hl_reconnect(mserve_hl *);
extern void mserve_hl_destroy(mserve_hl *);

/**
	Check for mServe network events and call the callbacks.
*/
extern mserve_error mserve_hl_run_iter(mserve_hl *);
/**
	Loops, checking for network events and calling callbacks, until an error
	occurs.
	
	Returns:
		MSERVE_E_PIPE
*/
extern mserve_error mserve_hl_run_loop(mserve_hl *);

extern void mserve_hl_set_playback_callback(mserve_hl *, PlaybackUpdateCallback);
extern void mserve_hl_set_media_length_callback(mserve_hl *, MediaLengthUpdateCallback);
extern void mserve_hl_set_url_callback(mserve_hl *, URLUpdateCallback);
extern void mserve_hl_set_queue_number_callback(mserve_hl *, QueueNumberUpdateCallback);
extern void mserve_hl_set_media_info_callback(mserve_hl *, MediaInfoUpdateCallback);
extern void mserve_hl_set_callback_data(mserve_hl *, void *data);

/**
	The interval argument generally means:
		> 0: An interval of milliseconds between updates
		  0: No updates at all
		 -1: Updated as needed (upon change - depends on the update type)
	
	Intervals are sent in 16-bits over the network, so the maximum interval is
	65535. MSERVE_E_INVALID should be returned for such intervals.
	
	The default interval is undefined - callbacks may be called at any time from
	the invocation of mserve_hl_connect/reconnect.
*/

/**	
	Returns:
		MSERVE_SUCCESS
		MSERVE_E_PIPE
		MSERVE_E_INVALID
			The interval argument is invalid
*/
extern mserve_error mserve_hl_set_status_updates(mserve_hl *, StatusUpdateType type, int interval);
/**
	Request the server to send the specified status message. When the message
	is received the callback as specified by mserve_hl_set_*_callback is called.
	
	Returns:
		MSERVE_SUCCESS
		MSERVE_E_PIPE
*/
extern mserve_error mserve_hl_request_status_update(mserve_hl *, StatusUpdateType type);

/**
	
	
	Errors:
		MSERVE_SUCCESS
		MSERVE_E_PIPE
*/
//extern mserve_error mserve_hl_request_config_update(mserve_hl *);
/**
	
	Returns:
		MSERVE_SUCCESS
		MSERVE_E_PIPE
*/
//extern mserve_error mserve_hl_request_config(mserve_hl *, const char *id);

/**
	NOTE: Returned strings are duplicated (strdup) and must be freed with
	free() by the application.
*/
extern mserve_error mserve_hl_queue_url(mserve_hl *, const char *);
/**
	The high-level api stores the queue numbers for an undefined
	number of recently queued files.
*/
extern mserve_error mserve_hl_get_url_qn(mserve_hl *, const char *, uint32 **qn);
extern mserve_error mserve_hl_get_url_from_qn(mserve_hl *, uint32 qn, char **url);

extern mserve_error mserve_hl_skip(mserve_hl *);
extern mserve_error mserve_hl_stop(mserve_hl *);
extern mserve_error mserve_hl_pause(mserve_hl *);
extern mserve_error mserve_hl_resume(mserve_hl *);

/**
	Pos is milliseconds
*/
extern mserve_error mserve_hl_seek(mserve_hl *, uint pos);

extern void mserve_hl_lowlevel_lock(mserve_hl *);
extern void mserve_hl_lowlevel_unlock(mserve_hl *);
/**
	Use with caution - high-level state may be bound to the low-level state
	in mysterious ways.
	
	Use if, and only if:
		(A) You have to
	and (B) You are sure that you know what you are doing
	
	You must call the mserve_hl_lowlevel_lock before performing operations on
	the lowlevel context, and call ..._unlock when done with it.
*/
extern mserve *mserve_hl_get_lowlevel_context(mserve_hl *);

END_DECLS

#endif
