/*
 * This file is part of libmserve.
 * 
 * libmserve is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * libmserve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with libmserve; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <libmserve/mserve.h>
#include "internal.h"

#include <linux/sockios.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/select.h>
#include <sys/ioctl.h>

#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

void *mserve_client_thread(void *);
void mserve_msg_reader_callback(msg_reader_ctx *, uint, uint, void *);

void mserve::clear_queue(deque <mserve_message> &q)
{
	deque <mserve_message>::iterator it=q.begin();
	while (it != q.end())
	{
		mserve_free_message(&*it);
		++it;
	}
	q.clear();
}

mserve::mserve():
	//in(),
	//out(),
	fd(0),
	ctl_fd((int[]){0, 0}),
	threadStarted(false),
	writemsg(NULL),
	writepos(0)
{
	pthread_mutex_init(&in_mutex, NULL);
	pthread_mutex_init(&out_mutex, NULL);
	pthread_mutex_init(&cond_mutex, NULL);
	pthread_cond_init(&cond, NULL);
	msg_reader_init(&msg_reader, mserve_msg_reader_callback, this);
}

mserve::~mserve()
{
	if (fd)
		disconnect();
	clear_in();
	msg_reader_destroy(&msg_reader);
}

mserve_error mserve::connect(const char *host, int port)
{
	int res;
	sockaddr_in addr;
	hostent *he;
	
	res=pipe(ctl_fd);
	if (res != 0)
		return MSERVE_E_PIPE;
	
	fd=socket(PF_INET, SOCK_STREAM, 0);
	if (fd == -1)
	{
		fd=0;
		return MSERVE_E_PIPE;
	}
	
	addr.sin_family=AF_INET;
	if (port != -1)
		addr.sin_port=htons(port);
	else
		addr.sin_port=htons(4097);
	he=gethostbyname(host);
	if (he == NULL)
	{
		close(fd);
		fd=0;
		return MSERVE_E_NOHOST;
	}
	addr.sin_addr=*(struct in_addr *)he->h_addr;
	
	res=::connect(fd, (sockaddr *)&addr, sizeof(addr));
	if (res == -1)
	{
		close(fd);
		fd=0;
		return MSERVE_E_PIPE;
	}
	
	long flags=fcntl(fd, F_GETFL);
	flags|=O_NONBLOCK;
	fcntl(fd, F_SETFL, flags);
	
	pthread_create(&thread, NULL, mserve_client_thread, this);
	pthread_detach(thread);
	threadStarted=true;
	
	return MSERVE_SUCCESS;
}

void mserve::disconnect_socket()
{
	if (fd)
	{
		long fl=fcntl(fd, F_GETFL);
		fcntl(fd, F_SETFL, fl&~O_NONBLOCK);
		while (shutdown(fd, SHUT_RDWR) != 0 && errno != EBADF)
			perror("pending socket error on shutdown");
		while (close(fd) != 0 && errno != EBADF)
			perror("pending socket error on close");
	}
}

int mserve::disconnect()
{
	if (threadStarted)
	{
		pthread_cancel(thread);
		::write(ctl_fd[1], (char[]){0}, 1);
		threadStarted=false;
	}
	disconnect_socket();
	clear_out();
	return (int)in.size();
}

void mserve::client_thread()
{
	uint8 dummy[1];
	
	while (fd)
	{
		fd_set read;
		fd_set write;
		timeval timeout;
		
		FD_ZERO(&read);
		FD_ZERO(&write);
		
		FD_SET(fd, &read);
		FD_SET(ctl_fd[0], &read);
		if (out.size() || writemsg)
		{
			FD_SET(fd, &write);
		}
		
		timeout.tv_sec=0;
		timeout.tv_usec=10000;
		int res=select((fd >? ctl_fd[0])+1, &read, &write, NULL, &timeout);
		
		if (res > 0)
		{
			if (FD_ISSET(fd, &read))
				read_cb();
			if (FD_ISSET(fd, &write))
				write_cb();
			
			if (FD_ISSET(ctl_fd[0], &read))
				::read(ctl_fd[0], dummy, 1);
		}
		pthread_testcancel();
	}
}

void mserve::msg_reader_callback(uint type, uint len, void *data)
{
	lock_in();
	mserve_message tmp={type, len, data};
	mserve_message msg;
	mserve_copy_message(&msg, &tmp);
	in.push_back(msg);
	unlock_in();
	cond_signal();
}

void mserve_msg_reader_callback(msg_reader_ctx *msg_reader, uint type, uint len, void *data)
{
	mserve *ctx=(mserve *)msg_reader->userdata;
	ctx->msg_reader_callback(type, len, data);
}

void mserve::read_cb()
{
	int res;
	int nread=0;
	uint toread;
	
	errno=0;
	res=ioctl(fd, SIOCINQ, &nread);
	if (res!=0 || (res==0 && nread==0))
	{
		disconnect_socket();
		clear_out();
		cond_signal();
		pthread_exit(PTHREAD_CANCELED);
		return;
	}
	
	void *buf=alloca(nread);
	toread=nread;
	res=read(fd, buf, nread);
	if (res > 0)
		msg_reader_indata(&msg_reader, buf, res);
}

void mserve::write_cb()
{
	int res=0;
	int cnt=0;
	while (res == cnt)
	{
		if (writemsg && writepos==writemsg->length+4)
		{
			//printf("Finished writing message: type 0x%04x length %u\n", writemsg->type, writemsg->length);
			lock_out();
			out.pop_front();
			writemsg=NULL;
			unlock_out();
			// For mserve_wait_flush
			cond_signal();
		}
		if (!writemsg)
		{
			lock_out();
			if (out.size())
				writemsg=&out.front();
			else
				writemsg=NULL;
			unlock_out();
			writepos=0;
		}
		if (!writemsg) return;
		//printf("Before; writepos is %u, type is 0x%04x length is %u\n", writepos, writemsg->type, writemsg->length);
		if (writepos < 4)
		{
			uint8 hdr[4];
			*(uint16 *)(hdr)=writemsg->type;
			*(uint16 *)(hdr+2)=writemsg->length;
			res=write(fd, hdr+writepos, cnt=4-writepos);
		}
		else
			res=write(fd, ((char *)writemsg->data)+writepos-4, cnt=writemsg->length);
		if (res != -1)
			writepos+=res;
		else
			return;
		//printf("After; writepos is %u, length is %u, res %d\n", writepos, writemsg->length, res);
	}
}

bool mserve::check_socket()
{
	int nread=0;
	int res=ioctl(fd, SIOCINQ, &nread);
	return (res == 0);
}

mserve_error mserve::send_message(const mserve_message *msg)
{
	if (!check_socket())
		return MSERVE_E_PIPE;
	mserve_message tmp_msg;
	mserve_copy_message(&tmp_msg, msg);
	lock_out();
	out.push_back(tmp_msg);
	unlock_out();
	char dummy[1];
	write(ctl_fd[1], dummy, 1);
	return MSERVE_SUCCESS;
}

mserve_error mserve::has_message()
{
	lock_in();
	int n=in.size();
	unlock_in();
	if (n)
		return MSERVE_SUCCESS;
	if (check_socket())
		return MSERVE_E_EMPTYQUEUE;
	else
		return MSERVE_E_PIPE;
}

mserve_error mserve::try_pop_message(mserve_message *dst)
{
	if (!check_socket())
		return MSERVE_E_PIPE;
	lock_in();
	if (in.size())
	{
		mserve_message &src=in.front();
		dst->type=src.type;
		dst->length=src.length;
		dst->data=src.data;
		in.pop_front();
		unlock_in();
		return MSERVE_SUCCESS;
	}
	else
	{
		unlock_in();
		return MSERVE_E_EMPTYQUEUE;
	}
}

mserve_error mserve::wait_pop_message(mserve_message *dst)
{
	if (!check_socket())
		return MSERVE_E_PIPE;
	lock_in();
	while (in.size()==0)
	{
		unlock_in();
		if (!check_socket())
			return MSERVE_E_PIPE;
		cond_wait();
		lock_in();
	}
	mserve_message &src=in.front();
	dst->type=src.type;
	dst->length=src.length;
	dst->data=src.data;
	in.pop_front();
	unlock_in();
	return MSERVE_SUCCESS;
}

mserve_error mserve::wait_flush()
{
	lock_out();
	while (out.size() || writemsg)
	{
		unlock_out();
		if (!check_socket())
			return MSERVE_E_PIPE;
		cond_wait();
		lock_out();
	}
	unlock_out();
	return MSERVE_SUCCESS;
}

void *mserve_client_thread(void *data)
{
	mserve *ctx=(mserve *)data;
	ctx->client_thread();
	return NULL;
}

mserve *mserve_init()
{
	mserve *ret=(mserve *)malloc(sizeof(mserve));
	new (ret) mserve();
	return ret;
}

void mserve_destroy(mserve *ctx)
{
	ctx->~mserve();
	free(ctx);
}

mserve_error mserve_connect(mserve *ctx, const char *host, int port)
{
	return ctx->connect(host, port);
}

int mserve_disconnect(mserve *ctx)
{
	return ctx->disconnect();
}

mserve_error mserve_send_message(mserve *ctx, const mserve_message *msg)
{
	return ctx->send_message(msg);
}

mserve_error mserve_wait_send_message(mserve *ctx, const mserve_message *msg)
{
	return ctx->send_message(msg);
}

mserve_error mserve_has_message(mserve *ctx)
{
	return ctx->has_message();
}

mserve_error mserve_try_pop_message(mserve *ctx, mserve_message *dst)
{
	return ctx->try_pop_message(dst);
}

mserve_error mserve_wait_pop_message(mserve *ctx, mserve_message *dst)
{
	return ctx->wait_pop_message(dst);
}

mserve_error mserve_wait_flush(mserve *ctx)
{
	return ctx->wait_flush();
}

void mserve_free_message(mserve_message *msg)
{
	if (msg->data)
		free(msg->data);
	msg->data=NULL;
	msg->length=0;
}

void mserve_copy_message(mserve_message *dst, const mserve_message *src)
{
	dst->type=src->type;
	dst->length=src->length;
	if (dst->length)
	{
		dst->data=malloc(dst->length);
		memcpy(dst->data, src->data, dst->length);
	}
	else
		dst->data=NULL;
}

void mserve_handle_VersionInfo(mserve *ctx, const mserve_message *msg, const VersionInfo *client_info, int verbose)
{
	mserve_message tmp;
	if (verbose)
	{
		VersionInfo vi;
		msg_parse_VersionInfo(&vi, msg);
		printf("VersionInfo: \"%s\" 0x%08x (PV 0x%08x)\n", vi.SoftwareName, vi.SoftwareVersion, vi.ProtocolVersion);
		msg_free_VersionInfo(&vi);
	}
	msg_create_VersionInfo(&tmp, client_info);
	mserve_send_message(ctx, &tmp);
	mserve_free_message(&tmp);
}

void mserve_register_extensions(mserve *ctx, const UseExtensions *ue, int verbose)
{
	ctx->register_extensions(ue, verbose);
}

int mserve_get_extension_id(mserve *ctx, const char *extname)
{
	return ctx->get_extension_id(extname);
}

void mserve::register_extensions(const UseExtensions *ue, int verbose)
{
	lock_extname_map();
	for (uint i=0;i<ue->ExtensionsCount;i++)
	{
		UseExtensionsElement *uee=&ue->Extensions[i];
		if (verbose)
			printf("mserve_register_extensions: \"%-16s\", version %d. ID: 0x%04x\n", uee->ExtensionName, uee->ExtensionVersion, uee->ExtensionID);
		extname_map.insert(pair<extname,int>(uee->ExtensionName, uee->ExtensionID));
	}
	unlock_extname_map();
}

int mserve::get_extension_id(const char *extname)
{
	lock_extname_map();
	int ret=extname_map[extname];
	unlock_extname_map();
	printf("Looked up \"%-16s\", id was 0x%04x\n", extname, ret);
	return ret;
}

void mserve_create_extended_message(mserve *ctx, mserve_message *dst, const char *extname, const message *src)
{
	ExtendedMessage em={
		mserve_get_extension_id(ctx, extname),
		src->type,
		src->data,
		src->length
	};
	msg_create_ExtendedMessage(dst, &em);
}

void mserve_create_extended_message_id(mserve *ctx, mserve_message *dst, int extid, const message *src)
{
	ExtendedMessage em={
		extid,
		src->type,
		src->data,
		src->length
	};
	msg_create_ExtendedMessage(dst, &em);
}

void mserve_extract_extended_message(mserve *ctx, message *dst, const ExtendedMessage *src)
{
	dst->type=src->ExtendedType;
	dst->data=src->ExtendedData;
	dst->length=src->ExtendedDataLength;
}
