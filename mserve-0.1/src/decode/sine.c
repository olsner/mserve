/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include "sine.h"
#include "out/buffer.h"

#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int sine_supports(input_ctx *ctx)
{
	if (strncasecmp(ctx->url, "sine://", 7)==0)
		return 1;
	return 0;
}

int sine_init(decode_ctx *ctx)
{
	ctx->internal=(void *)atoi(ctx->input.url+7);
	
	return 0;
}

int sine_decode_run(decode_ctx *ctx)
{
	short buffer[11025];
	int freq=(int)ctx->internal;
	float w=M_PI*freq/22050.0;
	float f;
	int n,i,res;
	
	ctx->output->set_format(44100, SFMT_16LE_S, 2);

	for (n=0;n<20;n++)
	{
		for (i=0;i<11025;i++)
		{
			f=sin(w*(i+n*11025));
			buffer[i]=(int)(f*32767);
		}
		while (out_buffer_available() < 22050)
		{
			decode_ctx_wait_event(ctx);
		}
		res=out_buffer_push((char *)buffer, 22050);
		if (res < 22050)
		{
			printf(" I got %d res, I wanted to write %d\n", res, 22050);
		}
	}
	return 0;
}

void sine_deinit(decode_ctx *dec_ctx)
{}

int sine_seek(decode_ctx *dec_ctx, uint millipos)
{
	return -1;
}
