/*
 * This file is part of libmserve.
 * 
 * libmserve is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * libmserve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with libmserve; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <stdlib.h>
#include <string.h>

#include <libmserve/common.h>
#include <libmserve/msg_reader.h>

void msg_reader_init(msg_reader_ctx *reader, msg_reader_cb callback, void *userdata)
{
	memset(reader, 0, sizeof(msg_reader_ctx));
	reader->msg_cb=callback;
	reader->userdata=userdata;
}

void msg_reader_destroy(msg_reader_ctx *reader)
{
	if (reader->msgbufferallocated)
	{
		free(reader->msgbuffer);
		reader->msgbuffer=NULL;
		reader->msgbufferallocated=0;
	}
}

void msg_reader_indata(msg_reader_ctx *reader, void *data, uint count)
{
	if (!reader->msgbufferallocated)
	{
		reader->msgbuffer=(uint8 *)malloc(256);
		reader->msgbufferallocated=256;
	}

	char *buf=(char *)data;

	uint pos=0;
	while (pos < count)
	{
		if (!reader->readingdata)
		{
			uint toread=4-reader->readpos;
			if (toread > count-pos) toread=count-pos;
			memcpy(reader->headerbuf+reader->readpos, buf+pos, toread);
			pos += toread;
			reader->readpos += toread;
			if (reader->readpos == 4)
			{
				reader->readpos=0;
				reader->readingdata=1;
			}
			uint16 length=*(uint16 *)(reader->headerbuf+2);
			if (length == 0)
			{
				reader->readpos=0;
				reader->readingdata=0;
				reader->msg_cb(reader, *(uint16 *)(reader->headerbuf), 0, reader->msgbuffer);
			}
			if (reader->msgbufferallocated < length)
			{
				if (reader->msgbufferallocated)
					free(reader->msgbuffer);
				reader->msgbuffer=(uint8 *)malloc(length);
				reader->msgbufferallocated=length;
			}
		}
		else
		{
			uint16 type=*(uint16 *)(reader->headerbuf);
			uint16 length=*(uint16 *)(reader->headerbuf+2);
			uint toread=length-reader->readpos;
			if (toread > count-pos) toread=count-pos;
			memcpy(reader->msgbuffer+reader->readpos, buf+pos, toread);
			pos+=toread;
			reader->readpos+=toread;
			if (reader->readpos == length)
			{
				reader->readpos=0;
				reader->readingdata=0;
				reader->msg_cb(reader, type, length, reader->msgbuffer);
			}
		}
	}
}
