/* THIS IS A GENERATED FILE */
#include "filebrowser_msg.h"
#include <stdlib.h>
#include <string.h>
#define SWAB64(_a) (_a)
#define SWAB32(_a) (_a)
#define SWAB16(_a) (_a)
#define parse_int(_in, _dst, _pos, _bits, _type) do {req_bytes(_in, _pos, _bits/8);*_dst=SWAB##_bits(*(_type *)(((char *)_in->data)+*_pos));*_pos+=_bits/8;} while (0)
#define create_int(_out, _src, _pos, _bits, _type) do {_type *dst=(_type *)(((char *)_out->data)+*_pos);*dst=SWAB##_bits(_src);*_pos+=_bits/8;} while (0)
#define SWAB8(_a) (_a)
#define parse_uint64(_in, _dst, _pos) parse_int(_in, _dst, _pos, 64, uint64)
#define create_uint64(_out, _src, _pos) create_int(_out, _src, _pos, 64, uint64)
#define parse_uint32(_in, _dst, _pos) parse_int(_in, _dst, _pos, 32, uint32)
#define create_uint32(_out, _src, _pos) create_int(_out, _src, _pos, 32, uint32)
#define parse_uint16(_in, _dst, _pos) parse_int(_in, _dst, _pos, 16, uint16)
#define create_uint16(_out, _src, _pos) create_int(_out, _src, _pos, 16, uint16)
#define parse_uint8(_in, _dst, _pos) parse_int(_in, _dst, _pos, 8, uint8)
#define create_uint8(_out, _src, _pos) create_int(_out, _src, _pos, 8, uint8)
#define parse_error() do { return -1; } while (0)
#define req_bytes(_in, _pos, _bytes) do {if (*(_pos)+_bytes > (_in)->length)parse_error();} while (0)
#define dryAlloc(_bytes) allocNeeded+=_bytes
#define alloc(_bytes) ({void *_ret=allocBuffer; uint _rbytes=((uint)(_bytes)); allocBuffer=((char*)allocBuffer)+_rbytes; allocUsed+=_rbytes; _ret;})

void fb_msg_free_GetWD(GetWD* msg)
{
	free(msg->allocBuffer);
}
void fb_msg_create_GetWD(message *out) {
	out->length=0;
	out->type=FileBrowser_MT_GetWD;
	out->data=NULL;
}
int fb_msg_parse_SetWD(SetWD *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	{char *p=((char *)in->data)+pos;char *begin=p;char *end=((char *)in->data)+in->length;while (*p && p<end) p++;if (p==end && *p) parse_error();pos+=p-begin+(*p?0:1);dryAlloc((p-begin+1));}
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	{char *p=((char *)in->data)+pos;
	char *begin=p;
	char *end=((char *)in->data)+in->length;
	while (*p && p < end) p++;
	if (p == end && *p) parse_error();
	out->Directory=(char *)alloc(p-begin+1);
	memcpy(out->Directory, begin, p-begin);
	out->Directory[p-begin]=0;
	pos+=p-begin+1;}
	out->allocUsed=allocUsed;
	return 0;
}
void fb_msg_free_SetWD(SetWD* msg)
{
	free(msg->allocBuffer);
}
void fb_msg_create_SetWD(message *out, const SetWD* msg)
{
	uint pos=0;	pos+=strlen(msg->Directory)+1;
	out->data=malloc(pos);
	out->length=pos;
	out->type=FileBrowser_MT_SetWD;
	pos=0;
	strcpy(((char *)out->data)+pos, msg->Directory);pos+=strlen(msg->Directory)+1;
}
int fb_msg_parse_Iterate(Iterate *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	req_bytes(in, &pos, sizeof(uint16)); pos+=sizeof(uint16);
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	parse_uint16(in, &out->TypeMask, &pos);
	out->allocUsed=allocUsed;
	return 0;
}
void fb_msg_free_Iterate(Iterate* msg)
{
	free(msg->allocBuffer);
}
void fb_msg_create_Iterate(message *out, const Iterate* msg)
{
	uint pos=0;	pos+=sizeof(uint16);
	out->data=malloc(pos);
	out->length=pos;
	out->type=FileBrowser_MT_Iterate;
	pos=0;
	create_uint16(out, msg->TypeMask, &pos);
}
int fb_msg_parse_FileList(FileList *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	{char *p=((char *)in->data)+pos;char *begin=p;char *end=((char *)in->data)+in->length;while (*p && p<end) p++;if (p==end && *p) parse_error();pos+=p-begin+(*p?0:1);dryAlloc((p-begin+1));}
	
	FileListEntry tmp_Files;
	out->FilesCount=0;
	while (pos < in->length) {
		out->FilesCount++;
		FileListEntry *out;out=&tmp_Files;
		dryAlloc(sizeof(tmp_Files));
		req_bytes(in, &pos, sizeof(uint8)); pos+=sizeof(uint8);
		{char *p=((char *)in->data)+pos;char *begin=p;char *end=((char *)in->data)+in->length;while (*p && p<end) p++;pos+=p-begin+(*p?0:1);dryAlloc((p-begin+1));}
	}
	pos=in->length;
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	{char *p=((char *)in->data)+pos;
	char *begin=p;
	char *end=((char *)in->data)+in->length;
	while (*p && p < end) p++;
	if (p == end && *p) parse_error();
	out->Directory=(char *)alloc(p-begin+1);
	memcpy(out->Directory, begin, p-begin);
	out->Directory[p-begin]=0;
	pos+=p-begin+1;}
	
	out->Files=(FileListEntry*)alloc((out->FilesCount)*sizeof(FileListEntry));
	{uint i; for (i=0;i<out->FilesCount;i++) {
		FileListEntry *o_out=out->Files;
		FileListEntry *out=&o_out[i];
		parse_uint8(in, &out->Type, &pos);
		{char *p=((char *)in->data)+pos;
	char *begin=p;
	char *end=((char *)in->data)+in->length;
	while (*p && p < end) p++;
	out->Name=(char *)alloc(p-begin+1);
	memcpy(out->Name, begin, p-begin);
	out->Name[p-begin]=0;
	pos+=p-begin+1;}
	}}
	out->allocUsed=allocUsed;
	return 0;
}
void fb_msg_free_FileList(FileList* msg)
{
	free(msg->allocBuffer);
}
void fb_msg_create_FileList(message *out, const FileList* msg)
{
	uint pos=0;	pos+=strlen(msg->Directory)+1;
	{uint i;
	for (i=0;i<msg->FilesCount;i++) {
		typeof(msg) o_msg=msg;
		FileListEntry *msg; msg=&o_msg->Files[i];
		pos+=sizeof(uint8);
		pos+=strlen(msg->Name)+1;
	}}
	out->data=malloc(pos);
	out->length=pos;
	out->type=FileBrowser_MT_FileList;
	pos=0;
	strcpy(((char *)out->data)+pos, msg->Directory);pos+=strlen(msg->Directory)+1;
	{uint i;
	for (i=0;i<msg->FilesCount;i++) {
		typeof(msg) o_msg=msg;
		FileListEntry *msg; msg=&o_msg->Files[i];
		create_uint8(out, msg->Type, &pos);
		strcpy(((char *)out->data)+pos, msg->Name);pos+=strlen(msg->Name)+1;
	}}
}
int fb_msg_parse_IterateDir(IterateDir *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	req_bytes(in, &pos, sizeof(uint16)); pos+=sizeof(uint16);
	{char *p=((char *)in->data)+pos;char *begin=p;char *end=((char *)in->data)+in->length;while (*p && p<end) p++;pos+=p-begin+(*p?0:1);dryAlloc((p-begin+1));}
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	parse_uint16(in, &out->TypeMask, &pos);
	{char *p=((char *)in->data)+pos;
	char *begin=p;
	char *end=((char *)in->data)+in->length;
	while (*p && p < end) p++;
	out->Directory=(char *)alloc(p-begin+1);
	memcpy(out->Directory, begin, p-begin);
	out->Directory[p-begin]=0;
	pos+=p-begin+1;}
	out->allocUsed=allocUsed;
	return 0;
}
void fb_msg_free_IterateDir(IterateDir* msg)
{
	free(msg->allocBuffer);
}
void fb_msg_create_IterateDir(message *out, const IterateDir* msg)
{
	uint pos=0;	pos+=sizeof(uint16);
	pos+=strlen(msg->Directory)+1;
	out->data=malloc(pos);
	out->length=pos;
	out->type=FileBrowser_MT_IterateDir;
	pos=0;
	create_uint16(out, msg->TypeMask, &pos);
	strcpy(((char *)out->data)+pos, msg->Directory);pos+=strlen(msg->Directory)+1;
}
