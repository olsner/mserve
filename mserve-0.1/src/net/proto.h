/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _mServe_network_proto_H
#define _mServe_network_proto_H

#include "client.h"
#include "common.h"
#include <libmserve/mserve.h>
#include <libmserve/attriblist.h>

/**
	A mask specifying the status messages to send when a client has completed
	the protocol version and extension negotiations.
*/
#define INITIAL_STATUS_MESSAGES (StatusMask(0)|StatusMask(1)|StatusMask(3))

#ifdef __cplusplus
extern "C" {
#endif

extern void proto_send_status_message(client_ctx *, int status_msg);
extern void proto_message_callback(client_ctx *, const mserve_message *);
extern void proto_send_VersionInfo(client_ctx*, const char *, uint32);
extern void broadcast_message(const mserve_message *);
extern void broadcast_status_msg(int id);
extern void proto_init_status_update(client_ctx *);
extern void proto_media_info_callback(attriblist *, void *data, const char *name, const char *value);
extern void proto_set_config_callback(attriblist *, void *data, const char *name, const char *value);

#ifdef __cplusplus
}
#endif

#endif
