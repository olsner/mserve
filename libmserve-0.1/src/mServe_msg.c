/* THIS IS A GENERATED FILE */
#include "../include/libmserve/mServe_msg.h"
#include <stdlib.h>
#include <string.h>
#define SWAB64(_a) (_a)
#define SWAB32(_a) (_a)
#define SWAB16(_a) (_a)
#define parse_int(_in, _dst, _pos, _bits, _type) do {req_bytes(_in, _pos, _bits/8);*_dst=SWAB##_bits(*(_type *)(((char *)_in->data)+*_pos));*_pos+=_bits/8;} while (0)
#define create_int(_out, _src, _pos, _bits, _type) do {_type *dst=(_type *)(((char *)_out->data)+*_pos);*dst=SWAB##_bits(_src);*_pos+=_bits/8;} while (0)
#define SWAB8(_a) (_a)
#define parse_uint64(_in, _dst, _pos) parse_int(_in, _dst, _pos, 64, uint64)
#define create_uint64(_out, _src, _pos) create_int(_out, _src, _pos, 64, uint64)
#define parse_uint32(_in, _dst, _pos) parse_int(_in, _dst, _pos, 32, uint32)
#define create_uint32(_out, _src, _pos) create_int(_out, _src, _pos, 32, uint32)
#define parse_uint16(_in, _dst, _pos) parse_int(_in, _dst, _pos, 16, uint16)
#define create_uint16(_out, _src, _pos) create_int(_out, _src, _pos, 16, uint16)
#define parse_uint8(_in, _dst, _pos) parse_int(_in, _dst, _pos, 8, uint8)
#define create_uint8(_out, _src, _pos) create_int(_out, _src, _pos, 8, uint8)
#define parse_error() do { return -1; } while (0)
#define req_bytes(_in, _pos, _bytes) do {if (*(_pos)+_bytes > (_in)->length)parse_error();} while (0)
#define dryAlloc(_bytes) allocNeeded+=_bytes
#define alloc(_bytes) ({void *_ret=allocBuffer; uint _rbytes=((uint)(_bytes)); allocBuffer=((char*)allocBuffer)+_rbytes; allocUsed+=_rbytes; _ret;})
int msg_parse_VersionInfo(VersionInfo *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	{char *p=((char *)in->data)+pos;char *begin=p;char *end=((char *)in->data)+in->length;while (*p && p<end) p++;if (p==end && *p) parse_error();pos+=p-begin+(*p?0:1);dryAlloc((p-begin+1));}
	req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	{char *p=((char *)in->data)+pos;
	char *begin=p;
	char *end=((char *)in->data)+in->length;
	while (*p && p < end) p++;
	if (p == end && *p) parse_error();
	out->SoftwareName=(char *)alloc(p-begin+1);
	memcpy(out->SoftwareName, begin, p-begin);
	out->SoftwareName[p-begin]=0;
	pos+=p-begin+1;}
	parse_uint32(in, &out->SoftwareVersion, &pos);
	parse_uint32(in, &out->ProtocolVersion, &pos);
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_VersionInfo(VersionInfo* msg)
{
	free(msg->allocBuffer);
}
void msg_create_VersionInfo(message *out, const VersionInfo* msg)
{
	uint pos=0;	pos+=strlen(msg->SoftwareName)+1;
	pos+=sizeof(uint32);
	pos+=sizeof(uint32);
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_VersionInfo;
	pos=0;
	strcpy(((char *)out->data)+pos, msg->SoftwareName);pos+=strlen(msg->SoftwareName)+1;
	create_uint32(out, msg->SoftwareVersion, &pos);
	create_uint32(out, msg->ProtocolVersion, &pos);
}
int msg_parse_UseProtocolVersion(UseProtocolVersion *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	parse_uint32(in, &out->ProtocolVersion, &pos);
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_UseProtocolVersion(UseProtocolVersion* msg)
{
	free(msg->allocBuffer);
}
void msg_create_UseProtocolVersion(message *out, const UseProtocolVersion* msg)
{
	uint pos=0;	pos+=sizeof(uint32);
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_UseProtocolVersion;
	pos=0;
	create_uint32(out, msg->ProtocolVersion, &pos);
}
int msg_parse_ExtensionInfo(ExtensionInfo *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	
	ExtensionInfoElement tmp_Extensions;
	out->ExtensionsCount=0;
	while (pos < in->length) {
		out->ExtensionsCount++;
		ExtensionInfoElement *out;out=&tmp_Extensions;
		dryAlloc(sizeof(tmp_Extensions));
		req_bytes(in, &pos, 16); pos+=16;
		req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	}
	pos=in->length;
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	
	out->Extensions=(ExtensionInfoElement*)alloc((out->ExtensionsCount)*sizeof(ExtensionInfoElement));
	{uint i; for (i=0;i<out->ExtensionsCount;i++) {
		ExtensionInfoElement *o_out=out->Extensions;
		ExtensionInfoElement *out=&o_out[i];
		req_bytes(in, &pos, 16);
	memcpy(out->ExtensionName, ((char *)in->data)+pos, 16);
	pos+=16;
		parse_uint32(in, &out->ExtensionVersion, &pos);
	}}
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_ExtensionInfo(ExtensionInfo* msg)
{
	free(msg->allocBuffer);
}
void msg_create_ExtensionInfo(message *out, const ExtensionInfo* msg)
{
	uint pos=0;	{uint i;
	for (i=0;i<msg->ExtensionsCount;i++) {
		typeof(msg) o_msg=msg;
		ExtensionInfoElement *msg; msg=&o_msg->Extensions[i];
		pos+=16;
		pos+=sizeof(uint32);
	}}
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_ExtensionInfo;
	pos=0;
	{uint i;
	for (i=0;i<msg->ExtensionsCount;i++) {
		typeof(msg) o_msg=msg;
		ExtensionInfoElement *msg; msg=&o_msg->Extensions[i];
		memcpy(((char *)out->data)+pos, msg->ExtensionName, 16);pos+=16;
		create_uint32(out, msg->ExtensionVersion, &pos);
	}}
}
int msg_parse_UseExtensions(UseExtensions *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	
	UseExtensionsElement tmp_Extensions;
	out->ExtensionsCount=0;
	while (pos < in->length) {
		out->ExtensionsCount++;
		UseExtensionsElement *out;out=&tmp_Extensions;
		dryAlloc(sizeof(tmp_Extensions));
		req_bytes(in, &pos, 16); pos+=16;
		req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
		req_bytes(in, &pos, sizeof(uint16)); pos+=sizeof(uint16);
	}
	pos=in->length;
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	
	out->Extensions=(UseExtensionsElement*)alloc((out->ExtensionsCount)*sizeof(UseExtensionsElement));
	{uint i; for (i=0;i<out->ExtensionsCount;i++) {
		UseExtensionsElement *o_out=out->Extensions;
		UseExtensionsElement *out=&o_out[i];
		req_bytes(in, &pos, 16);
	memcpy(out->ExtensionName, ((char *)in->data)+pos, 16);
	pos+=16;
		parse_uint32(in, &out->ExtensionVersion, &pos);
		parse_uint16(in, &out->ExtensionID, &pos);
	}}
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_UseExtensions(UseExtensions* msg)
{
	free(msg->allocBuffer);
}
void msg_create_UseExtensions(message *out, const UseExtensions* msg)
{
	uint pos=0;	{uint i;
	for (i=0;i<msg->ExtensionsCount;i++) {
		typeof(msg) o_msg=msg;
		UseExtensionsElement *msg; msg=&o_msg->Extensions[i];
		pos+=16;
		pos+=sizeof(uint32);
		pos+=sizeof(uint16);
	}}
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_UseExtensions;
	pos=0;
	{uint i;
	for (i=0;i<msg->ExtensionsCount;i++) {
		typeof(msg) o_msg=msg;
		UseExtensionsElement *msg; msg=&o_msg->Extensions[i];
		memcpy(((char *)out->data)+pos, msg->ExtensionName, 16);pos+=16;
		create_uint32(out, msg->ExtensionVersion, &pos);
		create_uint16(out, msg->ExtensionID, &pos);
	}}
}
int msg_parse_SetStatusUpdates(SetStatusUpdates *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	req_bytes(in, &pos, sizeof(uint16)); pos+=sizeof(uint16);
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	parse_uint32(in, &out->StatusUpdateMask, &pos);
	parse_uint16(in, &out->Interval, &pos);
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_SetStatusUpdates(SetStatusUpdates* msg)
{
	free(msg->allocBuffer);
}
void msg_create_SetStatusUpdates(message *out, const SetStatusUpdates* msg)
{
	uint pos=0;	pos+=sizeof(uint32);
	pos+=sizeof(uint16);
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_SetStatusUpdates;
	pos=0;
	create_uint32(out, msg->StatusUpdateMask, &pos);
	create_uint16(out, msg->Interval, &pos);
}
int msg_parse_Status0(Status0 *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	req_bytes(in, &pos, sizeof(uint8)); pos+=sizeof(uint8);
	req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	parse_uint8(in, &out->PlaybackState, &pos);
	parse_uint32(in, &out->InputMillis, &pos);
	parse_uint32(in, &out->InputBytes, &pos);
	parse_uint32(in, &out->OutputMillis, &pos);
	parse_uint32(in, &out->OutputBytes, &pos);
	parse_uint32(in, &out->PlaybackPosition, &pos);
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_Status0(Status0* msg)
{
	free(msg->allocBuffer);
}
void msg_create_Status0(message *out, const Status0* msg)
{
	uint pos=0;	pos+=sizeof(uint8);
	pos+=sizeof(uint32);
	pos+=sizeof(uint32);
	pos+=sizeof(uint32);
	pos+=sizeof(uint32);
	pos+=sizeof(uint32);
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_Status0;
	pos=0;
	create_uint8(out, msg->PlaybackState, &pos);
	create_uint32(out, msg->InputMillis, &pos);
	create_uint32(out, msg->InputBytes, &pos);
	create_uint32(out, msg->OutputMillis, &pos);
	create_uint32(out, msg->OutputBytes, &pos);
	create_uint32(out, msg->PlaybackPosition, &pos);
}
int msg_parse_Status1(Status1 *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	{char *p=((char *)in->data)+pos;char *begin=p;char *end=((char *)in->data)+in->length;while (*p && p<end) p++;pos+=p-begin+(*p?0:1);dryAlloc((p-begin+1));}
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	{char *p=((char *)in->data)+pos;
	char *begin=p;
	char *end=((char *)in->data)+in->length;
	while (*p && p < end) p++;
	out->FileName=(char *)alloc(p-begin+1);
	memcpy(out->FileName, begin, p-begin);
	out->FileName[p-begin]=0;
	pos+=p-begin+1;}
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_Status1(Status1* msg)
{
	free(msg->allocBuffer);
}
void msg_create_Status1(message *out, const Status1* msg)
{
	uint pos=0;	pos+=strlen(msg->FileName)+1;
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_Status1;
	pos=0;
	strcpy(((char *)out->data)+pos, msg->FileName);pos+=strlen(msg->FileName)+1;
}
int msg_parse_Status2(Status2 *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	parse_uint32(in, &out->QueueNumber, &pos);
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_Status2(Status2* msg)
{
	free(msg->allocBuffer);
}
void msg_create_Status2(message *out, const Status2* msg)
{
	uint pos=0;	pos+=sizeof(uint32);
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_Status2;
	pos=0;
	create_uint32(out, msg->QueueNumber, &pos);
}
int msg_parse_QueueFile(QueueFile *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	{char *p=((char *)in->data)+pos;char *begin=p;char *end=((char *)in->data)+in->length;while (*p && p<end) p++;pos+=p-begin+(*p?0:1);dryAlloc((p-begin+1));}
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	{char *p=((char *)in->data)+pos;
	char *begin=p;
	char *end=((char *)in->data)+in->length;
	while (*p && p < end) p++;
	out->URL=(char *)alloc(p-begin+1);
	memcpy(out->URL, begin, p-begin);
	out->URL[p-begin]=0;
	pos+=p-begin+1;}
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_QueueFile(QueueFile* msg)
{
	free(msg->allocBuffer);
}
void msg_create_QueueFile(message *out, const QueueFile* msg)
{
	uint pos=0;	pos+=strlen(msg->URL)+1;
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_QueueFile;
	pos=0;
	strcpy(((char *)out->data)+pos, msg->URL);pos+=strlen(msg->URL)+1;
}
int msg_parse_QueueNumber(QueueNumber *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	{char *p=((char *)in->data)+pos;char *begin=p;char *end=((char *)in->data)+in->length;while (*p && p<end) p++;pos+=p-begin+(*p?0:1);dryAlloc((p-begin+1));}
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	parse_uint32(in, &out->Number, &pos);
	{char *p=((char *)in->data)+pos;
	char *begin=p;
	char *end=((char *)in->data)+in->length;
	while (*p && p < end) p++;
	out->URL=(char *)alloc(p-begin+1);
	memcpy(out->URL, begin, p-begin);
	out->URL[p-begin]=0;
	pos+=p-begin+1;}
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_QueueNumber(QueueNumber* msg)
{
	free(msg->allocBuffer);
}
void msg_create_QueueNumber(message *out, const QueueNumber* msg)
{
	uint pos=0;	pos+=sizeof(uint32);
	pos+=strlen(msg->URL)+1;
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_QueueNumber;
	pos=0;
	create_uint32(out, msg->Number, &pos);
	strcpy(((char *)out->data)+pos, msg->URL);pos+=strlen(msg->URL)+1;
}
int msg_parse_ExtendedMessage(ExtendedMessage *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	req_bytes(in, &pos, sizeof(uint16)); pos+=sizeof(uint16);
	req_bytes(in, &pos, sizeof(uint16)); pos+=sizeof(uint16);
	dryAlloc(in->length-pos); pos=in->length;
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	parse_uint16(in, &out->ExtensionID, &pos);
	parse_uint16(in, &out->ExtendedType, &pos);
	out->ExtendedDataLength=in->length-pos;
	if(in->length-pos) {
		out->ExtendedData=alloc(in->length-pos);
		memcpy(out->ExtendedData, ((char *)in->data)+pos, in->length-pos);
	} else
		out->ExtendedData=NULL;
	pos=in->length;
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_ExtendedMessage(ExtendedMessage* msg)
{
	free(msg->allocBuffer);
}
void msg_create_ExtendedMessage(message *out, const ExtendedMessage* msg)
{
	uint pos=0;	pos+=sizeof(uint16);
	pos+=sizeof(uint16);
	pos+=msg->ExtendedDataLength;
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_ExtendedMessage;
	pos=0;
	create_uint16(out, msg->ExtensionID, &pos);
	create_uint16(out, msg->ExtendedType, &pos);
	memcpy(((char *)out->data)+pos, msg->ExtendedData, msg->ExtendedDataLength); pos+=msg->ExtendedDataLength;
}

void msg_free_Pause(Pause* msg)
{
	free(msg->allocBuffer);
}
void msg_create_Pause(message *out) {
	out->length=0;
	out->type=MT_Pause;
	out->data=NULL;
}

void msg_free_Resume(Resume* msg)
{
	free(msg->allocBuffer);
}
void msg_create_Resume(message *out) {
	out->length=0;
	out->type=MT_Resume;
	out->data=NULL;
}

void msg_free_Stop(Stop* msg)
{
	free(msg->allocBuffer);
}
void msg_create_Stop(message *out) {
	out->length=0;
	out->type=MT_Stop;
	out->data=NULL;
}
int msg_parse_Seek(Seek *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	parse_uint32(in, &out->PlaybackPosition, &pos);
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_Seek(Seek* msg)
{
	free(msg->allocBuffer);
}
void msg_create_Seek(message *out, const Seek* msg)
{
	uint pos=0;	pos+=sizeof(uint32);
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_Seek;
	pos=0;
	create_uint32(out, msg->PlaybackPosition, &pos);
}

void msg_free_Skip(Skip* msg)
{
	free(msg->allocBuffer);
}
void msg_create_Skip(message *out) {
	out->length=0;
	out->type=MT_Skip;
	out->data=NULL;
}
int msg_parse_GetConfig(GetConfig *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	
	ConfigName tmp_Names;
	out->NamesCount=0;
	while (pos < in->length) {
		out->NamesCount++;
		ConfigName *out;out=&tmp_Names;
		dryAlloc(sizeof(tmp_Names));
		{char *p=((char *)in->data)+pos;char *begin=p;char *end=((char *)in->data)+in->length;while (*p && p<end) p++;pos+=p-begin+(*p?0:1);dryAlloc((p-begin+1));}
	}
	pos=in->length;
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	
	out->Names=(ConfigName*)alloc((out->NamesCount)*sizeof(ConfigName));
	{uint i; for (i=0;i<out->NamesCount;i++) {
		ConfigName *o_out=out->Names;
		ConfigName *out=&o_out[i];
		{char *p=((char *)in->data)+pos;
	char *begin=p;
	char *end=((char *)in->data)+in->length;
	while (*p && p < end) p++;
	out->Name=(char *)alloc(p-begin+1);
	memcpy(out->Name, begin, p-begin);
	out->Name[p-begin]=0;
	pos+=p-begin+1;}
	}}
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_GetConfig(GetConfig* msg)
{
	free(msg->allocBuffer);
}
void msg_create_GetConfig(message *out, const GetConfig* msg)
{
	uint pos=0;	{uint i;
	for (i=0;i<msg->NamesCount;i++) {
		typeof(msg) o_msg=msg;
		ConfigName *msg; msg=&o_msg->Names[i];
		pos+=strlen(msg->Name)+1;
	}}
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_GetConfig;
	pos=0;
	{uint i;
	for (i=0;i<msg->NamesCount;i++) {
		typeof(msg) o_msg=msg;
		ConfigName *msg; msg=&o_msg->Names[i];
		strcpy(((char *)out->data)+pos, msg->Name);pos+=strlen(msg->Name)+1;
	}}
}
int msg_parse_SetConfig(SetConfig *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	
	ConfigValuePair tmp_Values;
	out->ValuesCount=0;
	while (pos < in->length) {
		out->ValuesCount++;
		ConfigValuePair *out;out=&tmp_Values;
		dryAlloc(sizeof(tmp_Values));
		{char *p=((char *)in->data)+pos;char *begin=p;char *end=((char *)in->data)+in->length;while (*p && p<end) p++;if (p==end && *p) parse_error();pos+=p-begin+(*p?0:1);dryAlloc((p-begin+1));}
		{char *p=((char *)in->data)+pos;char *begin=p;char *end=((char *)in->data)+in->length;while (*p && p<end) p++;pos+=p-begin+(*p?0:1);dryAlloc((p-begin+1));}
	}
	pos=in->length;
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	
	out->Values=(ConfigValuePair*)alloc((out->ValuesCount)*sizeof(ConfigValuePair));
	{uint i; for (i=0;i<out->ValuesCount;i++) {
		ConfigValuePair *o_out=out->Values;
		ConfigValuePair *out=&o_out[i];
		{char *p=((char *)in->data)+pos;
	char *begin=p;
	char *end=((char *)in->data)+in->length;
	while (*p && p < end) p++;
	if (p == end && *p) parse_error();
	out->Name=(char *)alloc(p-begin+1);
	memcpy(out->Name, begin, p-begin);
	out->Name[p-begin]=0;
	pos+=p-begin+1;}
		{char *p=((char *)in->data)+pos;
	char *begin=p;
	char *end=((char *)in->data)+in->length;
	while (*p && p < end) p++;
	out->Value=(char *)alloc(p-begin+1);
	memcpy(out->Value, begin, p-begin);
	out->Value[p-begin]=0;
	pos+=p-begin+1;}
	}}
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_SetConfig(SetConfig* msg)
{
	free(msg->allocBuffer);
}
void msg_create_SetConfig(message *out, const SetConfig* msg)
{
	uint pos=0;	{uint i;
	for (i=0;i<msg->ValuesCount;i++) {
		typeof(msg) o_msg=msg;
		ConfigValuePair *msg; msg=&o_msg->Values[i];
		pos+=strlen(msg->Name)+1;
		pos+=strlen(msg->Value)+1;
	}}
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_SetConfig;
	pos=0;
	{uint i;
	for (i=0;i<msg->ValuesCount;i++) {
		typeof(msg) o_msg=msg;
		ConfigValuePair *msg; msg=&o_msg->Values[i];
		strcpy(((char *)out->data)+pos, msg->Name);pos+=strlen(msg->Name)+1;
		strcpy(((char *)out->data)+pos, msg->Value);pos+=strlen(msg->Value)+1;
	}}
}
int msg_parse_RequestStatusMsg(RequestStatusMsg *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	parse_uint32(in, &out->StatusUpdateMask, &pos);
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_RequestStatusMsg(RequestStatusMsg* msg)
{
	free(msg->allocBuffer);
}
void msg_create_RequestStatusMsg(message *out, const RequestStatusMsg* msg)
{
	uint pos=0;	pos+=sizeof(uint32);
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_RequestStatusMsg;
	pos=0;
	create_uint32(out, msg->StatusUpdateMask, &pos);
}
int msg_parse_Status3(Status3 *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	req_bytes(in, &pos, sizeof(uint64)); pos+=sizeof(uint64);
	req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	parse_uint64(in, &out->MediaBytes, &pos);
	parse_uint32(in, &out->MediaMillis, &pos);
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_Status3(Status3* msg)
{
	free(msg->allocBuffer);
}
void msg_create_Status3(message *out, const Status3* msg)
{
	uint pos=0;	pos+=sizeof(uint64);
	pos+=sizeof(uint32);
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_Status3;
	pos=0;
	create_uint64(out, msg->MediaBytes, &pos);
	create_uint32(out, msg->MediaMillis, &pos);
}
int msg_parse_SetExtendedStatusUpdates(SetExtendedStatusUpdates *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	req_bytes(in, &pos, sizeof(uint16)); pos+=sizeof(uint16);
	req_bytes(in, &pos, sizeof(uint32)); pos+=sizeof(uint32);
	req_bytes(in, &pos, sizeof(uint16)); pos+=sizeof(uint16);
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	parse_uint16(in, &out->ExtensionID, &pos);
	parse_uint32(in, &out->ExtendedStatusUpdateMask, &pos);
	parse_uint16(in, &out->Interval, &pos);
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_SetExtendedStatusUpdates(SetExtendedStatusUpdates* msg)
{
	free(msg->allocBuffer);
}
void msg_create_SetExtendedStatusUpdates(message *out, const SetExtendedStatusUpdates* msg)
{
	uint pos=0;	pos+=sizeof(uint16);
	pos+=sizeof(uint32);
	pos+=sizeof(uint16);
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_SetExtendedStatusUpdates;
	pos=0;
	create_uint16(out, msg->ExtensionID, &pos);
	create_uint32(out, msg->ExtendedStatusUpdateMask, &pos);
	create_uint16(out, msg->Interval, &pos);
}
int msg_parse_ExtendedStatusMessage(ExtendedStatusMessage *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	req_bytes(in, &pos, sizeof(uint16)); pos+=sizeof(uint16);
	req_bytes(in, &pos, sizeof(uint8)); pos+=sizeof(uint8);
	dryAlloc(in->length-pos); pos=in->length;
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	parse_uint16(in, &out->ExtensionID, &pos);
	parse_uint8(in, &out->ExtendedStatusID, &pos);
	out->ExtendedStatusDataLength=in->length-pos;
	if(in->length-pos) {
		out->ExtendedStatusData=alloc(in->length-pos);
		memcpy(out->ExtendedStatusData, ((char *)in->data)+pos, in->length-pos);
	} else
		out->ExtendedStatusData=NULL;
	pos=in->length;
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_ExtendedStatusMessage(ExtendedStatusMessage* msg)
{
	free(msg->allocBuffer);
}
void msg_create_ExtendedStatusMessage(message *out, const ExtendedStatusMessage* msg)
{
	uint pos=0;	pos+=sizeof(uint16);
	pos+=sizeof(uint8);
	pos+=msg->ExtendedStatusDataLength;
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_ExtendedStatusMessage;
	pos=0;
	create_uint16(out, msg->ExtensionID, &pos);
	create_uint8(out, msg->ExtendedStatusID, &pos);
	memcpy(((char *)out->data)+pos, msg->ExtendedStatusData, msg->ExtendedStatusDataLength); pos+=msg->ExtendedStatusDataLength;
}

void msg_free_GetAllConfig(GetAllConfig* msg)
{
	free(msg->allocBuffer);
}
void msg_create_GetAllConfig(message *out) {
	out->length=0;
	out->type=MT_GetAllConfig;
	out->data=NULL;
}

void msg_free_GetAllMediaInfo(GetAllMediaInfo* msg)
{
	free(msg->allocBuffer);
}
void msg_create_GetAllMediaInfo(message *out) {
	out->length=0;
	out->type=MT_GetAllMediaInfo;
	out->data=NULL;
}
int msg_parse_SetMediaInfo(SetMediaInfo *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	
	MediaInfoPair tmp_Values;
	out->ValuesCount=0;
	while (pos < in->length) {
		out->ValuesCount++;
		MediaInfoPair *out;out=&tmp_Values;
		dryAlloc(sizeof(tmp_Values));
		{char *p=((char *)in->data)+pos;char *begin=p;char *end=((char *)in->data)+in->length;while (*p && p<end) p++;if (p==end && *p) parse_error();pos+=p-begin+(*p?0:1);dryAlloc((p-begin+1));}
		{char *p=((char *)in->data)+pos;char *begin=p;char *end=((char *)in->data)+in->length;while (*p && p<end) p++;pos+=p-begin+(*p?0:1);dryAlloc((p-begin+1));}
	}
	pos=in->length;
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	
	out->Values=(MediaInfoPair*)alloc((out->ValuesCount)*sizeof(MediaInfoPair));
	{uint i; for (i=0;i<out->ValuesCount;i++) {
		MediaInfoPair *o_out=out->Values;
		MediaInfoPair *out=&o_out[i];
		{char *p=((char *)in->data)+pos;
	char *begin=p;
	char *end=((char *)in->data)+in->length;
	while (*p && p < end) p++;
	if (p == end && *p) parse_error();
	out->Name=(char *)alloc(p-begin+1);
	memcpy(out->Name, begin, p-begin);
	out->Name[p-begin]=0;
	pos+=p-begin+1;}
		{char *p=((char *)in->data)+pos;
	char *begin=p;
	char *end=((char *)in->data)+in->length;
	while (*p && p < end) p++;
	out->Value=(char *)alloc(p-begin+1);
	memcpy(out->Value, begin, p-begin);
	out->Value[p-begin]=0;
	pos+=p-begin+1;}
	}}
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_SetMediaInfo(SetMediaInfo* msg)
{
	free(msg->allocBuffer);
}
void msg_create_SetMediaInfo(message *out, const SetMediaInfo* msg)
{
	uint pos=0;	{uint i;
	for (i=0;i<msg->ValuesCount;i++) {
		typeof(msg) o_msg=msg;
		MediaInfoPair *msg; msg=&o_msg->Values[i];
		pos+=strlen(msg->Name)+1;
		pos+=strlen(msg->Value)+1;
	}}
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_SetMediaInfo;
	pos=0;
	{uint i;
	for (i=0;i<msg->ValuesCount;i++) {
		typeof(msg) o_msg=msg;
		MediaInfoPair *msg; msg=&o_msg->Values[i];
		strcpy(((char *)out->data)+pos, msg->Name);pos+=strlen(msg->Name)+1;
		strcpy(((char *)out->data)+pos, msg->Value);pos+=strlen(msg->Value)+1;
	}}
}
int msg_parse_GetMediaInfo(GetMediaInfo *out, const message *in)
{
	uint pos=0;
	uint allocNeeded=0;
	
	MediaInfoName tmp_Names;
	out->NamesCount=0;
	while (pos < in->length) {
		out->NamesCount++;
		MediaInfoName *out;out=&tmp_Names;
		dryAlloc(sizeof(tmp_Names));
		{char *p=((char *)in->data)+pos;char *begin=p;char *end=((char *)in->data)+in->length;while (*p && p<end) p++;pos+=p-begin+(*p?0:1);dryAlloc((p-begin+1));}
	}
	pos=in->length;
	pos=0;
	void *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);
	out->allocated=allocNeeded;
	uint allocUsed;
	
	out->Names=(MediaInfoName*)alloc((out->NamesCount)*sizeof(MediaInfoName));
	{uint i; for (i=0;i<out->NamesCount;i++) {
		MediaInfoName *o_out=out->Names;
		MediaInfoName *out=&o_out[i];
		{char *p=((char *)in->data)+pos;
	char *begin=p;
	char *end=((char *)in->data)+in->length;
	while (*p && p < end) p++;
	out->Name=(char *)alloc(p-begin+1);
	memcpy(out->Name, begin, p-begin);
	out->Name[p-begin]=0;
	pos+=p-begin+1;}
	}}
	out->allocUsed=allocUsed;
	return 0;
}
void msg_free_GetMediaInfo(GetMediaInfo* msg)
{
	free(msg->allocBuffer);
}
void msg_create_GetMediaInfo(message *out, const GetMediaInfo* msg)
{
	uint pos=0;	{uint i;
	for (i=0;i<msg->NamesCount;i++) {
		typeof(msg) o_msg=msg;
		MediaInfoName *msg; msg=&o_msg->Names[i];
		pos+=strlen(msg->Name)+1;
	}}
	out->data=malloc(pos);
	out->length=pos;
	out->type=MT_GetMediaInfo;
	pos=0;
	{uint i;
	for (i=0;i<msg->NamesCount;i++) {
		typeof(msg) o_msg=msg;
		MediaInfoName *msg; msg=&o_msg->Names[i];
		strcpy(((char *)out->data)+pos, msg->Name);pos+=strlen(msg->Name)+1;
	}}
}

void msg_free_EndOfResponse(EndOfResponse* msg)
{
	free(msg->allocBuffer);
}
void msg_create_EndOfResponse(message *out) {
	out->length=0;
	out->type=MT_EndOfResponse;
	out->data=NULL;
}

void msg_free_Ping(Ping* msg)
{
	free(msg->allocBuffer);
}
void msg_create_Ping(message *out) {
	out->length=0;
	out->type=MT_Ping;
	out->data=NULL;
}

void msg_free_Pong(Pong* msg)
{
	free(msg->allocBuffer);
}
void msg_create_Pong(message *out) {
	out->length=0;
	out->type=MT_Pong;
	out->data=NULL;
}
