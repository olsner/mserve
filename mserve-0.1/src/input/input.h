/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _input_H
#define _input_H

#include "common.h"
#include <libmserve/attriblist.h>

enum SeekSupportedValues
{
	/**
		Seeking is not supported at all by this plugin.
	*/
	SEEK_NOT_SUPPORTED=0,
	/**
		The operation is supported but inflicts severe performance
		penalties. This value should be reported for operations that
		take so long time that the user has to be notified. (operations
		that require a HTTP reconnection for example)
	*/
	SEEK_SUPPORTED_COSTLY=1,
	/**
		The operation is supported but could take some time to
		complete. A plugin may report this value if the operation
		takes enough time to disrupt the decoding/output process, but
		not so long that the user must be notified.
	*/
	SEEK_SUPPORTED=2,
	/**
		The operation can be performed efficiently. An mmap or file 
		input plugin would report this for all operations. An operation
		that gives this response should not produce any noticeable delay.
	*/
	SEEK_SUPPORTED_GOOD=3
};

struct _input_driver_info
{
	char *name;
	char *description;
	
	/**
		Return true if the url is supported by the input driver
	*/
	int (*supports)(const char *url);
	/**
		Initialize the input context for input with this input driver
		
		Return zero if the input context was successfully initialized, or a
		non-zero error code
	*/
	int (*init)(input_ctx *);
	/**
		Deinitialize the input context - deallocate internal data, close file
		descriptors etc.
	*/
	void (*deinit)(input_ctx *);
	
	/**
		Fill the buffer with input data. Should block.
		
		Returns the number of bytes read, or -1 on error
		Return zero on EOF
	*/
	int (*read)(input_ctx *, void *buffer, uint length);
	/**
		Return true if the input context is still active - that is, in working
		condition. Use the function after read or seek has returned an error
		to see if the error has been recovered from
	*/
	int (*isactive)(input_ctx *);
	/**
		Fill in the attriblist with attributes relevant for the input source.
		No attributes are defined, but you should not set attributes that belong
		to the decoder such as "title", "genre", "artist" etc.
	*/
	void (*update_info)(input_ctx *, attriblist *);
	/**
		Return the length of the media available, or -1 if unapplicable/unknown
	*/
	int64 (*get_media_length)(input_ctx *);
	/**
		Seek to the specified position. The whence argument has the same values
		and semantics as the whence argument to fseek/lseek.
		
		Returns the new absolute file position or -1 if the seek was
		insuccessful.
		Be aware that a plugin could support a subset of whence/pos arguments,
		such as not being able to seek SEEK_END. Also note that seek may impose
		significant overhead as certain combinations may require reopening the
		input source (HTTP when not seeking forward, for example)
	*/
	int64 (*seek)(input_ctx *, int64 pos, int whence);
	/**
		This function accepts the same parameters as the seek function and
		the parameters have the same semantics. However, this function does
		not perform a seek operation but instead returns a value describing
		the efficiency of the implementation of the seek operation that would
		have been performed by the seek function.
		
		Return values: (for details, see the SeekSupportedValues enum)
			0 (SEEK_NOT_SUPPORTED)
			1 (SEEK_SUPPORTED_COSTLY)
			2 (SEEK_SUPPORTED)
			3 (SEEK_SUPPORTED_GOOD)
	*/
	/*int (*seek_supported)(input_ctx *, int64 pos, int whence);*/
	/**
		Return the current stream position, or -1 if unapplicable/unknown
	*/
	int64 (*tell)(input_ctx *);
};

struct _input_ctx
{
	char *url;
	/* Internal data for input driver */
	void *internal;
	input_driver_info *driver;

/*#ifdef __cplusplus
	inline int init()
	{	return driver->init(this); }
	
	inline void deinit()
	{	driver->deinit(this); }
	
	inline int read(void *buffer, uint length)
	{	return driver->read(this, buffer, length); }
	
	inline bool isactive()
	{	return driver->isactive(this); }
	
	inline void update_info(attriblist *al)
	{	driver->update_info(this, al); }
	
	inline int64 seek(int64 off, int whence)
	{	return driver->seek(this, off, whence); }
	
	inline int64 get_media_length()
	{	return driver->get_media_length(this); }
#endif
*/
};

BEGIN_DECLS

extern input_driver_info *get_input_driver(const char *url);
extern input_ctx *init_input_context(input_ctx *, const char *url);

END_DECLS

#endif
