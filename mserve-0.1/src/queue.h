/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _mServe_queue_H
#define _mServe_queue_H

#include "common.h"

BEGIN_DECLS

typedef struct _qent
{
	char *url;
	uint32 number;
} qent;

extern void queue_lock();
extern void queue_unlock();
/**
	If the caller does not have a lock, there is no guarantee that the returned
	length is valid when it is processed by the caller.
*/
extern uint queue_len();

/**
	queue_push_unsec doesn't lock the queue mutex, queue_push does
	
	The push functions make a copy of the qent struct and the url. No extra
	memory allocation needs to be done by the caller.
	
	Returns: the queue number assigned to the file
*/
extern uint queue_push(const char *);
extern uint queue_push_unsec(const char *);
/**
	Like queue_push_unsec, queue_pop_unsec doesn't perform queue mutex locking,
	while queue_pop does.
	
	The qent struct returned by the functions should be freed by calling
	queue_free_qent
*/
extern qent *queue_pop(qent *);
extern qent *queue_pop_unsec(qent *);

extern void queue_free_qent(qent *);

/**
	Waits until the queue length is larger than zero and the queue is running.
	Call this if a queue lock is not held
*/
extern void queue_wait();
/**
	Like queue_wait, but a queue lock must be held on entry. The queue lock
	is released while waiting, but will be held on exit.
*/
extern void queue_wait_unsec();

/**
	Return non-zero if the queue is not currently stopped, zero if it is
	The queue is initially running
*/
extern int queue_isrunning();
/**
	Stop the queue. Future calls to queue_isrunning() will return FALSE, and 
	future calls to queue_wait() will block until queue_resume() is called.
*/
extern void queue_stop();
/**
	Restart the queue. Future calls to queue_isrunning() will return TRUE.
*/
extern void queue_resume(); 

END_DECLS

#endif
