/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <stdio.h>

#include <libmserve/mserve.h>
#include <testbase.h>
#include "../src/ext/filebrowser_msg.h"
#include "../src/ext/filebrowser_msg.c"

mserve *ctx;
#define CLIENT_NAME "mServe_ext_test"
#define CLIENT_VERSION 1

#define STATUS_UPDATE_MASK 0
#define STATUS_UPDATE_INTERVAL 0

int FileBrowser_ExtensionID=-1;

void handle_extended_message(const ExtendedMessage *em)
{
	message emsg={em->ExtendedType, em->ExtendedDataLength, em->ExtendedData};
	mserve_message tmp;
	if (em->ExtensionID==FileBrowser_ExtensionID)
	{
		switch (emsg.type)
		{
			case FileBrowser_MT_SetWD:
			{
				SetWD setwd;
				fb_msg_parse_SetWD(&setwd, &emsg);
				printf("Current FileBrowser Working Directory: \"%s\"\n", setwd.Directory);
				fb_msg_free_SetWD(&setwd);
				Iterate it;
				it.TypeMask=TYPE_MASK_MEDIA|TYPE_MASK_MISC;
				create_ext_message_id(&tmp, FileBrowser_ExtensionID, fb_msg_create_Iterate, &it);
				x_send_message(ctx, &tmp);
				mserve_free_message(&tmp);
				break;
			}
			case FileBrowser_MT_FileList:
			{
				FileList fl;
				fb_msg_parse_FileList(&fl, &emsg);
				printf("Directory %s:\n", fl.Directory);
				uint i=0;
				for (i=0;i<fl.FilesCount;i++)
				{
					printf("\t%s [%d]\n", fl.Files[i].Name, fl.Files[i].Type);
				}
				break;
			}
		}
	}
}

void handle_message(const mserve_message *msg)
{
	mserve_message tmp;
	switch (msg->type)
	{
		case MT_VersionInfo:
			mserve_handle_VersionInfo(ctx, msg, &(VersionInfo){CLIENT_NAME, CLIENT_VERSION, MSERVE_PROTOCOL_VERSION}, 1);
			break;
		case MT_UseProtocolVersion:
		{
			ExtensionInfoElement eie={"FileBrowser0001", 1};
			ExtensionInfo ei={1, &eie};
			msg_create_ExtensionInfo(&tmp, &ei);
			x_send_message(ctx, &tmp);
			mserve_free_message(&tmp);
			break;
		}
		case MT_UseExtensions:
		{
			UseExtensions ue;
			uint i;
			SetWD swd={"/test/tmppfx/bin"};
			message tmp2;

			printf("UseExtensions\n");
			msg_parse_UseExtensions(&ue, msg);
			for (i=0;i<ue.ExtensionsCount;i++)
			{
				printf("UseExtension: \"%-16s\", ID 0x%04x, Version %d\n", ue.Extensions[i].ExtensionName, ue.Extensions[i].ExtensionID, ue.Extensions[i].ExtensionVersion);
			}
			mserve_register_extensions(ctx, &ue, 1);
			msg_free_UseExtensions(&ue);
			FileBrowser_ExtensionID=mserve_get_extension_id(ctx, "FileBrowser0001");
			
			create_ext_message_id(&tmp, FileBrowser_ExtensionID, fb_msg_create_SetWD, &swd);
			x_send_message(ctx, &tmp);
			mserve_free_message(&tmp);
			
			tmp2.type=FileBrowser_MT_GetWD;
			tmp2.length=0;
			tmp2.data=0;
			mserve_create_extended_message(ctx, &tmp, "FileBrowser0001", &tmp2);
			x_send_message(ctx, &tmp);
			mserve_free_message(&tmp);
			
			mserve_wait_flush(ctx);
			break;
		}
		case MT_ExtendedMessage:
		{
			ExtendedMessage em;
			msg_parse_ExtendedMessage(&em, msg);
			handle_extended_message(&em);
			msg_free_ExtendedMessage(&em);
			break;
		}
	}
}

int main()
{
	socket_init(&ctx, handle_message);
	x_wait_message(ctx, MT_UseExtensions, handle_message);
	x_wait_extended_message(ctx, FileBrowser_ExtensionID, FileBrowser_MT_SetWD, handle_message);
	x_wait_extended_message(ctx, FileBrowser_ExtensionID, FileBrowser_MT_FileList, handle_message);
}
