/*
 * This file is part of libmserve.
 * 
 * libmserve is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * libmserve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with libmserve; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <libmserve/mserve-hl.h>
#include "internal-hl.h"

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

uint StatusMask[]=
{
	/*PLAYBACK*/1,
	/*URL*/2,
	/*QN*/4,
	/*MEDIA_LENGTH*/8
};

#define DEF_CLIENT_NAME "mServe HL Client"
#define DEF_CLIENT_VERSION 0xdeadbeef

mserve_hl *mserve_hl_init()
{
	mserve_hl *ret=(mserve_hl *)malloc(sizeof(mserve_hl));
	memset(ret, 0, sizeof(mserve_hl));
	ret->ctx=mserve_init();
	ret->verbosity=1;
	if (!ret->ctx)
	{
		free(ret);
		return NULL;
	}
	return ret;
}

mserve_error mserve_hl_connect(mserve_hl *ctx, const char *host, int port)
{
	return mserve_connect(ctx->ctx, host, port);
}

void mserve_hl_set_client_info(mserve_hl *ctx, const char *name, uint32 version)
{
	ctx->client_name=strdup(name);
	ctx->client_version=version;
}

void mserve_hl_destroy(mserve_hl *ctx)
{
	if (ctx->ctx)
		mserve_destroy(ctx->ctx);
	if (ctx->client_name)
		free(ctx->client_name);
	free(ctx);
}

/* Who likes to spend time typing similar stuff? better typing twice as much
   non-similar stuff =) */
#define callback_setter(_tp, _nm) \
	void mserve_hl_set_##_nm##_callback(mserve_hl *ctx, _tp##UpdateCallback cb) \
	{ 	ctx->_nm##_cb = cb; }

callback_setter(Playback, playback)
callback_setter(MediaLength, media_length)
callback_setter(URL, url)
callback_setter(QueueNumber, queue_number)
callback_setter(MediaInfo, media_info)

void mserve_hl_set_callback_data(mserve_hl *ctx, void *data)
{
	ctx->cb_data=data;
}

mserve_error mserve_hl_set_status_updates(mserve_hl *ctx, StatusUpdateType type, int interval)
{
	if (interval == -1)
	{
		return MSERVE_E_INVALID;
	}
	if (interval >= 0)
	{
		SetStatusUpdates ssu;
		mserve_message msg;
		
		if (type > STATUS_TYPE_ALL && type < STATUS_TYPE_LAST)
			ssu.StatusUpdateMask=StatusMask[type];
		else if (type == STATUS_TYPE_ALL)
		{
			for (int i=0;i<STATUS_TYPE_LAST;i++)
			{
				ssu.StatusUpdateMask=StatusMask[i];
			}
		}
		else
			return MSERVE_E_INVALID;
		ssu.Interval=interval;
		
		msg_create_SetStatusUpdates(&msg, &ssu);
		mserve_wait_send_message(ctx->ctx, &msg);
		mserve_free_message(&msg);
	}
	return MSERVE_SUCCESS;
}

mserve_error mserve_hl_run_loop(mserve_hl *ctx)
{
	int res;
	mserve_message msg;
	
	while ((res=mserve_wait_pop_message(ctx->ctx, &msg))==MSERVE_SUCCESS)
	{
		switch (msg.type)
		{
			case MT_VersionInfo:
			{
				VersionInfo vi=
					ctx->client_name?
						(VersionInfo){ctx->client_name, ctx->client_version}
					:	(VersionInfo){DEF_CLIENT_NAME, DEF_CLIENT_VERSION};
				mserve_handle_VersionInfo(ctx->ctx, &msg, &vi, ctx->verbosity);
				break;
			}
			case MT_UseProtocolVersion:
				// EXTENSION NEGOTIATION
				break;
			case MT_Status0:
			{
				if (ctx->playback_cb)
				{
					Status0 s0;
					msg_parse_Status0(&s0, &msg);
					ctx->playback_cb(ctx, ctx->cb_data, &s0);
					msg_free_Status0(&s0);
				}
				break;
			}
			case MT_Status3:
			{
				if (ctx->media_length_cb)
				{
					Status3 s3;
					msg_parse_Status3(&s3, &msg);
					ctx->media_length_cb(ctx, ctx->cb_data, &s3);
					msg_free_Status3(&s3);
				}
				break;
			}
			case MT_Status1:
			{
				if (ctx->url_cb)
				{
					Status1 s1;
					msg_parse_Status1(&s1, &msg);
					ctx->url_cb(ctx, ctx->cb_data, s1.FileName);
					msg_free_Status1(&s1);
				}
			}
			default:
				if (ctx->verbosity)
					printf("mserve_hl_run_loop: Unhandled message type 0x%04x, length %d\n", msg.type, msg.length);
				break;
		}
		mserve_free_message(&msg);
	}
	return res;
}

mserve_error mserve_hl_resume(mserve_hl *ctx)
{
	mserve_message msg;
	msg_create_Resume(&msg);
	mserve_error ret=mserve_wait_send_message(ctx->ctx, &msg);
	mserve_free_message(&msg);
	return ret;
}

mserve_error mserve_hl_pause(mserve_hl *ctx)
{
	mserve_message msg;
	msg_create_Pause(&msg);
	mserve_error ret=mserve_wait_send_message(ctx->ctx, &msg);
	mserve_free_message(&msg);
	return ret;
}

mserve_error mserve_hl_stop(mserve_hl *ctx)
{
	mserve_message msg;
	msg_create_Stop(&msg);
	mserve_error ret=mserve_wait_send_message(ctx->ctx, &msg);
	mserve_free_message(&msg);
	return ret;
}

mserve_error mserve_hl_skip(mserve_hl *ctx)
{
	mserve_message msg;
	msg_create_Skip(&msg);
	mserve_error ret=mserve_wait_send_message(ctx->ctx, &msg);
	mserve_free_message(&msg);
	return ret;
}
