/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <errno.h>
#include <pthread.h>

#include <sys/ioctl.h>
#include <linux/sockios.h>
#include <asm/ioctls.h>

#define _NETWORK_INTERNALS

#include <glib.h>
#include "socketdispatch.h"

uint int_hash_func(const void *);
void socket_ctx_dealloc_cb(void *);

GHashTable *fd_ctx_map=NULL;
pthread_mutex_t fd_ctx_map_mutex=PTHREAD_MUTEX_INITIALIZER;

void socketdispatch_init()
{
	fd_ctx_map=g_hash_table_new_full(g_direct_hash, g_direct_equal, NULL, socket_ctx_dealloc_cb);
}

socket_ctx *socket_ctx_from_fd(int fd)
{
	if (!fd_ctx_map)
		socketdispatch_init();
	socket_ctx *ctx=(socket_ctx *)g_hash_table_lookup(fd_ctx_map, (void *)fd);
	if (!ctx)
	{
		socket_ctx *newctx=(socket_ctx *)malloc(sizeof(socket_ctx));
		
		memset(newctx, 0, sizeof(socket_ctx));
		newctx->fd=fd;
		
		pthread_mutex_lock(&fd_ctx_map_mutex);
		g_hash_table_insert(fd_ctx_map, (void *)fd, newctx);
		pthread_mutex_unlock(&fd_ctx_map_mutex);
		
		return newctx;
	}
	else
		return ctx;
}

void socket_ctx_foreach(foreach_cb cb, void *data)
{
	if (!fd_ctx_map)
	{
		socketdispatch_init();
		return;
	}
	pthread_mutex_lock(&fd_ctx_map_mutex);
	g_hash_table_foreach(fd_ctx_map, (void (*)(void*,void*,void*))cb, data);
	pthread_mutex_unlock(&fd_ctx_map_mutex);	
}

void socket_ctx_dealloc_cb(void *data)
{
	socket_ctx *ctx=(socket_ctx *)data;
	if (ctx->deleted)
		ctx->deleted(ctx, SNE_DELETED);
	free(ctx);
}

void socket_ctx_delete(socket_ctx *ctx)
{
	pthread_mutex_lock(&fd_ctx_map_mutex);
	g_hash_table_remove(fd_ctx_map, (void *)ctx->fd);
	pthread_mutex_unlock(&fd_ctx_map_mutex);
}

struct dispatch_scan_data
{
	fd_set *fds[3]; // Read, Write, Exception
	int *maxfd;
};

void dispatch_scan_cb(void *key, void *value, void *data)
{
	struct dispatch_scan_data *sd=(struct dispatch_scan_data *)data;
	int fd=(int)key;
	socket_ctx *ctx=(socket_ctx *)value;
	if (ctx->read || ctx->error)
		FD_SET(fd, sd->fds[0]);
	if (ctx->write && (ctx->wantswrite?ctx->wantswrite(ctx):TRUE))
		FD_SET(fd, sd->fds[1]);
	if (ctx->exception)
		FD_SET(fd, sd->fds[2]);
	if (fd > *sd->maxfd) *sd->maxfd=fd;
}

int socket_dispatch(struct timeval *timeout)
{
	if (!fd_ctx_map)
	{
		socketdispatch_init();
		return;
	}

	fd_set rfd,wfd,efd;
	int maxfd=0, res, i;
	struct dispatch_scan_data sd={{&rfd, &wfd, &efd},&maxfd};
	
	FD_ZERO(&rfd);
	FD_ZERO(&wfd);
	FD_ZERO(&efd);
	
	pthread_mutex_lock(&fd_ctx_map_mutex);
	g_hash_table_foreach(fd_ctx_map, dispatch_scan_cb, &sd);
	pthread_mutex_unlock(&fd_ctx_map_mutex);
	
	//printf("maxfd: %d\n", maxfd);
	res=select(maxfd+1, &rfd, &wfd, &efd, timeout);
	
	if (res < 0)
		return -1;
	
	if (res == 0) return 0;
	
	//printf("socket_dispatch: select() res is %d\n", res);
	
	for (i=0;i<=maxfd;i++)
	{
		int hasRead=FD_ISSET(i, &rfd);
		int hasWrite=FD_ISSET(i, &wfd);
		int hasExcept=FD_ISSET(i, &efd);
		
		if (!(hasRead+hasWrite+hasExcept)) continue;
		socket_ctx *ctx=socket_ctx_from_fd(i);
		if (hasRead) 
		{
			int res=ioctl(i, SIOCINQ, &ctx->available);
			ctx->lasterr=errno;
			//printf("socketdispatch: res %d, fd %d, avail %d, error: %s [%d]\n",
			//	res, i, ctx->available, strerror(ctx->lasterr), ctx->lasterr);
			// EINVAL means that we have a server socket
			if ((res != 0 && ctx->lasterr != EINVAL) || (res==0 && ctx->available==0))
			{
				if (res==0)
					ctx->lasterr=EPIPE;
				if (ctx->error)
					ctx->error(ctx, SNE_ERROR);
			}
			else if (ctx->read)
				ctx->read(ctx, SNE_READ);
		}
		if (hasWrite)
		{
			int res=ioctl(i, SIOCOUTQ, &ctx->available);
			if (res != 0)
			{
				ctx->lasterr = res;
				if (ctx->error)
					ctx->error(ctx, SNE_ERROR);
			}
			//printf("socket_dispatch: hasWrite for %d, available is %d\n", i, ctx->available);
			if (ctx->write)
				ctx->write(ctx, SNE_WRITE);
		}
		if (hasExcept && ctx->exception)
			ctx->exception(ctx, SNE_EXCEPTION);
	}
	
	return 0;
}
