/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include "out_driver.h"
#include "common.h"
#include "buffer.h"
#include "decode/decode.h"
#include "cfg.h"

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

int disk_init(const char *dev);
void disk_deinit();
int disk_set_format(int freq, SampleFormat fmt, int channels);
int disk_output(decode_ctx *, void *buffer, unsigned int size);
int disk_get_preferred_chunk_size();
void disk_set_volume(int volume);
int disk_get_volume();

out_driver_info disk_info =
{
	"disk",
	"Disk writer plugin",
	disk_init,
	disk_deinit,
	disk_set_format,
	disk_output,
	disk_get_preferred_chunk_size,
	NULL,NULL
};

int fd=0;
int g_bps=1;
int g_freq=1;

out_driver_info * out_driver_get_info()
{
	return &disk_info;
}

int disk_init(const char *dev)
{
	fd=open(dev?dev:"pcmdump", O_CREAT|O_WRONLY, 0644);
	return 0;
}

void disk_deinit()
{
	close(fd);
	fd=0;
}

int disk_set_format(int freq, SampleFormat sfmt, int channels)
{
	g_freq=freq;
	g_bps=channels*(sfmt<4?2:1);
	return 0;
}

int disk_get_preferred_chunk_size()
{
	return 1024;
}

int disk_output(decode_ctx *dec_ctx, void *buf, uint size)
{
	int wrt=0;
	while (size)
	{
		int res=write(fd, buf, size);
		if (res < 0) return -1;
		size-=res;
		buf=(char*)buf+res;
		wrt+=res;
	}
	dec_ctx->status0.OutputBytes += wrt;
	dec_ctx->status0.OutputMillis += (wrt*1000)/(g_freq*g_bps);
	return wrt;
}
