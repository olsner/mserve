/*
 * This file is part of mdfgen.
 * 
 * mdfgen is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mdfgen is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mdfgen; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Copyright (C) Simon Brenner 2003
 */

#ifndef fields_hpp
#define fields_hpp

#define foreach(_it, _vect) for (typeof(_vect.begin()) _it=_vect.begin();_it!=_vect.end();++_it)

#include "istruct.hpp"

struct Protocol;

struct Field
{
	string name;

	virtual string getFieldDef()=0;
	virtual string getCreateStmts()=0;
	virtual string getDryCreateStmts()=0;
	virtual string getParseStmts()=0;
	virtual string getDryParseStmts()=0;
	
	Field(xmlNodePtr);
	static Field *parseFieldNode(xmlNodePtr, IStruct *, Protocol *);
};

struct ArrayField: public IStruct, public Field
{
	string type;
	
	ArrayField(xmlNodePtr nod);

	// IStruct
	virtual string getStructDef();
	virtual string getName();

	// Field
	virtual string getFieldDef();
	virtual string getCreateStmts();
	virtual string getDryCreateStmts();
	virtual string getParseStmts();
	virtual string getDryParseStmts();
};

struct SimpleField: public Field
{
	string type;
	inline SimpleField(xmlNodePtr node):
		Field(node),
		type(string((char *)node->name))
	{}
	
	virtual string getFieldDef();
	virtual string getCreateStmts();
	virtual string getDryCreateStmts();
	virtual string getParseStmts();
	virtual string getDryParseStmts();
};

struct StringField: public Field
{
	uint length;
	string lengthstr;
	bool optionalTermination;
	
	StringField(xmlNodePtr nod);
	
	virtual string getFieldDef();
	virtual string getCreateStmts();
	virtual string getDryCreateStmts();
	virtual string getParseStmts();
	virtual string getDryParseStmts();
};

struct DataField: public Field
{
	inline DataField(xmlNodePtr node):
		Field(node)
	{}
	
	virtual string getFieldDef();
	virtual string getCreateStmts();
	virtual string getDryCreateStmts();
	virtual string getParseStmts();
	virtual string getDryParseStmts();
};

#endif
