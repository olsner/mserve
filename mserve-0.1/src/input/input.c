/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include "input.h"
#include "file_input.h"
#include "null_input.h"
#include <stddef.h>
#include <stdlib.h>

#define MAKE_INFO(name, desc, prefix) \
	{name, desc, prefix##_supports, prefix##_init, \
	prefix##_deinit, prefix##_read, prefix##_isactive, prefix##_update_info, \
	prefix##_get_media_length, prefix##_seek, prefix##_tell}

input_driver_info static_drivers[]=
{
	MAKE_INFO("file", "File Input Driver", file),
	//MAKE_INFO("http", "HTTP Input Driver", http),
	MAKE_INFO("null", "NULL Input Driver", null),
};

#define NUM_STATIC_DRIVERS sizeof(static_drivers)/sizeof(static_drivers[0])

input_driver_info *get_input_driver(const char *url)
{
	int i;
	for (i=0;i<NUM_STATIC_DRIVERS;i++)
	{
		input_driver_info *ret=&static_drivers[i];
		if (ret->supports(url))
			return ret;
	}
	return NULL;
	// Search through input plugin directory. Later =)
}

input_ctx *init_input_context(input_ctx *ctx, const char *url)
{
	input_driver_info *driver=get_input_driver(url);
	input_ctx *ret=ctx?ctx:(input_ctx *)malloc(sizeof(input_ctx));
	ret->url=(char *)url;
	ret->driver=driver;
	ret->internal=0;
	
	return ret;
}
