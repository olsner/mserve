/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <errno.h>

#define _NETWORK_INTERNALS
#include "client.h"
#include "net.h"
#include "msched.h"
#include "ext/extension.h"

client_ctx *client_ctx_create(socket_ctx *socket)
{
	client_ctx *ret=(client_ctx *)malloc(sizeof(client_ctx));
	memset(ret, 0, sizeof(client_ctx));
	ret->socket=socket;
	ret->socket->userdata=ret;
	ret->socket->read=client_ctx_read_cb;
	ret->socket->write=client_ctx_write_cb;
	ret->socket->error=client_ctx_error_cb;
	ret->socket->wantswrite=client_ctx_wants_write_cb;
	pthread_mutex_init(&ret->outbuffer_mutex, NULL);
	
	msg_reader_init(&ret->msg_reader, client_msg_reader_callback, ret);
	
	return ret;
}

socket_ctx *client_ctx_delete(client_ctx *client, ClientDestroyReason reason)
{
	socket_ctx *ret=client->socket;
	
	sched_clear_all(client);
	extension_free_client(client);
	
	if (client->deleted)
		client->deleted(client, reason);

	msg_reader_destroy(&client->msg_reader);

	free(client);
	return ret;
}

void client_msg_reader_callback(msg_reader_ctx *msg_reader, uint type, uint len, void *data)
{
	client_ctx *client=(client_ctx*)msg_reader->userdata;
	if (client->message_cb)
	{
		mserve_message msg={type, len, data};
		client->message_cb(client, &msg);
	}
}

void client_ctx_read_cb(socket_ctx *socket, SocketNotifyEvent sne)
{
	client_ctx *client=(client_ctx *)socket->userdata;
	char *buf=(char *)alloca(socket->available);
	uint count=read(socket->fd, buf, socket->available);
	
	msg_reader_indata(&client->msg_reader, buf, count);
}

void client_ctx_write_cb(socket_ctx *socket, SocketNotifyEvent sne)
{
	client_ctx *client=(client_ctx *)socket->userdata;
	pthread_mutex_lock(&client->outbuffer_mutex);
	int towrite=client->outbufpos;
	int written=write(socket->fd, client->outbuffer, towrite);
	if (written < 0)
	{
		socket->error(socket, SNE_ERROR);
		written=0;
	}
	memmove(client->outbuffer, client->outbuffer+written, client->outbufpos-written);
	client->outbufpos -= written;
	pthread_mutex_unlock(&client->outbuffer_mutex);
}

int client_ctx_wants_write_cb(socket_ctx *socket)
{
	return ((client_ctx *)socket->userdata)->outbufpos;
}

void client_ctx_error_cb(socket_ctx *socket, SocketNotifyEvent sne)
{
	log_printf("%s [%d]", strerror(socket->lasterr), socket->lasterr);
	socket_ctx_delete(client_ctx_delete((client_ctx *)socket->userdata, CDR_NET_ERROR));
}

uint client_ctx_get_sendbuffer_free(client_ctx *client)
{
	return CLIENT_OUTBUFLEN-client->outbufpos;
}

int internal_sendbuffer_add(client_ctx *client, const void *data, uint len)
{
	uint avail=CLIENT_OUTBUFLEN-client->outbufpos;
	if (avail < len)
	{
		return -1;
	}
	memcpy(client->outbuffer+client->outbufpos, data, len);
	client->outbufpos += len;
	return 0;
}

int client_ctx_buffer_message(client_ctx *client, const mserve_message *msg)
{
	//printf("client_ctx_buffer_message(): fd %d, mt 0x%04x, len %d\n", client->socket->fd, messagetype, len);
	pthread_mutex_lock(&client->outbuffer_mutex);
	if (client->outbufpos+4+msg->length > CLIENT_OUTBUFLEN)
	{
		pthread_mutex_unlock(&client->outbuffer_mutex);
		return -1;
	}
	
	*(uint16 *)(client->outbuffer+client->outbufpos)=msg->type;
	*(uint16 *)(client->outbuffer+client->outbufpos+2)=msg->length;
	client->outbufpos += 4;
	internal_sendbuffer_add(client, msg->data, msg->length);
	
	pthread_mutex_unlock(&client->outbuffer_mutex);
	
	client_ctx_write_cb(client->socket, SNE_WRITE);
	//net_signal_reselect();
	
	return 0;
}

int client_ctx_buffer_ext_message(client_ctx *client, const ExtendedMessage *msg)
{
	pthread_mutex_lock(&client->outbuffer_mutex);
	if (client->outbufpos+4+msg->ExtendedDataLength > CLIENT_OUTBUFLEN)
	{
		pthread_mutex_unlock(&client->outbuffer_mutex);
		return -1;
	}
	
	*(uint16 *)(client->outbuffer+client->outbufpos)=MT_ExtendedMessage;
	*(uint16 *)(client->outbuffer+client->outbufpos+2)=msg->ExtendedDataLength+4;
	*(uint16 *)(client->outbuffer+client->outbufpos+4)=msg->ExtensionID;
	*(uint16 *)(client->outbuffer+client->outbufpos+6)=msg->ExtendedType;
	client->outbufpos += 8;
	internal_sendbuffer_add(client, msg->ExtendedData, msg->ExtendedDataLength);
	
	pthread_mutex_unlock(&client->outbuffer_mutex);
	
	client_ctx_write_cb(client->socket, SNE_WRITE);
	return 0;
}
