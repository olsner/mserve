/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _mServe_outdriver_H
#define _mServe_outdriver_H

#include "common.h"

struct _decode_ctx;

enum _SampleFormat
{
	SFMT_16BE_U,
	SFMT_16BE_S,
	SFMT_16LE_U,
	SFMT_16LE_S,
	SFMT_8_U,
	SFMT_8_S
};
typedef enum _SampleFormat SampleFormat;

/**
	An output driver defines a function out_driver_get_info, according to the 
	prototype below that returns a pointer to this struct.
*/
struct _out_driver_info
{
	/**
		These fields should be filled in by the out_driver_get_info function
		The name field must correspond to the filename
	*/
	const char *name;
	const char *description;
	/**
		init should return zero on success, non-zero for failure
		The output device, i.e. any string following the colon in the driver
		specification, is passed as the first argument. If there is no colon in
		the driver specification; a NULL pointer is passed
	*/
	int (*init)(const char *);
	/* */
	void (*deinit)();
	/**
		set format for future sample data
		
		freq is the sampling frequency, in samples per channel and second
		channels is the number of channels
		
		The samples are always provided packed, unaligned:
		
		(Provided Right is channel 0 and left channel 1)
		R1,L1,R2,L2,R3,L3, ..... Rn, Ln
	
		return zero if the format is supported
		return non-zero for unsupported format
	*/
	int (*set_format)(int freq, SampleFormat fmt, int channels);
	/**
		Send samples to output device
		size is the length of the buffer in bytes.
		There is no "default" sample format, so any calls to this function
		between calling init() and calling set_format() have undefined results,
		and should be ignored by the plugin.
		The size of the buffer provided should match an even amount of samples
		for each channel.
		
		Returns the bytes written to output
	*/
	int (*output)(struct _decode_ctx *ctx, void *buffer, unsigned int size);
	/**
		Return the size of the preferred sample chunk, in bytes. Will be called
		after a set_format.
		
		Returns the bytes in a preferred sample chunk, or 0 if it does not matter
	*/
	int (*get_preferred_chunk_size)();
	/**
		Set the volume 
		volume is 0-255, 0==mute, 255=full
	*/
	void (*set_volume)(int volume);
	/**
		Get the volume
		See set_volume for range etc.
	*/
	int (*get_volume)();
};

typedef out_driver_info *(out_driver_get_info_func)();

#define LOWWATER_MARK (8192)
#define OUT_BUFFER_SZ (LOWWATER_MARK*2)

BEGIN_DECLS

extern out_driver_info *out_driver_get_info();

/**
	This function must be called from the main function, before using any other
	out_driver function
*/
extern void out_driver_init();
/**
	Load and retrieve the output driver with basename <name>
	
	Returns: A pointer to the out_driver_info struct, or NULL
*/
extern out_driver_info *get_out_driver(const char *name);

extern int out_driver_run(struct _decode_ctx *);
extern void out_driver_run_thread(struct _decode_ctx *);
extern void out_driver_join_thread();

END_DECLS

#endif
