/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _null_input_H
#define _null_input_H

#include "common.h"

BEGIN_DECLS

extern int null_supports(const char *url);
extern int null_init(input_ctx *);
extern void null_deinit(input_ctx *);
extern int null_read(input_ctx *, void *buffer, uint length);
extern int null_isactive(input_ctx *);
extern void null_update_info(input_ctx *, attriblist *);
extern int64 null_get_media_length(input_ctx *);
extern int64 null_seek(input_ctx *, int64, int);
extern int64 null_tell(input_ctx *);

END_DECLS

#endif
