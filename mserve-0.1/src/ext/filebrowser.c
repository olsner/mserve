/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>

#include <libmserve/mserve.h>

#include "cfg.h"
#include "extension.h"
#include "filebrowser_msg.h"

/*extern extension_info **get_extension_info();*/

void fb_init_context(client_extension_ctx *);
void fb_destroy_context(client_extension_ctx *);
void fb_message(client_extension_ctx *, message *);
void fb_set_status_updates(client_extension_ctx *, uint, uint);
void fb_send_status_update(client_extension_ctx *, uint);

#define PULL_INTERNAL(_client) struct fb_client_internal *internal=(struct fb_client_internal *)((_client)->internal)

struct fb_client_internal
{
	char wd[PATH_MAX];
	char real_wd[PATH_MAX];
	int has_real_wd;
};

extension_info fb_extinfo={
	0,
	"FileBrowser0001",
	1,
	NULL,
	fb_init_context,
	fb_destroy_context,
	fb_message,
	fb_set_status_updates,
	fb_send_status_update
};

extension_info *extinfos[]={
	&fb_extinfo,
	NULL
};

extension_info **get_extension_info()
{
	log_print("get_extension_info()");
	char rpath[PATH_MAX];
	char *fpath="/test/tmppfx/bin";
	int r=translate_path(rpath, fpath);
	log_printf("\"%s\" => \"%s\"\n", fpath, rpath);
	return extinfos;
}

void fb_init_context(client_extension_ctx *client)
{
	log_printf("client %p", client->client);
	client->internal=malloc(sizeof(struct fb_client_internal));
	PULL_INTERNAL(client);
	memcpy(internal->wd, "/", 2);
	memcpy(internal->real_wd, "/", 2);	
}

void fb_destroy_context(client_extension_ctx *client)
{
	log_printf("client %p", client->client);
	free(client->internal);
}

char *pathchr(const char *ptr, int c, const char *end)
{
	return (char *)memchr(ptr, c, end-ptr);
}

/**
	Return length or -1
*/
int getroot(char *optr, char *rpend, const char *root, uint rootlen)
{
	// "filebrowser_root_"+root+'\0': 
	char *tmp=(char *)alloca(rootlen+18);
	memcpy(tmp, "filebrowser_root_", 17);
	memcpy(tmp+17, root, rootlen);
	tmp[rootlen+17]=0;
	
	int r=cfg_get_string(tmp, optr, rpend-optr);
	return r;
}

/**
	realpath must point to a buffer at least PATH_MAX long. fakepath must
	be an absolute virtual path
	
	Return zero if the virtual path was valid. -1 if it is invalid or did not
	fit into PATH_MAX chars
*/
int translate_path(char *realpath, const char *fakepath)
{
	char *fpend=pathchr(fakepath, 0, fakepath+PATH_MAX);
	char *optr=realpath;
	char *rpend=realpath+PATH_MAX;
	if (*fakepath == '/')
		fakepath++;
	else
		return -1;
	// Begin; take the first component, run it through getroot to get first output
	// component
	char *fslash=pathchr(fakepath, '/', fpend);
	if (fslash == NULL)
		fslash=fpend;
	int res=getroot(optr, rpend, fakepath, fslash-fakepath);
	if (res == -1)
		return -1;
	else
		optr+=res;
	if (optr >= rpend)
		return -1;
	int level=0;
	// For each remaining component, add a slash to the output and append
	// the input component
	while (fslash < fpend)
	{
		*optr++='/';
		fakepath=fslash+1;
		fslash=pathchr(fakepath, '/', fpend);
		if (fakepath[0]=='.' && fakepath[1]=='.')
			level--;
		else
			level++;
		if (level < 0)
			return -1;
		if (!fslash)
			fslash=fpend;
		if (optr+(fslash-fakepath) >= rpend)
			return -1;
		memcpy(optr, fakepath, fslash-fakepath);
		optr+=fslash-fakepath;
	}
	*optr++='/';
	*optr=0;
	return optr-realpath;
}

void fb_update_real_wd(client_extension_ctx *client, int wdchanged)
{
	PULL_INTERNAL(client);
	if (wdchanged || !internal->has_real_wd)
	{
		int r=translate_path(internal->real_wd, internal->wd);
		if (r < 0)
		{
			log_printf("Invalid path \"%s\"", internal->wd);
		}
		else
		{
			//log_printf("\"%s\" -> \"%s\"", internal->wd, internal->real_wd);
		}
	}
}

void fb_message(client_extension_ctx *client, mserve_message *msg)
{
	PULL_INTERNAL(client);
	mserve_message tmp;

	switch (msg->type)
	{
		case FileBrowser_MT_SetWD:
		{
			SetWD setwd;
			char rpath[PATH_MAX];
			fb_msg_parse_SetWD(&setwd, msg);
			log_printf("FileBrowser_MT_SetWD: \"%s\"", setwd.Directory);
			int r=translate_path(rpath, setwd.Directory);
			if (r < 0)
				log_printf("FileBrowser_MT_SetWD: \"%s\": Invalid path", setwd.Directory);
			else
			{
				log_printf("FileBrowser_MT_SetWD: \"%s\" -> \"%s\"", setwd.Directory, rpath);
				strncpy(internal->wd, setwd.Directory, PATH_MAX-1);
				// Always have a terminating null
				internal->wd[PATH_MAX-1]=0;
			}
			fb_update_real_wd(client, TRUE);
			fb_msg_free_SetWD(&setwd);
			break;
		}
		case FileBrowser_MT_GetWD:
		{
			log_print("FileBrowser_MT_GetWD");
			SetWD setwd;
			setwd.Directory=internal->wd;
			fb_msg_create_SetWD(&tmp, &setwd);
			extension_buffer_message(client, &tmp);
			mserve_free_message(&tmp);
			break;
		}
		case FileBrowser_MT_Iterate:
		{
			log_print("FileBrowser_MT_Iterate");
			Iterate it;
			fb_msg_parse_Iterate(&it, msg);
			FileList fl;
			fl.Directory=internal->wd;
			
			fb_update_real_wd(client, FALSE);
			
			DIR *dp=opendir(internal->real_wd);
			if (!dp)
			{
				log_printf("%s invalid: %s", internal->real_wd, strerror(errno));
				break;
			}
			uint n=0, i=0;
			while (readdir(dp))
				n++;
			fl.Files=(FileListEntry *)malloc(sizeof(FileListEntry)*n);
			rewinddir(dp);
			int wdlen=strlen(internal->real_wd);
			for (i=0;i<n;i++)
			{
				struct dirent *de=readdir(dp);
				
				if (!de) break;
				
				struct stat statbuf;
				int len=strlen(de->d_name);
				char *name=(char *)malloc(len+2+wdlen);
				
				memcpy(name, internal->real_wd, wdlen);
				memcpy(name+wdlen, de->d_name, len+1);
				
				if (stat(name, &statbuf) == -1)
				{
					fl.Files[i].Name=NULL;
					free(name);
					i--;
					continue;
				}
				
				fl.Files[i].Name=name+wdlen;
				fl.Files[i].Type=S_ISDIR(statbuf.st_mode)?TYPE_DIR:(!strncmp(de->d_name+len-4, ".mp3", 4)?TYPE_MEDIA:TYPE_MISC);
				if (!((1 << fl.Files[i].Type) & it.TypeMask))
				{
					fl.Files[i].Name=NULL;
					free(name);
					i--;
					continue;
				}
			}
			
			fl.FilesCount=i;
			fb_msg_create_FileList(&tmp, &fl);
			extension_buffer_message(client, &tmp);
			
			for (i=0;i<fl.FilesCount;i++)
				if (fl.Files[i].Name) free(fl.Files[i].Name-wdlen);
			free(fl.Files);
		}
	}
}

void fb_set_status_updates(client_extension_ctx *ctx, uint mask, uint interval)
{}

void fb_send_status_update(client_extension_ctx *ctx, uint id)
{}
