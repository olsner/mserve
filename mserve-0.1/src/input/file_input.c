/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include "input.h"
#include "file_input.h"

#define __USE_LARGEFILE64
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>

int file_supports(const char *url)
{
	struct stat f_info;
	if (strncmp(url, "file://", 7)==0)
		return TRUE;
	else if (stat(url, &f_info)==0)
		return TRUE;
	else
		return FALSE;
}

int file_init(input_ctx *ctx)
{
	char *filename=ctx->url;
	struct stat f_info;
	if (strncmp(filename, "file://", 7)==0)
		filename+=7;
	else if (stat(filename, &f_info)!=0)
		return -1;
	
	int fd=open(filename, O_RDONLY);
	if (fd == -1)
		return -1;
	ctx->internal=(void *)fd;
	return 0;
}

void file_deinit(input_ctx *ctx)
{
	int fd=(int)ctx->internal;
	
	if (fd != -1)
		close(fd);
	
	ctx->driver=NULL;
}

int file_read(input_ctx *ctx, void *buffer, uint length)
{
	int fd=(int)ctx->internal;
	
	if (fd == -1)
		return -1;
	
	int res=read(fd, buffer, length);
	if (res == 0)
	{
		close(fd);
		fd=-1;
	}
	return res;
}

int file_isactive(input_ctx *ctx)
{
	int fd=(int)ctx->internal;
	
	return fd != -1;
}

void file_update_info(input_ctx *ctx, attriblist *al)
{
}

int64 file_get_media_length(input_ctx *ctx)
{
	int fd=(int)ctx->internal;
	struct stat64 statbuf;
	
	fstat64(fd, &statbuf);
	if (S_ISREG(statbuf.st_mode))
	{
		return statbuf.st_size;
	}
	else
		return -1;
}

int64 file_seek(input_ctx *ctx, int64 off, int whence)
{
	int fd=(int)ctx->internal;
	return lseek64(fd, off, whence);
}

int64 file_tell(input_ctx *ctx)
{
	int fd=(int)ctx->internal;
	return lseek64(fd, 0, SEEK_CUR);
}
