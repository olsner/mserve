/*
 * This file is part of mdfgen.
 * 
 * mdfgen is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mdfgen is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mdfgen; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Copyright (C) Simon Brenner 2003
 */

#ifndef protocol_hpp
#define protocol_hpp

#include "istruct.hpp"
#include "fields.hpp"

struct Protocol;

struct Define
{
	string name;
	string value;
	
	Define(xmlNodePtr nod);
};

struct Message: public IStruct
{
	string name;
	string id;
	
	virtual string getStructDef();
	virtual string getName();
	string getParseFunc(const string &funcpfx);
	string getFreeFunc(const string &funcpfx);
	string getCreateFunc(const string &funcpfx, const string &mtprefix);
	string getPrototypes(const string &funcpfx);
	
	Message(xmlNodePtr node, Protocol *proto);
};

enum ProtocolByteOrder
{
	BO_Host,
	BO_Intel,
	BO_Motorola
};

struct Protocol
{
	vector <Define> defs;
	vector <IStruct *> structs;
	vector <Message> msgs;
	
	ProtocolByteOrder byteorder;
	string name;
	uint version;
	string mtprefix;
	string funcprefix;
	
	Protocol(xmlNodePtr node, xmlDocPtr doc);
	
	void makeFunctionPrototypes(ofstream &out);
	void makeFunctions(ofstream &out);
	void makePrivateDefines(ofstream &out);
	void makePublicDefines(ofstream &out);
	void makeStructures(ofstream &out);
};

#endif
