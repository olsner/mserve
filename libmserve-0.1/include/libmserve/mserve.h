/*
 * This file is part of libmserve.
 * 
 * libmserve is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * libmserve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with libmserve; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _libmserve_mserve_H
#define _libmserve_mserve_H

#include <libmserve/common.h>
#include <libmserve/mServe_msg.h>
#include <libmserve/msg_reader.h>

struct mserve;
typedef struct mserve mserve;

enum PlaybackStates
{
	PS_Stopped,
	PS_Playing,
	PS_Paused
};

enum mserve_error
{
	/**
		MSERVE_SUCCESS has a defined value of 0 (zero). This means that checks
		of the type if (mserve_call()) HANDLE_ERROR; are valid.
	*/
	MSERVE_SUCCESS=0,
	MSERVE_E_PIPE,
	MSERVE_E_NOHOST,
	MSERVE_E_FULLQUEUE,
	MSERVE_E_EMPTYQUEUE,
	MSERVE_E_OUTOFDATE,
	MSERVE_E_INVALID
};
typedef enum mserve_error mserve_error;

/**
	NOTE: As of now, the only operation mode is MSERVE_MODE_THREAD. The
	mserve_init function does not accept an operation mode argument.
*/
enum mserve_operation_mode
{
	/**
		A thread is started on mserve_connect, managing the socket and the 
		receive and send queues.
		
		A message notification function may be used to notify the main thread
		in an application specific manner (such as Windows messages etc)
	*/
	MSERVE_MODE_THREAD,
	/**
		No thread is started, all socket I/O is managed by the has_message,
		try_pop_message and wait_pop_message functions
		
		NOTE: This means that you *must* call the has_messsage or one of the
		_pop_message functions regularly! Messages may remain unsent or partially
		sent when the send_message function returns!
	*/
	MSERVE_MODE_ASYNC,
	/**
		No thread is started, send_message and try_pop_message have blocking
		semantics, just as the wait_send_message and wait_pop_message functions
		
		All socket I/O is handled in-line by the *_pop_message, has_message and
		*_send_message functions
	*/
	MSERVE_MODE_BLOCK
};

typedef void (*mserveMessageNotificationFunc) (mserve_error);

#ifdef __cplusplus
extern "C" {
#endif

extern mserve *mserve_init(/*mserve_operation_mode*/);
/**
	Disconnect the socket if it is connected
	Empty input and output queues
	Free all allocated memory associated with the context (messages filled by
	the *_pop_message functions are *not* freed - use mserve_free_message)
*/
extern void mserve_destroy(mserve *);
/**
	Attempt to connect to the remote host and port specified. 
*/
extern mserve_error mserve_connect(mserve *, const char *host, int port);
/**
	Disconnect the socket and terminate the I/O thread. The input queue is
	kept, but the output queue is cleared and associated memory deallocated
	
	Returns:
		The total number of messages in the input queue
*/
extern int mserve_disconnect(mserve *);

/**
	Send a message (put it on the send queue).
	
	The message struct and data is copied when put into the buffer
	
	Returns:
		MSERVE_SUCCESS (0):
			The message was successfully sent
		MSERVE_E_PIPE
			The connection has been closed
		MSERVE_E_FULLQUEUE
			The send queue is full - retry later or use mserve_wait_send_message
*/
extern mserve_error mserve_send_message(mserve *, const mserve_message *);
/**
	The only difference between wait_send_message and send_message is that wait_send_message
	will not return MSERVE_E_FULLQUEUE, but instead will wait for the queue to empty
*/
extern mserve_error mserve_wait_send_message(mserve *, const mserve_message *);
/**
	Check the receive queue for messages
	
	Returns:
		MSERVE_E_SUCCESS
			There are messages in the receive queue
		MSERVE_E_PIPE
			The socket has been disconnected - no messages
		MSERVE_E_EMPTYQUEUE
			The receive queue is empty
*/
extern mserve_error mserve_has_message(mserve *);
/**
	Pop a message from receive queue if it is not empty
	
	The message struct should be freed using mserve_free_message when processing
	is done
	
	Returns:
		MSERVE_E_SUCCESS
			A message was successfully received
		MSERVE_E_PIPE
			The socket has been disconnected and the queue is empty
		MSERVE_E_EMPTYQUEUE
			The receive queue is empty
*/
extern mserve_error mserve_try_pop_message(mserve *, mserve_message *);
/**
	Wait for a message to be pushed on the receive queue and pop it
	
	Similar to the mserve_try_pop_message function, other than that this one
	does not return MSERVE_E_EMPTYQUEUE
*/
extern mserve_error mserve_wait_pop_message(mserve *, mserve_message *);

/**
	The callback will be called by the mserve client thread when a message has
	been received or an error has occured. The function pointer will not be
	checked in any way, it is up to the user programmer to provide correct pointers
*/
extern void mserve_set_message_notification(mserve *, mserveMessageNotificationFunc);


/***********************************************************************
	Convenience/utility functions
************************************************************************/

/**
	Wait until all messages queueud for output have been sent over the network
	(or buffered in OS network buffers - libmserve has no control over these)
	
	Returns:
		MSERVE_SUCCESS (0)
			Success. All queued messages have been sent.
		MSERVE_E_PIPE
			The connection is broken. The output queue has been deallocated.
*/
extern mserve_error mserve_wait_flush(mserve *);
/**
	Take care of a MT_VersionInfo message
	
	msg: The MT_VersionInfo message to be handled
	client_info: The VersionInfo struct describing the client
	verbose: 0: No info printed
			 1: The received VersionInfo message is parsed and printed
	
	Note: Values of verbose other than 0 or 1 are reserved for future use
*/
extern void mserve_handle_VersionInfo(mserve *, const mserve_message *msg, const VersionInfo *client_info, int verbose);
/**
	Build an extension id map from the provided UseExtensions message
	
	verbose: 0: No info printed
			 1: The extension id map and extension versions are printed
	
	Note: Values of verbose other than 0 and 1 are reserved for future use
*/
extern void mserve_register_extensions(mserve *, const UseExtensions *, int verbose);
/**
	Retrieves an extension id from the map created by mserve_register_extensions
*/
extern int mserve_get_extension_id(mserve *, const char *extname);
/**
	Takes an extended message and creates the corresponding ExtendedMessage
	message.
	
	The extname is used to get an extension id from the map created by
	mserve_register_extensions
	
	This function does *not* free any messages.
*/
extern void mserve_create_extended_message(mserve *, mserve_message *, const char *extname, const message *);
/**
	Takes an extended message and creates the corresponding ExtendedMessage
	message.
	
	This function is the same as mserve_create_extended_message, but it uses
	the extension id instead of the name. When sending several messages, a map
	lookup is saved for each message.
	
	This function does *not* free any messages.
*/
extern void mserve_create_extended_message_id(mserve *, mserve_message *, int extid, const message *);

/**
	Create an extended message using the extension's *_msg_create_* function.
	
	Arguments:
		_dst	A pointer to a message struct to hold the newly created message
		_id		The ID of the extension
		_cfunc	The extension's message creation function
		_src	A pointer to the extension's message structure
*/
#define create_ext_message_id(_dst, _id, _cfunc, _src) \
	do { \
		mserve_message _tmp2; \
		ExtendedMessage _em; \
		_cfunc(&_tmp2, _src); \
		_em.ExtensionID=_id; _em.ExtendedType=_tmp2.type; \
		_em.ExtendedData=_tmp2.data; _em.ExtendedDataLength=_tmp2.length; \
		msg_create_ExtendedMessage(_dst, &_em); \
		mserve_free_message(&_tmp2); \
	} while (0)

/**
	Create an extended message using the extension's *_msg_create_* function.
	
	This macro is the same as create_ext_message_id, but takes the extension
	name instead of the ID
*/
#define create_ext_message(_ctx, _dst, _id, _cfunc, _src) \
	create_ext_message_id(_dst, mserve_get_extension_id(_ctx, _id), _cfunc, _src)

/***********************************************************************
	Low-level Message "Synthesis" API
************************************************************************/
extern void mserve_free_message(mserve_message *);
extern void mserve_copy_message(mserve_message *, const mserve_message *);

#ifdef __cplusplus
}
#endif

#endif
