/* THIS IS A GENERATED FILE */
#ifndef _filebrowser_msg_h
#define _filebrowser_msg_h
#include "libmserve/common.h"
#ifdef __cplusplus
extern "C" {
#endif
struct FileListEntry {
	uint8 Type;
	char *Name;
};
typedef struct FileListEntry FileListEntry;
struct GetWD {
	void *allocBuffer;
	uint allocated;
	uint allocUsed;
};
typedef struct GetWD GetWD;
struct SetWD {
	char *Directory;
	void *allocBuffer;
	uint allocated;
	uint allocUsed;
};
typedef struct SetWD SetWD;
struct Iterate {
	uint16 TypeMask;
	void *allocBuffer;
	uint allocated;
	uint allocUsed;
};
typedef struct Iterate Iterate;
struct FileList {
	char *Directory;
	uint FilesCount; FileListEntry *Files;
	void *allocBuffer;
	uint allocated;
	uint allocUsed;
};
typedef struct FileList FileList;
struct IterateDir {
	uint16 TypeMask;
	char *Directory;
	void *allocBuffer;
	uint allocated;
	uint allocUsed;
};
typedef struct IterateDir IterateDir;
#define FileBrowser_EXT_NAME "FileBrowser0001"
#define FileBrowser_EXT_VERSION 1
#define TYPE_DIR 0
#define TYPE_MEDIA 1
#define TYPE_MISC 3
#define TYPE_LINK 2
#define TYPE_MASK_DIR 0x0001
#define TYPE_MASK_MEDIA 0x0002
#define TYPE_MASK_MISC 0x0008
#define TYPE_MASK_ALL 0x000f
#define FileBrowser_MT_GetWD 0x0001
#define FileBrowser_MT_SetWD 0x0002
#define FileBrowser_MT_Iterate 0x0003
#define FileBrowser_MT_FileList 0x0004
#define FileBrowser_MT_IterateDir 0x0005
void fb_msg_create_GetWD(struct message *);
int fb_msg_parse_SetWD(SetWD*, const struct message *);
void fb_msg_free_SetWD(SetWD*);
void fb_msg_create_SetWD(struct message *, const SetWD*);
int fb_msg_parse_Iterate(Iterate*, const struct message *);
void fb_msg_free_Iterate(Iterate*);
void fb_msg_create_Iterate(struct message *, const Iterate*);
int fb_msg_parse_FileList(FileList*, const struct message *);
void fb_msg_free_FileList(FileList*);
void fb_msg_create_FileList(struct message *, const FileList*);
int fb_msg_parse_IterateDir(IterateDir*, const struct message *);
void fb_msg_free_IterateDir(IterateDir*);
void fb_msg_create_IterateDir(struct message *, const IterateDir*);
#ifdef __cplusplus
};
#endif
#endif
