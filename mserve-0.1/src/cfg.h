/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _mServe_config_H
#define _mServe_config_H

#include "common.h"

BEGIN_DECLS

/**
	Initialize the config API, and set the config file path.
*/
extern void cfg_init(const char *path);
/**
	Load/Reload the config file for 
*/
extern void cfg_load();
/**
	cfg_set_string copies the strings, so they should still be deallocated by
	the caller.
*/
extern void cfg_set_string(const char *id, const char *val);
/**
	The same as cfg_set_string, but this one does not replace a prior value,
	and the value set by cfg_set_default is never saved by cfg_save()
*/
extern void cfg_set_default(const char *id, const char *val);
/*extern void cfg_set_int(const char *id, int val);
extern void cfg_set_float(const char *id, float val);
extern void cfg_set_uint(const char *id, uint val);*/
/**
	Get the config value of the specified id, and put it in the specified buffer.
	If bufsize is less than the needed length, the buffer is filled up to the
	specified size and the actual length of the config value is returned.
	
	Returns the length of the config value, or -1 if the value did not exist. A
	zero length is not an error (config ids can have empty values)
*/
extern int cfg_get_string(const char *id, char *buf, uint bufsize);
/**
	Returns the value of id in a newly allocated (with malloc) buffer. The caller
	must free the buffer when done with it.
	
	If the value is not set, NULL is returned.
*/
extern char *cfg_get_string_alloc(const char *id);
/**
	These numeric values are parsed from the string. An invalid string would
	return a value of zero.
*/
extern int cfg_get_int(const char *id);
extern float cfg_get_float(const char *id);
extern uint cfg_get_uint(const char *id);
extern void cfg_save();

/**
	The callback function for cfg_iterate_values()
*/
typedef void (cfg_iterator) (const char *id, const char *value, void *data);
/**
	The callback function for cfg_iterate_values2()
	Returns true (non-zero) if the scan should continue, false/zero if it
	should be aborted
*/
typedef int (cfg_iterator2) (const char *id, const char *value, void *data);

/**
	Iterate all id/value pairs in the config hash. The callback must return
	swiftly, since the locks on the table are held while the iteration is in
	progress
*/
extern void cfg_iterate_values(cfg_iterator *, void *data);
/**
	Return true (non-zero) if the scan was aborted by the callback function,
	false if it completed.
	
	Like the cfg_iterate_values function, the callbacks are called while locks
	are held, so callbacks should complete in a short time.
*/
extern int cfg_iterate_values2(cfg_iterator2 *, void *data);

END_DECLS

#endif
