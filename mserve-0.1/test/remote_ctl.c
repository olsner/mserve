/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <popt.h>
#include <math.h>
#include <libmserve/mserve.h>
#include <testbase.h>

mserve *ctx;
#define CLIENT_NAME "mServe_tests_remotectl"
#define CLIENT_VERSION 1

#define STATUS_UPDATE_MASK 0

enum commands
{
	CMD_PAUSE=1,
	CMD_PLAY,
	CMD_STOP,
	CMD_ENQUEUE,
	CMD_SEEK,
	CMD_SKIP,
};

struct poptOption options[]=
{
	{ "pause", 'u', POPT_ARG_NONE, NULL, CMD_PAUSE, "Pause playback of current song", NULL},
	{ "play", 'p', POPT_ARG_NONE, NULL, CMD_PLAY, "Resume paused playback", NULL},
	{ "stop", 's', POPT_ARG_NONE, NULL, CMD_STOP, "Stop playback of this song (and hold next song)", NULL },
	{ "enqueue", 'e', POPT_ARG_STRING, NULL, CMD_ENQUEUE, "Add file to the playback queue", "path/url"},
	{ "seek", 0, POPT_ARG_STRING, NULL, CMD_SEEK, "Seek in current song", "seconds (float)"},
	{ "next", 'n', POPT_ARG_NONE, NULL, CMD_SKIP, "Skip to next song in queue [MT_Skip]", NULL},
	POPT_AUTOHELP
	POPT_TABLEEND
};

void handle_message(const mserve_message *msg)
{
	mserve_message tmp;
	switch (msg->type)
	{
		case MT_VersionInfo:
			mserve_handle_VersionInfo(ctx, msg, &(VersionInfo){CLIENT_NAME, CLIENT_VERSION, MSERVE_PROTOCOL_VERSION}, 1);
			break;
		case MT_UseProtocolVersion:
			break;
		case MT_QueueNumber:
		{
			QueueNumber qn;
			msg_parse_QueueNumber(&qn, msg);
			printf("MT_QueueNumber: %x\n", qn.Number);
			msg_free_QueueNumber(&qn);
			break;
		}
		default:
			printf("Unhandled message type 0x%04x, length %d\n", msg->type, msg->length);
	}
}

int main(int argc, const char *argv[])
{
	poptContext poptctx;
	int res;

	poptctx=poptGetContext(NULL, argc, argv, options, 0);
	
	mserve_message msg;
	while ((res = poptGetNextOpt(poptctx)) > 0)
	{
		printf("popt res %d\n", res);
		socket_init(&ctx, handle_message);
		switch (res)
		{
			case CMD_PAUSE:
				printf("Pause\n");
				msg_create_Pause(&msg);
				x_send_message(ctx, &msg);
				mserve_free_message(&msg);
				break;
			case CMD_PLAY:
				printf("Resume\n");
				msg_create_Resume(&msg);
				x_send_message(ctx, &msg);
				mserve_free_message(&msg);
				break;
			case CMD_STOP:
				printf("Stop\n");
				msg_create_Stop(&msg);
				x_send_message(ctx, &msg);
				mserve_free_message(&msg);
				break;
			case CMD_ENQUEUE:
			{
				QueueFile qf={(char *)poptGetOptArg(poptctx)};
				msg_create_QueueFile(&msg, &qf);
				x_send_message(ctx, &msg);
				mserve_free_message(&msg);
				break;
			}
			case CMD_SEEK:
			{
				const char *arg=poptGetOptArg(poptctx);
				float seconds=atof(arg);
				uint millis=lround(seconds*1000);
				printf("Seeking to %u millis\n", millis);
				Seek seek={millis};
				msg_create_Seek(&msg, &seek);
				x_send_message(ctx, &msg);
				mserve_free_message(&msg);
				break;
			}
			case CMD_SKIP:
				printf("Next/skip\n");
				msg_create_Skip(&msg);
				x_send_message(ctx, &msg);
				mserve_free_message(&msg);
				break;
		}
		
		mserve_wait_flush(ctx);
		x_empty_inqueue(ctx, handle_message);
	}
	if (res < -1)
	{
		printf("popt error on option \"%s\": %s\n", poptBadOption(poptctx, 0), poptStrerror(res));
		exit(-1);
	}
	poptFreeContext(poptctx);
	
	sleep(1);
	x_empty_inqueue(ctx, handle_message);
	
	return 0;
}
