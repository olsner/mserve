/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include "decode.h"
#include "sine.h"
#include "net/proto.h"
#include "cfg.h"

#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <dlfcn.h>
#include <stdlib.h>

#define sine_extract NULL

decoder_info static_decoders[]=
{
	MAKE_DECODER_INFO("sine", "Sine Generator", sine)
};

#define NUM_STATIC_DECODERS (sizeof(static_decoders)/sizeof(static_decoders[0]))

decoder_info *get_decoder(input_ctx *ctx)
{
	int i;
	char *decode_dir, *filename, *end;
	DIR *dir;
	struct dirent *file;
	
	for (i=0;i<NUM_STATIC_DECODERS;i++)
	{
		decoder_info *ret=&static_decoders[i];
		if (ret->supports(ctx))
			return ret;
	}
	
	cfg_set_default("decode_dir", DEF_DECODE_DIR);
	decode_dir=cfg_get_string_alloc("decode_dir");
	filename=(char *)alloca(strlen(decode_dir)+NAME_MAX+2);
	strcpy(filename, decode_dir);
	end=strchr(filename, 0);
	if (*(end-1) != '/')
		*end++='/';
	dir=opendir(decode_dir);
	free(decode_dir);
	if (!dir)
		return NULL;
	while ((file=readdir(dir)))
	{
		void *handle;
		decoder_get_info_func *get_decoder_info;
		decoder_info *decoder;
		char *tmp;
		
		tmp=strchr(file->d_name, 0);
		if (strncmp(tmp-3, ".so", 3))
			continue;
		memcpy(end, file->d_name, 1+tmp-file->d_name);
		log_printf("Trying %s.", filename);
		
		handle=dlopen(filename, RTLD_LAZY);
		if (handle == NULL) continue;
		get_decoder_info=(decoder_get_info_func *)dlsym(handle, "get_decoder_info");
		if (get_decoder_info == NULL) continue;
		decoder=get_decoder_info();
		
		if (decoder->supports(ctx))
		{
			closedir(dir);
			return decoder;
		}
		else
			dlclose(handle);
	}
	
	closedir(dir);
	return NULL;
}

int decode_ctx_init(decode_ctx *dec_ctx, input_ctx *input, decoder_info *decoder, out_driver_info *output)
{
	if (&dec_ctx->input != input)
		memcpy(&dec_ctx->input, input, sizeof(input_ctx));
	
	dec_ctx->internal=NULL;
	dec_ctx->decoder=decoder;
	dec_ctx->output=output;
	attriblist_init(&dec_ctx->media_info);
	attriblist_set_callback(&dec_ctx->media_info, proto_media_info_callback, NULL);
	memset(&dec_ctx->status0, 0, sizeof(dec_ctx->status0));
	memset(&dec_ctx->status3, 0, sizeof(dec_ctx->status3));
	dec_ctx->state=DS_Running;
	dec_ctx->error=DE_None;
	
	pthread_mutex_init(&dec_ctx->statusUpdateCondMutex, NULL);
	pthread_cond_init(&dec_ctx->statusUpdateCond, NULL);
	
	attriblist_put(&dec_ctx->media_info, "url", dec_ctx->input.url);
	dec_ctx->input.driver->update_info(&dec_ctx->input, &dec_ctx->media_info);
	return decoder->init(dec_ctx);
}

void decode_ctx_destroy(decode_ctx *dec_ctx)
{
	attriblist_destroy(&dec_ctx->media_info);
	pthread_mutex_unlock(&dec_ctx->statusUpdateCondMutex);
	pthread_mutex_destroy(&dec_ctx->statusUpdateCondMutex);
	pthread_cond_destroy(&dec_ctx->statusUpdateCond);
}

void decode_ctx_signal_event(decode_ctx *dec_ctx)
{
	pthread_mutex_lock(&dec_ctx->statusUpdateCondMutex);
	pthread_cond_broadcast(&dec_ctx->statusUpdateCond);
	pthread_mutex_unlock(&dec_ctx->statusUpdateCondMutex);
}

void decode_ctx_wait_event(decode_ctx *dec_ctx)
{
	pthread_mutex_lock(&dec_ctx->statusUpdateCondMutex);
	pthread_cond_wait(&dec_ctx->statusUpdateCond, &dec_ctx->statusUpdateCondMutex);
	pthread_mutex_unlock(&dec_ctx->statusUpdateCondMutex);
}

void cmd_queue_init(DecodeCommandQueue *cmd_queue)
{
	memset(cmd_queue, 0, sizeof(DecodeCommandQueue));	
	pthread_mutex_init(&cmd_queue->mutex, NULL);
}

int cmd_queue_push(DecodeCommandQueue *cmd_queue, const DecodeCommand *cmd)
{
	pthread_mutex_lock(&cmd_queue->mutex);
	if (cmd_queue->len == DC_QUEUELEN)
	{
		pthread_mutex_unlock(&cmd_queue->mutex);
		return -1;
	}
	memcpy(&cmd_queue->queue[cmd_queue->tail], cmd, sizeof(*cmd));
	cmd_queue->tail++;
	cmd_queue->tail %= DC_QUEUELEN;
	cmd_queue->len++;
	pthread_mutex_unlock(&cmd_queue->mutex);
	
	return 0;
}
	
/*int decode_ctx_push_command(decode_ctx *ctx, const DecodeCommand *cmd)
{
	int ret=cmd_queue_push(&ctx->cmd_queue, cmd);
	printf("decode_ctx_push_command: PlaybackPosition is %u\n", cmd->data.PlaybackPosition);
	decode_ctx_signal_event(ctx);
	return ret;
}*/

int cmd_queue_pop(DecodeCommandQueue *cmd_queue, DecodeCommand *cmd)
{
	pthread_mutex_lock(&cmd_queue->mutex);
	if (!cmd_queue->len)
	{
		pthread_mutex_unlock(&cmd_queue->mutex);
		return -1;
	}
	memcpy(cmd, &cmd_queue->queue[cmd_queue->head], sizeof(*cmd));
	cmd_queue->head++;
	cmd_queue->head %= DC_QUEUELEN;
	cmd_queue->len--;
	pthread_mutex_unlock(&cmd_queue->mutex);
	return 0;
}

/*int decode_ctx_pop_command(decode_ctx *ctx, DecodeCommand *cmd)
{
	int ret=cmd_queue_pop(&ctx->cmd_queue, cmd);
	decode_ctx_signal_event(ctx);
	return ret;
}*/
