/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include "buffer.h"

#include "common.h"
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

pthread_mutex_t ob_mutex[1]={PTHREAD_MUTEX_INITIALIZER};

#define LOCK() pthread_mutex_lock(ob_mutex)
#define UNLOCK() pthread_mutex_unlock(ob_mutex)

/*char *buffer;
char *bufferend;
char *begin;
char *end;
uint length;*/
buffer_ctx out_buffer;

void buffer_init(buffer_ctx *ctx, uint size)
{
	char *B=(char *)malloc(size);
	ctx->buffer=B;
	ctx->bufferend=B+size;
	ctx->begin=B;
	ctx->end=B;
	ctx->length=0;
}

void out_buffer_init(uint size)
{
	buffer_init(&out_buffer, size);
}

void buffer_deinit(buffer_ctx *ctx)
{
	free(ctx->buffer);
}

void out_buffer_deinit()
{
	buffer_deinit(&out_buffer);
}

int buffer_push(buffer_ctx *ctx, const char *data, uint datalen)
{
	int wrt=0;
	const char *in=data;
	const char *inend=data+datalen;
	if (ctx->length==(uint)(ctx->bufferend-ctx->buffer)) return 0;
	do
	{
		*ctx->end++=*in++;
		if (ctx->end==ctx->bufferend) ctx->end=ctx->buffer;
	}
	while (ctx->end!=ctx->begin && in!=inend);
	wrt=in-data;
	ctx->length += wrt;
	return wrt;
}

int out_buffer_push(char *data, uint datalen)
{
	//printf("Push: %u bytes\n", datalen);
	LOCK();
	int wrt=buffer_push(&out_buffer, data, datalen);
	UNLOCK();
	//printf("Length now: %u (avlbl %u), wrt %d\n", length, (bufferend-buffer)-length, wrt);
	return wrt;
}

int buffer_pop(buffer_ctx *ctx, char *data, uint datalen)
{
	int rd=0;
	char *out=data;
	char *outend=data+datalen;
	
	if (!ctx->length) return 0;
	do
	{
		*out++=*ctx->begin++;
		if (ctx->begin==ctx->bufferend) ctx->begin=ctx->buffer;
	}
	while (out!=outend && ctx->begin!=ctx->end);
	rd=out-data;
	ctx->length-=rd;
	return rd;
}

int out_buffer_pop(char *data, uint datalen)
{
	//printf("Pop: %d bytes\n", size);
	LOCK();
	int rd=buffer_pop(&out_buffer, data, datalen);
	UNLOCK();
	//printf("Length now: %u\n", length);
	return rd;
}

void buffer_clear(buffer_ctx *ctx)
{
	ctx->begin=ctx->buffer;
	ctx->end=ctx->buffer;
	ctx->length=0;
}

void out_buffer_clear()
{
	LOCK();
	buffer_clear(&out_buffer);
	UNLOCK();
}

uint buffer_get_length(buffer_ctx *ctx)
{
	return ctx->length;
}

uint out_buffer_length()
{
	LOCK();
	uint length=buffer_get_length(&out_buffer);
	UNLOCK();
	return length;
}

uint buffer_get_available(buffer_ctx *ctx)
{
	return (ctx->bufferend-ctx->buffer)-ctx->length;
}

uint out_buffer_available()
{
	LOCK();
	uint avail=buffer_get_available(&out_buffer);
	UNLOCK();
	return avail;
}

void out_buffer_wait_for_push(decode_ctx *ctx, uint datalen)
{
	DecodeState oldstatus=ctx->state;
	while (out_buffer_available() < datalen && ctx->state==oldstatus)
	{
		decode_ctx_wait_event(ctx);
	}
}

void out_buffer_wait_for_pop(decode_ctx *ctx, uint datalen)
{
	DecodeState oldstatus=ctx->state;
	while (out_buffer_length() < datalen && ctx->state==oldstatus)
	{
		decode_ctx_wait_event(ctx);
	}
}
