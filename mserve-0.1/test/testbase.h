/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _TEST_TESTBASE_H
#define _TEST_TESTBASE_H

#include <libmserve/mserve.h>

BEGIN_DECLS

typedef void (*message_handler)(const mserve_message *);

extern void x_empty_inqueue(mserve *, message_handler);
extern void socket_init(mserve **, message_handler);
extern void x_send_message(mserve *, const mserve_message *);
extern void x_wait_message(mserve *, uint, message_handler);
extern void x_wait_extended_message(mserve *, uint extid, uint exttype, message_handler);

END_DECLS

#endif
