/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _mServe_sched_H
#define _mServe_sched_H

#include "common.h"

BEGIN_DECLS

typedef void (*SchedCallback)(client_ctx*, extension_info *, int);

/**
	CALL BEFORE CALLING ANY OTHER sched_ FUNCTION!
	(should be called by main)
*/
extern void sched_init();

extern void sched_lock();
extern void sched_unlock();
extern void sched_set_unsec(uint, client_ctx *, extension_info *, int, SchedCallback);
extern void sched_set(uint, client_ctx *, extension_info *, int, SchedCallback);
extern void sched_clear(client_ctx *, extension_info *, int);
extern void sched_clear_unsec(client_ctx *, extension_info *, int);
extern void sched_clear_all(client_ctx *);
extern void sched_clear_all_unsec(client_ctx *);
extern void sched_run();

END_DECLS

#endif
