/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <pthread.h>
#include <ctype.h>
#include <errno.h>

#include "cfg.h"
#include "common.h"

typedef struct
{
	int isdefault;
	char *value;
} cfg_entry;

const char *cfg_filename="mServe.conf";
GHashTable *cfg_table;
pthread_mutex_t cfg_mutex=PTHREAD_MUTEX_INITIALIZER;

#define LOCK_cfg() pthread_mutex_lock(&cfg_mutex)
#define UNLOCK_cfg() pthread_mutex_unlock(&cfg_mutex)

void cfg_free_entry(void *ptr)
{
	cfg_entry *ent=(cfg_entry *)ptr;
	free(ent);
}

void cfg_init(const char *filename)
{
	cfg_filename=filename;
	cfg_table=g_hash_table_new_full(g_str_hash, g_str_equal, g_free, cfg_free_entry);
}

void cfg_internal_set_string_nolock(const char *id, const char *val, int isdefault)
{
	int len=strlen(val)+1;
	cfg_entry *ent=(cfg_entry *)malloc(sizeof(cfg_entry)+len);
	ent->isdefault=isdefault;
	ent->value=(char *)(ent+1);
	memcpy(ent->value, val, len);
	g_hash_table_insert(cfg_table, g_strdup(id), ent);
}

void cfg_internal_set_string(const char *id, const char *val, int isdefault)
{
	log_printf("\"%s\" = \"%s\"%s", id, val, isdefault?" (default)":"");
	pthread_mutex_lock(&cfg_mutex);
	cfg_internal_set_string_nolock(id, val, isdefault);
	pthread_mutex_unlock(&cfg_mutex);
}

void cfg_set_string(const char *id, const char *val)
{
	cfg_internal_set_string(id, val, 0);
}

const char *cfg_internal_get_string(const char *id)
{
	cfg_entry *ent=(cfg_entry *)g_hash_table_lookup(cfg_table, id);
	if (ent)
		return ent->value;
	else
		return NULL;
}

char *cfg_get_string_alloc(const char *id)
{
	const char *ret=cfg_internal_get_string(id);
	if (ret)
		return strdup(ret);
	else
		return NULL;
}

void cfg_set_default(const char *id, const char *val)
{
	const char *str=cfg_internal_get_string(id);
	if (!str)
		cfg_internal_set_string(id, val, 1);
}

int cfg_get_string(const char *id, char *buf, uint bufsize)
{
	cfg_entry *ent=NULL;
	int ret;
	pthread_mutex_lock(&cfg_mutex);
	ent=(cfg_entry *)g_hash_table_lookup(cfg_table, id);
	if (ent == NULL)
	{
		ret=-1;
	}
	else
	{
		ret=strlen(ent->value);
		strncpy(buf, ent->value, bufsize);
	}
	pthread_mutex_unlock(&cfg_mutex);
	return ret;
}

int cfg_get_int(const char *id)
{
	const char *str;
	int i;
	LOCK_cfg();
	str=cfg_internal_get_string(id);
	i=str?strtol(str, NULL, 0):0;
	UNLOCK_cfg();
	return i;
}

float cfg_get_float(const char *id)
{
	const char *str;
	float f;
	LOCK_cfg();
	str=cfg_internal_get_string(id);
	f=str?strtof(str, NULL):0;
	UNLOCK_cfg();
	return f;
}

uint cfg_get_uint(const char *id)
{
	const char *str;
	uint ui;
	
	LOCK_cfg();
	str=cfg_internal_get_string(id);
	ui=str?strtoul(str, NULL, 0):0;
	UNLOCK_cfg();
	return ui;
}

void cfg_load()
{
	FILE *fp=fopen(cfg_filename, "r");
	char *line=NULL;
	size_t size=0;
	int res;
	
	char *strp;
	char *value;
	char *nmend;
	char *tmp;
	
	if (!fp) return;
	
	pthread_mutex_lock(&cfg_mutex);
	while ((res=getline(&line, &size, fp))!=-1)
	{
		if (res==0) continue;
		strp=line;
		while (*strp && isspace(*strp))
			strp++;
		if (*strp==0 || *strp=='#') continue;
		value=strchr(strp, '=');
		if (value == NULL) continue;
		*value=0;
		nmend=value-1;
		value++;
		while (*value && isspace(*value))
			value++;
		while (nmend>strp && isspace(*nmend))
			nmend--;
		if (nmend==strp) continue;
		if (*value==0) continue;
		
		tmp=strchr(value, '\n');
		if (tmp) *tmp=0;
		
		/*
			at this point:
			strp points to the first non-whitespace character on the line
			value points to the first non-whitespace character after the '=' sign
			nmend points to the last non-whitespace character before the '=' sign
		*/
		
		*(nmend+1)=0;
		log_printf("\"%s\" = \"%s\"", strp, value);
		cfg_internal_set_string_nolock(strp, value, 0);
	}
	fclose(fp);
	pthread_mutex_unlock(&cfg_mutex);
}

void save_scan_cb(void *key, void *value, void *data)
{
	FILE *fp=(FILE *)data;
	cfg_entry *ent=(cfg_entry *)value;
	if (!ent->isdefault)
		fprintf(fp, "%s = %s\n", (char *)key, ent->value);
}

void cfg_save()
{
	FILE *fp=fopen(cfg_filename, "w");
	
	if (!fp)
	{
		log_printf("Opening config file \"%s\": %s", cfg_filename, strerror(errno));
		return;
	}
	
	pthread_mutex_lock(&cfg_mutex);
	g_hash_table_foreach(cfg_table, save_scan_cb, fp);
	pthread_mutex_unlock(&cfg_mutex);
	
	fclose(fp);
}

void cfg_iterate_values(cfg_iterator *iterator, void *data)
{
	pthread_mutex_lock(&cfg_mutex);
	g_hash_table_foreach(cfg_table, (GHFunc)iterator, data);
	pthread_mutex_unlock(&cfg_mutex);
}
