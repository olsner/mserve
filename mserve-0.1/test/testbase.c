/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include "testbase.h"
#include <stdio.h>
#include <stdlib.h>

void x_send_message(mserve *ctx, const mserve_message *msg)
{
	mserve_error res=mserve_send_message(ctx, msg);
	if (res == MSERVE_SUCCESS)
		return;
	else
	{
		perror("x_send_message/mserve_send_message");
		exit(-1);
	}
	mserve_wait_flush(ctx);
}

void x_wait_message(mserve *ctx, uint type, message_handler handle_message)
{
	int res=0;
	mserve_message msg;
	while (!(res=mserve_wait_pop_message(ctx, &msg)))
	{
		handle_message(&msg);
		mserve_free_message(&msg);
		if (msg.type==type) break;
	}
	if (res)
	{
		perror("mserve_wait_pop_message");
		exit(-1);
	}
}

void x_wait_extended_message(mserve *ctx, uint extid, uint exttype, message_handler handle_message)
{
	int res=0;
	mserve_message msg;
	while (!(res=mserve_wait_pop_message(ctx, &msg)))
	{
		handle_message(&msg);
		if (msg.type==MT_ExtendedMessage)
		{
			ExtendedMessage em;
			msg_parse_ExtendedMessage(&em, &msg);
			msg_free_ExtendedMessage(&em);
			if (em.ExtensionID==extid && em.ExtendedType == exttype)
			{
				mserve_free_message(&msg);
				break;
			}
		}
		mserve_free_message(&msg);
	}
	if (res)
	{
		perror("mserve_wait_pop_message");
		exit(-1);
	}
}

void x_empty_inqueue(mserve *ctx, message_handler handle_message)
{
	int res=0;
	mserve_message msg;
	while (!(res=mserve_try_pop_message(ctx, &msg)))
	{
		handle_message(&msg);
		mserve_free_message(&msg);
	}
	if (res != MSERVE_E_EMPTYQUEUE)
	{
		perror("mserve_try_pop_message");
		exit(-1);
	}
}

int socket_is_init=0;
void socket_init(mserve **ctx, message_handler handle_message)
{
	if (socket_is_init) return;
	*ctx=mserve_init();
	if (mserve_connect(*ctx, "localhost", -1))
	{
		perror("mserve_connect");
		exit(1);
	}
	
	int i;
	for (i=0;i<2;i++)
	{
		mserve_message msg;
		mserve_error ret=mserve_wait_pop_message(*ctx, &msg);
		if (ret)
		{
			perror("mserve_wait_pop_message");
			exit(-1);
		}
		handle_message(&msg);
		mserve_free_message(&msg);
	}
	socket_is_init++;
}

