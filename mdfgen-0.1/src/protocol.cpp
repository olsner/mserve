/*
 * This file is part of mdfgen.
 * 
 * mdfgen is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mdfgen is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mdfgen; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Copyright (C) Simon Brenner 2003
 */

#include <libxml/tree.h>

#include <vector>
#include <string>
#include <fstream>
using namespace std;

#include "protocol.hpp"

Protocol::Protocol(xmlNodePtr nod, xmlDocPtr doc)
{
	xmlChar *name;
	xmlChar *byteorder;
	xmlChar *version;
	xmlChar *mtprefix;
	xmlChar *funcprefix;
	
	name=xmlGetProp(nod, (const xmlChar *)"name");
	version=xmlGetProp(nod, (const xmlChar *)"version");
	byteorder=xmlGetProp(nod, (const xmlChar *)"byteorder");
	mtprefix=xmlGetProp(nod, (const xmlChar *)"mtPrefix");
	funcprefix=xmlGetProp(nod, (const xmlChar *)"funcPrefix");
	
	this->name=string((char *)name);
	
	this->version=atoi((char *)version);
	
	if (!byteorder)
		this->byteorder=BO_Host;
	else if (!xmlStrcmp(byteorder, (const xmlChar *)"host"))
		this->byteorder=BO_Host;
	else if (!xmlStrcmp(byteorder, (const xmlChar *)"intel"))
		this->byteorder=BO_Intel;
	else
		this->byteorder=BO_Motorola;
	
	if (mtprefix)
		this->mtprefix=string((char *)mtprefix);
	else
		this->mtprefix="MT_";
	
	if (funcprefix)
		this->funcprefix=string((char *)funcprefix);
	else
		this->funcprefix="msg";
	
	xmlFree(name);
	xmlFree(byteorder);
	xmlFree(version);
	xmlFree(mtprefix);
	xmlFree(funcprefix);
	
	nod=nod->xmlChildrenNode;
	while (nod)
	{
		if (!xmlStrcmp(nod->name, (const xmlChar *)"define"))
			defs.push_back(Define(nod));
		else if (!xmlStrcmp(nod->name, (const xmlChar *)"message"))
			msgs.push_back(Message(nod, this));
		nod=nod->next;
	}
}

void Protocol::makePublicDefines(ofstream &out)
{
	foreach (i, defs)
	{
		out << "#define " << i->name << ' ' << i->value << '\n';
	}
	foreach (i, msgs)
	{
		out << "#define " << mtprefix << i->name << ' ' << i->id << '\n';
	}
}

void Protocol::makeFunctionPrototypes(ofstream &out)
{
	foreach (i, msgs)
	{
		out << i->getPrototypes(funcprefix) << '\n';
	}
}

void Protocol::makeStructures(ofstream &out)
{
	foreach (i, structs)
	{
		out << (*i)->getStructDef() << '\n';
		out << "typedef struct " << (*i)->getName() << ' ' << (*i)->getName() << ";\n";
	}
	foreach (i, msgs)
	{
		out << i->getStructDef() << '\n';
		out << "typedef struct " << (i)->getName() << ' ' << (i)->getName() << ";\n";
	}
}

void Protocol::makeFunctions(ofstream &out)
{
	foreach (i, msgs)
	{
		out << (i)->getParseFunc(funcprefix) << '\n';
		out << (i)->getFreeFunc(funcprefix) << '\n';
		out << (i)->getCreateFunc(funcprefix, mtprefix) << '\n';
	}
}

void Protocol::makePrivateDefines(ofstream &out)
{
	switch (byteorder)
	{
		case BO_Host:
		case BO_Intel:
		case BO_Motorola:
			out << "#define SWAB64(_a) (_a)\n";
			out << "#define SWAB32(_a) (_a)\n";
			out << "#define SWAB16(_a) (_a)\n";
			break;
			break;
	}
	out << "#define parse_int(_in, _dst, _pos, _bits, _type) "
		"do {"
			"req_bytes(_in, _pos, _bits/8);"
			"*_dst=SWAB##_bits(*(_type *)(((char *)_in->data)+*_pos));"
			"*_pos+=_bits/8;"
		"} while (0)\n";
	out << "#define create_int(_out, _src, _pos, _bits, _type) "
		"do {"
			"_type *dst=(_type *)(((char *)_out->data)+*_pos);"
			"*dst=SWAB##_bits(_src);"
			"*_pos+=_bits/8;"
		"} while (0)\n";
	out << "#define SWAB8(_a) (_a)\n";
#define makeForInt(_bts, _tp) \
	out << "#define parse_" _tp "(_in, _dst, _pos) parse_int(_in, _dst, _pos, " _bts ", " _tp ")\n"; \
	out << "#define create_" _tp "(_out, _src, _pos) create_int(_out, _src, _pos, " _bts ", " _tp ")\n";
	makeForInt("64", "uint64");
	makeForInt("32", "uint32");
	makeForInt("16", "uint16");
	makeForInt("8", "uint8");
	out << "#define parse_error() "
		"do { return -1; } while (0)\n";
	out << "#define req_bytes(_in, _pos, _bytes) "
		"do {"
			"if (*(_pos)+_bytes > (_in)->length)"
				"parse_error();"
		"} while (0)\n";
	out << "#define dryAlloc(_bytes) allocNeeded+=_bytes\n";
	out << "#define alloc(_bytes) ({void *_ret=allocBuffer; uint _rbytes=((uint)(_bytes)); allocBuffer=((char*)allocBuffer)+_rbytes; allocUsed+=_rbytes; _ret;})\n";
}

Define::Define(xmlNodePtr nod)
{
	xmlChar *name;
	xmlChar *value;
	
	name=xmlGetProp(nod, (const xmlChar *)"name");
	value=xmlGetProp(nod, (const xmlChar *)"value");
	
	this->name=string((char *)name);
	this->value=string((char *)value);
	
	xmlFree(name);
	xmlFree(value);
}

Message::Message(xmlNodePtr nod, Protocol *proto)
{
	xmlChar *name;
	xmlChar *id;
	
	name=xmlGetProp(nod, (const xmlChar *)"name");
	id=xmlGetProp(nod, (const xmlChar *)"id");
	
	this->name=string((char *)name);
	this->id=string((char *)id);
	
	xmlFree(name);
	xmlFree(id);
	
	nod=nod->xmlChildrenNode;
	while (nod)
	{
		Field::parseFieldNode(nod, this, proto);
		nod=nod->next;
	}
}

string Message::getStructDef()
{
	string ret="struct "+name+" {";
	foreach (i, fields)
	{
		ret+="\n\t"+(*i)->getFieldDef();
	};
	ret+="\n\tvoid *allocBuffer;"
		"\n\tuint allocated;"
		"\n\tuint allocUsed;"
		"\n};";
	return ret;
}

string Message::getPrototypes(const string &fprefix)
{
	string ret="";
	if (fields.size() > 0)
	{
		ret+="int "+fprefix+"_parse_"+name+"("+name+"*, const struct message *);\n";
		ret+="void "+fprefix+"_free_"+name+"("+name+"*);\n";
		ret+="void "+fprefix+"_create_"+name+"(struct message *, const "+name+"*);";
	}
	else
		ret+="void "+fprefix+"_create_"+name+"(struct message *);";
	return ret;
}

string Message::getParseFunc(const string &fprefix)
{
	string ret="";
	if (fields.size()==0)
		return ret;
	ret+="int "+fprefix+"_parse_"+name+"("+name+" *out, const message *in)\n";
	ret+="{\n";
	ret+="\tuint pos=0;\n";
	ret+="\tuint allocNeeded=0;\n";
	foreach (i, fields)
	{
		ret+="\t"+(*i)->getDryParseStmts()+"\n";
	}
	ret+="\tpos=0;\n";
	ret+="\tvoid *allocBuffer;allocBuffer=out->allocBuffer=(allocNeeded?malloc(allocNeeded):NULL);\n";
	ret+="\tout->allocated=allocNeeded;\n";
	ret+="\tuint allocUsed;\n";
	foreach (i, fields)
	{
		ret+="\t"+(*i)->getParseStmts()+"\n";
	}
	ret+="\tout->allocUsed=allocUsed;\n";
	ret+="\treturn 0;\n}";
	return ret;
}

string Message::getCreateFunc(const string &fprefix, const string &mtprefix)
{
	if (fields.size()==0)
	{
			return
				"void "+fprefix+"_create_"+name+"(message *out) {\n" +
					"\tout->length=0;\n" +
					"\tout->type="+mtprefix+name+";\n" +
					"\tout->data=NULL;\n" +
				"}";
	}
	string ret="";
	ret+="void "+fprefix+"_create_"+name+"(message *out, const "+name+"* msg)\n" +
	"{\n" +
	"\tuint pos=0;";
	foreach (i, fields)
		ret+="\t"+(*i)->getDryCreateStmts()+"\n";
	ret+="\tout->data=malloc(pos);\n";
	ret+="\tout->length=pos;\n";
	ret+="\tout->type="+mtprefix+name+";\n";
	ret+="\tpos=0;\n";
	foreach (i, fields)
		ret+="\t"+(*i)->getCreateStmts()+"\n";
	ret+="}";
	return ret;
}

string Message::getFreeFunc(const string &fprefix)
{
	return
	"void "+fprefix+"_free_"+name+"("+name+"* msg)\n"
	"{\n"
	"\tfree(msg->allocBuffer);\n"
	"}";
}

string Message::getName()
{
	return name;
}
