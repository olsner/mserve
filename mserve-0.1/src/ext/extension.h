/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _network_extension_H
#define _network_extension_H

#include "common.h"

struct _extension_info;
struct _client_ctx;
struct ExtendedMessage;

/**
	Context information for a client and an extension. Each client may have one 
	of these for each loaded and enabled extension.
	
	These are allocated by the framework as needed, then given to the
	init_context function for extension specific initialization.
*/
struct _client_extension_ctx
{
	extension_info *extension;
	client_ctx *client;
	/**
		The extension version as negotiated with this client. An extension
		is assumed to accept all extension versions below the value in the
		extension_info struct
	*/
	uint NegotiatedVersion;

	uint StatusUpdateIntervals[32];
	void *internal;
};

struct _extension_info
{
	int ExtensionID;
	char ExtensionName[16];
	uint32 ExtensionVersion;
	
	void *internal;
	
	/**
		Called by the framework when the client_extension_ctx struct is created.
		The extension and client fields of the client_extension_ctx are initialized
		prior to entering this function.
	*/
	void (*init_context)(client_extension_ctx *);
	/**
		Called by the framework when a client_extension_ctx is not needed anymore
		
		This function will not be called on a context that has not been passed
		to init_context
	*/
	void (*destroy_context)(client_extension_ctx *);
	/**
		Here, msg contains the ExtendedType, length of ExtendedData and
		ExtendedData. The message should not be freed.
	*/
	void (*message)(client_extension_ctx *, mserve_message *msg);
	/**
		Called by the framework to notify the extension that status updates
		have been altered. The client_extension_ctx does not need to be updated
		by this function.
	*/
	void (*set_status_updates)(client_extension_ctx *, uint StatusUpdateMask, uint Interval);
	/**
		Send a status update to the client. The id is an index into the
		StatusUpdateIntervals array.
	*/
	void (*send_status_update)(client_extension_ctx *, uint id);
};

/**
	The function should return a NULL-terminated list of pointers to
	extension_info structs
*/
typedef extension_info **(get_extension_info_func)();

BEGIN_DECLS

/**
	Initialize global data. Must be called before any of the other extension
	functions are called.
*/
extern void extension_init();
/**
	Loads the file specified by extension_dir/basename.so, where extension_dir
	is the configured path for loading extensions.
	
	When the .so file is loaded, the function get_extension_info from the .so
	file is called and all extensions returned are registered
	
	Returns -1 if there was an error, the number of extensions loaded from the
	file otherwise.
*/
extern int load_extensions(const char *basename);
/**
	Returns the extension_info struct for the extension with the
	specified extension name (the 16-character id)
	
	If there is no such extension registered, this function returns NULL
*/
extern extension_info *get_extension_by_name(const char *extname);
/**
	Return the extension_info struct for the extension with the	specified 
	protocol ID, as registererd by register_extension_id
	
	If there is no such extension registered, this function returns NULL
*/
extern extension_info *get_extension_by_id(int id);
/**
	Allocate a new protocol ID and associate the provided extension with it.
	If the extension already has an associated protocol ID, that ID is returned.
	
	Returns: The newly allocated protocol ID or -1 if there are no extension
	ID's available.
*/
extern int register_extension_newid(extension_info *);

/* These callbacks are primarily intended for internal use within mServe */
extern void extension_dispatch_message(client_ctx *, ExtendedMessage *);
extern void extension_sched_callback(client_ctx *, extension_info *, int id);
extern void extension_set_status_updates(client_ctx *, int extid, uint mask, uint interval);

extern void extension_set_client_extension_version(client_ctx *, extension_info *, uint);
extern void extension_free_client(client_ctx *);

/* These functions are intended for use by extension_info callbacks */
/**
	The mserve_message contains the *extended* message type, length and data
	The function will add all the ExtendedMessage stuffing needed.
*/
extern void extension_buffer_message(client_extension_ctx *, const mserve_message *);
extern client_extension_ctx *extension_get_client_context(client_ctx *, extension_info *);

END_DECLS

#endif
