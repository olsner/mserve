/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include "cfg.h"
#include "common.h"
#include "out_driver.h"
#include "buffer.h"
#include "decode/decode.h"

#include <unistd.h>
#include <fcntl.h>
#include <alsa/asoundlib.h>

int alsa_init(const char*);
void alsa_deinit();
int alsa_set_format(int freq, SampleFormat fmt, int channels);
int alsa_output(decode_ctx *, void *buffer, unsigned int size);
int alsa_get_preferred_chunk_size();
void alsa_set_volume(int volume);
int alsa_get_volume();

#define CHK_ERR(_stmt, _errmsg) do {if (_stmt < 0) { printf(_errmsg); return -1; }} while (0)

out_driver_info alsa_info =
{
	"alsa",
	"Advanced Linux Sound Architecture mServe output driver",
	alsa_init,
	alsa_deinit,
	alsa_set_format,
	alsa_output,
	alsa_get_preferred_chunk_size,
	NULL,
	NULL
};

snd_pcm_t *handle;
int chunk_bytes=0;
int chunk_frames;
int bps;
int has_format;
int g_freq;

pthread_mutex_t cond_mutex=PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond=PTHREAD_COND_INITIALIZER;

out_driver_info * out_driver_get_info()
{
	return &alsa_info;
}

int alsa_init(const char *dev)
{
	CHK_ERR(
		snd_pcm_open(&handle, dev?dev:"default", SND_PCM_STREAM_PLAYBACK, 0),
		"Error opening ALSA dev\n");
	has_format=0;
	return 0;
}

void alsa_deinit()
{
	snd_pcm_drain(handle);
	snd_pcm_close(handle);
	has_format=0;
}

int alsa_set_format(int freq, SampleFormat sfmt, int channels)
{
	enum _snd_pcm_format alsa_format[]={
		SND_PCM_FORMAT_U16_BE,
		SND_PCM_FORMAT_S16_BE,
		SND_PCM_FORMAT_U16_LE,
		SND_PCM_FORMAT_S16_LE,
		SND_PCM_FORMAT_U8,
		SND_PCM_FORMAT_S8
	};

	snd_pcm_hw_params_t *hwparams;
	snd_pcm_hw_params_alloca(&hwparams);
	CHK_ERR(
		snd_pcm_hw_params_any(handle, hwparams),
		"Can not configure this device\n");
	CHK_ERR(
		snd_pcm_hw_params_set_access(handle, hwparams, SND_PCM_ACCESS_RW_INTERLEAVED),
		"Error setting access mode\n");
	CHK_ERR(
		snd_pcm_hw_params_set_format(handle, hwparams, alsa_format[sfmt]),
		"Error setting format\n");
	CHK_ERR(
		snd_pcm_hw_params_set_rate_near(handle, hwparams, freq, 0),
		"Error setting sample rate\n");
	CHK_ERR(
		snd_pcm_hw_params_set_channels(handle, hwparams, channels),
		"Error setting channels\n");
	CHK_ERR(
		snd_pcm_hw_params(handle, hwparams),
		"Error setting HW params\n");
	bps=channels*(sfmt<SFMT_8_U?2:1);
	g_freq=freq;
	chunk_frames=snd_pcm_hw_params_get_period_size(hwparams, 0);
	chunk_frames*=snd_pcm_hw_params_get_periods(hwparams, 0)/8;
	chunk_bytes=chunk_frames*bps;
	has_format=1;
	pthread_mutex_lock(&cond_mutex);
	pthread_cond_broadcast(&cond);
	pthread_mutex_unlock(&cond_mutex);
	return 0;
}

int alsa_get_preferred_chunk_size()
{
	return has_format?chunk_bytes:0;
}

int alsa_output(decode_ctx *ctx, void *buf, uint size)
{
	int res;
	//if (!has_format) return 0;
	fflush(stdout);
	while ((res = snd_pcm_writei(handle, buf, size/bps)) < 0)
	{
		snd_pcm_prepare(handle);
		printf("alsa_output: buffer underrun\n");
	}
	ctx->status0.OutputBytes+=res;
	ctx->status0.OutputMillis+=(res*1000)/(g_freq);
	return res*bps;
}
