/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _mServe_log_H
#define _mServe_log_H

#include "common.h"
#include <glib.h>

/*typedef enum _LogLevels
{
	LOG_CRITICAL,
	LOG_ERROR,
	LOG_WARNING,
	LOG_INFO,
	LOG_DEBUG,
} LogLevels;*/

BEGIN_DECLS

/**
	buf must be at least 12 chars big
*/
extern char *getsympid(char *buf);
/**
	Note that this will only keep 11 chars of str (12 with terminating null)
*/
extern void regsympid(const char *str);

END_DECLS

#define PRE_FMT "[%s] %s(): "
#define PRE_ARGS getsympid(bf), __FUNCTION__/*, __FILE__, __LINE__*/
#define POST_FMT "%s\n"
#define POST_ARGS ""

#define log_print(_msg) log_printf("%s", _msg)
#ifdef G_HAVE_ISO_VARARGS
#define log_printf(_fmt, ...) do { char bf[12]; fprintf(stderr, PRE_FMT _fmt POST_FMT, PRE_ARGS, __VA_ARGS__, POST_ARGS); } while (0)
#elif defined(G_HAVE_GNUC_VARARGS)
#define log_printf(_fmt, _args...) do { char bf[12]; fprintf(stderr, PRE_FMT _fmt POST_FMT, PRE_ARGS, _args, POST_ARGS); } while (0)
#endif

#endif
