/*
 * This file is part of libmserve.
 * 
 * libmserve is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * libmserve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with libmserve; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _libmserve_internal_H
#define _libmserve_internal_H

#include "libmserve/mserve.h"

#include <deque>
#include <map>
using namespace std;

struct extname
{
	char str[16];
	
	inline extname(const char *nm)
	{
		strncpy(str, nm, 16);
	}
	
	inline operator const char *() const
	{ return str; }
	
	inline bool operator <(const extname &o) const
	{
		return strncmp(str, o, 16)<0;
	}
};

struct mserve
{
	deque <mserve_message> in;
	pthread_mutex_t in_mutex;
	deque <mserve_message> out;
	pthread_mutex_t out_mutex;
	map <extname, int> extname_map;
	pthread_mutex_t extname_map_mutex;
	
	pthread_cond_t cond;
	pthread_mutex_t cond_mutex;
	
	int fd;
	int ctl_fd[2];
	pthread_t thread;
	bool threadStarted;
	
	msg_reader_ctx msg_reader;
	
	mserve_message *writemsg;
	uint writepos;

private:
	void read_cb();
	void write_cb();
	
	void disconnect_socket();
	void clear_queue(deque <mserve_message> &);
public:
	void client_thread();

	mserve();
	~mserve();

	mserve_error connect(const char *, int);
	int disconnect();
	
	mserve_error send_message(const mserve_message *);
	
	mserve_error has_message();
	mserve_error try_pop_message(mserve_message *);
	mserve_error wait_pop_message(mserve_message *);
	
	mserve_error wait_flush();
	
	bool check_socket();
	
	void msg_reader_callback(uint, uint, void *);
	void register_extensions(const UseExtensions *, int);
	int get_extension_id(const char *);
	
	inline void lock_in()
	{	pthread_mutex_lock(&in_mutex); }
	inline void clear_in()
	{
		lock_in();
		clear_queue(in);
		unlock_in();
	}
	inline void unlock_in()
	{	pthread_mutex_unlock(&in_mutex); }
	
	inline void lock_out()
	{	pthread_mutex_lock(&out_mutex); }
	inline void clear_out()
	{
		lock_out();
		clear_queue(out);
		unlock_out();
	}
	inline void unlock_out()
	{	pthread_mutex_unlock(&out_mutex); }
	
	inline void lock_extname_map()
	{	pthread_mutex_lock(&extname_map_mutex); }
	inline void unlock_extname_map()
	{	pthread_mutex_unlock(&extname_map_mutex); }
	
	inline void cond_wait()
	{
		pthread_mutex_lock(&cond_mutex);
		pthread_cond_wait(&cond, &cond_mutex);
		pthread_mutex_unlock(&cond_mutex);
	}
	inline void cond_signal()
	{
		pthread_mutex_lock(&cond_mutex);
		pthread_cond_broadcast(&cond);
		pthread_mutex_unlock(&cond_mutex);
	}
};

#endif
