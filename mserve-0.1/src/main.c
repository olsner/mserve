/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <popt.h>
#include <math.h>
#include <signal.h>

#include "queue.h"
#include "cfg.h"
#include "out/out_driver.h"
#include "input/input.h"
#include "decode/decode.h"
#include "out/buffer.h"
#include "net/net.h"
#include "net/client.h"
#include "ext/extension.h"
#include "msched.h"

pthread_t decode_thread;
pthread_t scheduler_thread;
decode_ctx *current_decode_ctx=NULL;

const char *cfg_keys[]=
{
	"",
	"",//"out_device",
	"out_driver",
	"out_driver_dir"
};

struct poptOption options[]={
	//{ "out-device", 'o', POPT_ARG_STRING, NULL, 1, "specify output device", "device" },
	{ "out-driver", 0, POPT_ARG_STRING, NULL, 2, "specify output driver [help == list drivers]", "driver" },
	{ "od", 0, POPT_ARG_STRING|POPT_ARGFLAG_ONEDASH, NULL, 2, "alias for --out-driver", "driver" },
	{ "out-driver-dir", 0, POPT_ARG_STRING|POPT_ARGFLAG_SHOW_DEFAULT, NULL, 3, "specify directory to search for output plugins", "path" },
	POPT_AUTOHELP
	POPT_TABLEEND
};

void *decode_thread_main(void *data)
{
	regsympid("DEC");
	log_print("Decode thread started");
	decode_ctx *ctx=(decode_ctx *)data;
	ctx->status0.PlaybackState=PS_Playing;
	int res=ctx->decoder->decode_run(ctx);
	log_printf("ctx->run(): %d", res);
	ctx->state=DS_DecodeComplete;
	decode_ctx_signal_event(ctx);
	out_driver_join_thread();
	return NULL;
}

void start_decode_thread(decode_ctx *ctx)
{
	pthread_create(&decode_thread, NULL, decode_thread_main, ctx);
}

void join_decode_thread()
{
	pthread_join(decode_thread, NULL);
}

decode_ctx *get_current_decode_ctx()
{
	return current_decode_ctx;
}

void *scheduler_thread_main(void *arg) {
	sched_run();
	return NULL;
}

void start_scheduler_thread()
{
	pthread_create(&scheduler_thread, NULL, scheduler_thread_main, NULL);
}

void sigint_handler(int i)
{
	cfg_save();
	signal(SIGINT, SIG_DFL);
	kill(getpid(), SIGINT);
}

void sigterm_handler(int i)
{
	cfg_save();
	signal(SIGTERM, SIG_DFL);
	kill(getpid(), SIGTERM);
}

int main(int argc, const char *argv[])
{
	poptContext poptctx;
	int res;
	
	regsympid("MAIN");
	
	cfg_init(CONF_DIR "/mServe.conf");
	cfg_load();
	
	extension_init();
	extension_info *ext=get_extension_by_name("FileBrowser0001");
	log_printf("FileBrowser0001 extension_info: %p\n", ext);
	
	sched_init();
	start_scheduler_thread();
	
	signal(SIGINT, sigint_handler);
	signal(SIGTERM, sigterm_handler);

	poptctx=poptGetContext(NULL, argc, argv, options, 0);
	
	while ((res=poptGetNextOpt(poptctx))>0)
	{
		const char *str=poptGetOptArg(poptctx);
		if (str)
		{
			log_printf("Args: Set conf \"%s\" to \"%s\"\n", cfg_keys[res], str);
			cfg_set_string(cfg_keys[res], str);
		}
	}
	
	if (res < -1)
	{
		printf("popt error on option \"%s\": %s\n", poptBadOption(poptctx, 0), poptStrerror(res));
		exit(-1);
	}
	log_print("Main thread");
	//printf("out-device: %s\n", cfg::out_device);
	//printf("out-driver: %s\n", cfg::out_driver);
	//printf("out-driver-dir: %s\n", cfg::out_driver_dir);
	const char **files=poptGetArgs(poptctx);
	if (files)
	{
		int i;
		for (i=0;files[i];i++)
		{
			uint qn=queue_push(files[i]);
			log_printf("Q-File \"%s\", Q# %u", files[i], qn);
		}
	}
	poptFreeContext(poptctx);
	
	cfg_set_default("out_driver", "OSS");
	
	out_driver_init();
	net_thread_start();
	
	while (1)
	{
		qent queue_entry;
		
		queue_lock();
		queue_wait_unsec();
		queue_pop_unsec(&queue_entry);
		queue_unlock();
		log_printf("Q-entry: \"%s\" (%x)\n", queue_entry.url, queue_entry.number);
		
		decode_ctx dec_ctx[1];
		input_ctx *input=&dec_ctx->input;
		decoder_info *decoder;
		
		init_input_context(input, queue_entry.url);
		input->driver->init(input);
		decoder=get_decoder(input);
		
		char *cfg_out_driver=cfg_get_string_alloc("out_driver");
		if (!cfg_out_driver)
		{
			printf("No output driver!\n");
			return 1;
		}
		out_driver_info *out_driver=get_out_driver(cfg_out_driver);
		char *drv=strchr(cfg_out_driver, ':');
		out_driver->init(drv?++drv:NULL);
		
		res=decode_ctx_init(dec_ctx, input, decoder, out_driver);
		log_printf("decode_ctx_init: %d\n", res);
		res=decoder->extract(input, &dec_ctx->media_info);
		log_printf("decoder->extract: %d\n", res);
		char *mi_url=attriblist_get_dup(&dec_ctx->media_info, "url");
		//printf("dec_ctx->media_info[\"url\"] == \"%s\"\n", mi_url);
		free(mi_url);
		
		log_printf("out_buffer size will be %d", OUT_BUFFER_SZ);
		out_buffer_init(OUT_BUFFER_SZ);
		out_driver_run_thread(dec_ctx);
		
		current_decode_ctx=dec_ctx;
		start_decode_thread(dec_ctx);
		
		while (dec_ctx->state != DS_Complete)
		{
			decode_ctx_wait_event(dec_ctx);
			//Status2 status2;
			/*uint ib=dec_ctx->status0.InputBytes;
			uint ims=dec_ctx->status0.InputMillis;
			uint ob=dec_ctx->status0.OutputBytes;
			uint oms=dec_ctx->status0.OutputMillis;
			uint pbp=dec_ctx->status0.PlaybackPosition;
			printf("\r%2u:%02u.%03u I%2u:%02u.%03u O%2u:%02u.%03u (%u/%u)", pbp/60000, (pbp/1000)%60, pbp%1000, ims/60000, (ims/1000)%60, ims%1000, oms/60000, (oms/1000)%60, oms%1000, ib, ob);
			fflush(stdout);*/
		}
		log_print("Playback complete");
		
		join_decode_thread();
		log_print("Decode thread joined");
		
		out_buffer_deinit();
		dec_ctx->decoder->deinit(dec_ctx);
		decode_ctx_destroy(dec_ctx);
		current_decode_ctx=NULL;
		out_driver->deinit();
		input->driver->deinit(input);
		queue_free_qent(&queue_entry);
		free(cfg_out_driver);
	}
}
