/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include "input.h"
#include "null_input.h"

int null_supports(const char *url)
{
	return 1;
}

int null_init(input_ctx *ctx)
{
	return 0;
}

void null_deinit(input_ctx *ctx) {}

int null_read(input_ctx *ctx, void *data, uint length)
{
	return length;
}

int null_isactive(input_ctx *ctx)
{
	return 1;
}

void null_update_info(input_ctx *ctx, attriblist *mi)
{
}

int64 null_get_media_length(input_ctx *ctx)
{
	return -1;
}
int64 null_seek(input_ctx *ctx, int64 off, int whence)
{
	return -1;
}
int64 null_tell(input_ctx *ctx)
{
	return -1;
}
