/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <popt.h>
#include <string.h>
#include <libmserve/mserve.h>
#include <testbase.h>

mserve *ctx;
int verbosity=1;
#define CLIENT_NAME "mServe_conf"
#define CLIENT_VERSION 1

#define STATUS_UPDATE_MASK 0

enum commands
{
	SET=1,
	GET,
	GET_ALL,
	SAVE,
	RELOAD
};

struct poptOption options[]=
{
	{ "set", 's', POPT_ARG_STRING, NULL, SET, "Set a config value", NULL},
	{ "get", 'g', POPT_ARG_STRING, NULL, GET, "Get a config value", NULL},
	{ "get-all", 'a', POPT_ARG_NONE, NULL, GET_ALL, "Get all config values", NULL},
	{ "quiet", 'q', POPT_ARG_VAL, &verbosity, 0, "Suppress non-error messages", NULL},
	{ "verbose", 'v', POPT_ARG_VAL, &verbosity, 1, "Be verbose", NULL},
	POPT_AUTOHELP
	POPT_TABLEEND
};

void handle_message(const mserve_message *msg)
{
	mserve_message tmp;
	switch (msg->type)
	{
		case MT_VersionInfo:
			mserve_handle_VersionInfo(ctx, msg, &(VersionInfo){CLIENT_NAME, CLIENT_VERSION, MSERVE_PROTOCOL_VERSION}, verbosity);
			break;
		case MT_UseProtocolVersion:
			break;
		case MT_SetConfig:
		{
			SetConfig sc;
			msg_parse_SetConfig(&sc, msg);
			uint i;
			for (i=0;i<sc.ValuesCount;i++)
			{
				printf("%s=\"%s\"\n", sc.Values[i].Name, sc.Values[i].Value);
			}
			msg_free_SetConfig(&sc);
			break;
		}
		case MT_EndOfResponse:
			printf("--- end\n");
			break;
		default:
			if (verbosity)
				printf("Unhandled message type %04x, length %04x\n", msg->type, msg->length);
	}
}

int main(int argc, const char *argv[])
{
	poptContext poptctx;
	int res;

	poptctx=poptGetContext(NULL, argc, argv, options, 0);
	
	mserve_message msg;
	while ((res = poptGetNextOpt(poptctx)) > 0)
	{
		socket_init(&ctx, handle_message);
		switch (res)
		{
			case SET:
			{
				const char *arg=poptGetOptArg(poptctx);
				char *argcp=strdup(arg);
				char *val=strchr(argcp, '=');
				if (val == NULL)
				{
					fprintf(stderr, "Invalid argument for --set/-s: \"%s\". Expected a '=' sign\n", arg);
					free(argcp);
					break;
				}
				*val=0;
				val++;
				
				if (verbosity)
					printf("Setting \"%s\" to \"%s\"\n", argcp, val);
				
				ConfigValuePair cvp[]={{argcp, val}};
				SetConfig sc={1, cvp};
				msg_create_SetConfig(&msg, &sc);
				x_send_message(ctx, &msg);
				mserve_free_message(&msg);
				msg.type=MT_Ping;
				msg.length=0;
				x_send_message(ctx, &msg);
				x_wait_message(ctx, MT_Pong, handle_message);
				free(argcp);
				break;
			}
			case GET:
			{
				const char *arg=poptGetOptArg(poptctx);
				
				if (verbosity)
					printf("Getting \"%s\"\n", arg);
				
				ConfigName cn[]={{(char *)arg}};
				GetConfig gc={1, cn};
				msg_create_GetConfig(&msg, &gc);
				x_send_message(ctx, &msg);
				mserve_free_message(&msg);
				x_wait_message(ctx, MT_SetConfig, handle_message);
				break;
			}
			case GET_ALL:
				msg_create_GetAllConfig(&msg);
				x_send_message(ctx, &msg);
				mserve_free_message(&msg);
				printf("--- start\n");
				x_wait_message(ctx, MT_EndOfResponse, handle_message);
				break;
		}
	}
	if (res < -1)
	{
		printf("popt error on option \"%s\": %s\n", poptBadOption(poptctx, 0), poptStrerror(res));
		exit(-1);
	}
	poptFreeContext(poptctx);
	
	return 0;
}
