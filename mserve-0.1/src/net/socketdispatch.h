/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _socketdispatch_H
#define _socketdispatch_H

#include "common.h"
#include <glib.h>

#ifdef _NETWORK_INTERNALS
/*extern GHashTable *fd_ctx_map;
#include <pthread.h>
extern pthread_mutex_t fd_ctx_map_mutex;*/
#endif

typedef enum _SocketNotifyEvent
{
	SNE_NULL,
	SNE_READ,
	SNE_WRITE,
	SNE_EXCEPTION,
	SNE_ERROR,
	SNE_DELETED
} SocketNotifyEvent;

typedef void (*SocketNotifyCallback) (socket_ctx *, SocketNotifyEvent event);
typedef int (*SocketWantsWriteCallback) (socket_ctx *);

struct _socket_ctx
{
	/**
		Must be either NULL or pointing to valid functions
		If read or exception is not NULL, the socket is selected for the
		corresponding events.
		If write is not-NULL and wantswrite is either NULL or returning TRUE,
		the socket is selected for write events.
	*/
	SocketNotifyCallback read;
	SocketNotifyCallback write;
	/**
		See the select documentation
	*/
	SocketNotifyCallback exception;
	SocketNotifyCallback error;
	SocketNotifyCallback deleted;
	
	void *userdata;
	
	int fd;
	int available;
	int lasterr;
	
	SocketWantsWriteCallback wantswrite;
};

BEGIN_DECLS

/**
	Attempt to find the socket context for the specified file descriptor
	This is the way to get instances of socket_ctx's
*/
extern socket_ctx *socket_ctx_from_fd(int fd);
extern void socket_ctx_delete(socket_ctx *);
typedef void (*foreach_cb)(int fd, socket_ctx *, void *data);
extern void socket_ctx_foreach(foreach_cb, void *data);
/**
	Wait for socket events, with a timeout as specified. For an infinite
	timeout, use a NULL pointer.
	
	Returns 0 on success, or -1 on failure
*/
extern int socket_dispatch(struct timeval *timeout);

END_DECLS

#endif
