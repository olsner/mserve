/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <glib.h>
#include <pthread.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <dlfcn.h>

#include <libmserve/mserve.h>
#include "extension.h"
#include "net/client.h"
#include "cfg.h"

#define LOCK(_glbl) pthread_mutex_lock(&(_glbl ## _mutex))
#define UNLOCK(_glbl) pthread_mutex_unlock(&(_glbl ## _mutex))

typedef struct
{
	client_ctx *client;
	extension_info *ext;
} ce_ctx_key;

GHashTable *extension_names;
GHashTable *ce_ctx_map;
GPtrArray *extension_ids;

pthread_mutex_t extension_names_mutex=PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t extension_ids_mutex=PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t ce_ctx_map_mutex=PTHREAD_MUTEX_INITIALIZER;

guint extname_hash(gconstpointer v)
{
	char *p=(char *)v;
	guint ret=0;
	int i;
	for (i=0;i<16;i++)
	{
		ret=ret<<5+ret+p[i];
	}
	return ret;
}

gboolean extname_equal(gconstpointer v1, gconstpointer v2)
{
	return memcmp(v1, v2, 16)==0;
}

guint ce_ctx_key_hash(gconstpointer v)
{
	const ce_ctx_key *key=(const ce_ctx_key *)v;
	return ((guint)key->client) ^ ((guint)key->ext);
}

gboolean ce_ctx_key_equal(gconstpointer v1, gconstpointer v2)
{
	const ce_ctx_key *key1=(const ce_ctx_key *)v1;
	const ce_ctx_key *key2=(const ce_ctx_key *)v2;
	
	return key1->client==key2->client && key1->ext==key2->ext;
}

void free_ce_ctx(void *ptr)
{
	client_extension_ctx *ctx=(client_extension_ctx *)ptr;
	ctx->extension->destroy_context(ctx);
	free(ctx);
}

void extension_init()
{
	extension_names=g_hash_table_new(extname_hash, extname_equal);
	ce_ctx_map=g_hash_table_new_full(ce_ctx_key_hash, ce_ctx_key_equal, free, free_ce_ctx);
	extension_ids=g_ptr_array_new();
	cfg_set_default("extension_dir", DEF_EXTENSION_DIR);
	
	char *cfg_load_extensions=cfg_get_string_alloc("load_extensions");
	if (cfg_load_extensions)
	{
		char *p=cfg_load_extensions;
		char *lp=p;
		char *cfgend=p+strlen(p);
		while (p <= cfgend)
		{
			if (*p == ':' || *p == 0)
			{
				*p=0;
				load_extensions(lp);
				lp=p+1;
			}
			p++;
		}
		free(cfg_load_extensions);
	}
}

void int_register_extension_name(extension_info *ext)
{
	ext->ExtensionID=-1;
	LOCK(extension_names);
	g_hash_table_insert(extension_names, ext->ExtensionName, ext);
	UNLOCK(extension_names);
}

extension_info *get_extension_by_name(const char *extname)
{
	extension_info *ret;
	LOCK(extension_names);
	ret=(extension_info *)g_hash_table_lookup(extension_names, extname);
	UNLOCK(extension_names);
	return ret;
}

extension_info *get_extension_by_id(int id)
{
	LOCK(extension_ids);
	if (id > extension_ids->len || id < 0)
	{
		return NULL;
	}
	extension_info *ret=(extension_info *)g_ptr_array_index(extension_ids, id);
	UNLOCK(extension_ids);
	return ret;
}

int register_extension_newid(extension_info *ext)
{
	int ret;
	LOCK(extension_ids);
	if (ext->ExtensionID >= 0 && ext->ExtensionID < extension_ids->len
		&& g_ptr_array_index(extension_ids, ext->ExtensionID)==ext)
		ret=ext->ExtensionID;
	else
	{
		if (extension_ids->len == 65536) return -1;
		g_ptr_array_add(extension_ids, ext);
		ret=extension_ids->len-1;
		ext->ExtensionID=ret;
	}
	UNLOCK(extension_ids);
	return ret;
}

int load_extensions(const char *basename)
{
	char *ext_dir=cfg_get_string_alloc("extension_dir");
	int ext_dir_l=strlen(ext_dir);
	int basename_l=strlen(basename);
	int pathlen=ext_dir_l+basename_l+8;
	char *path=(char *)alloca(pathlen);
	memcpy(path, ext_dir, ext_dir_l);
	memcpy(path+ext_dir_l, "/lib", 4);
	memcpy(path+ext_dir_l+4, basename, basename_l);
	memcpy(path+ext_dir_l+4+basename_l, ".so", 4);
	
	free(ext_dir);
	
	void *dl_ctx=dlopen(path, RTLD_NOW);
	if (dl_ctx == NULL)
	{
		log_printf("File (%s) failed at dlopen: %s\n", path, dlerror());		
		return -1;
	}
	
	get_extension_info_func *get_extension_info=(get_extension_info_func *)dlsym(dl_ctx, "get_extension_info");
	if (get_extension_info == NULL)
	{
		log_printf("File (%s) failed at dlsym: %s\n", path, dlerror());
		return -1;
	}
	
	extension_info **exts=get_extension_info();
	
	int n=0;
	for (;exts[n];n++)
	{
		int_register_extension_name(exts[n]);
	}
	return n;
}

void extension_dispatch_message(client_ctx *client, ExtendedMessage *em)
{
	mserve_message msg;
	msg.type=em->ExtendedType;
	msg.length=em->ExtendedDataLength;
	msg.data=em->ExtendedData;
	
	extension_info *ext=get_extension_by_id(em->ExtensionID);
	if (ext == NULL)
	{
		log_printf("Invalid extension (0x%04x)\n", em->ExtensionID);
		return;
	}
	client_extension_ctx *ce_ctx=extension_get_client_context(client, ext);
	
	ext->message(ce_ctx, &msg);
}

void extension_set_client_extension_version(client_ctx *client, extension_info *ext, uint ExtensionVersion)
{
	client_extension_ctx *ctx=extension_get_client_context(client, ext);
	ctx->NegotiatedVersion=ExtensionVersion;
}

client_extension_ctx *extension_get_client_context(client_ctx *client, extension_info *ext)
{
	ce_ctx_key key={client, ext};
	LOCK(ce_ctx_map);
	client_extension_ctx *ce=(client_extension_ctx *)g_hash_table_lookup(ce_ctx_map, &key);
	if (ce == NULL)
	{
		ce=(client_extension_ctx *)malloc(sizeof(client_extension_ctx));
		memset(ce, sizeof(client_extension_ctx), 0);
		ce->extension=ext;
		ce->client=client;
		ce_ctx_key *nkey=(ce_ctx_key *)malloc(sizeof(ce_ctx_key));
		nkey->ext=ext;
		nkey->client=client;
		ext->init_context(ce);
		g_hash_table_insert(ce_ctx_map, nkey, ce);
	}	UNLOCK(ce_ctx_map);
	return ce;
}

gboolean free_client_foreach(void *k, void *val, void *data)
{
	ce_ctx_key *key=(ce_ctx_key *)k;
	if (key->client == data)
		return TRUE;
	else
		return FALSE;
}

void extension_free_client(client_ctx *client)
{
	LOCK(ce_ctx_map);
	g_hash_table_foreach_remove(ce_ctx_map, free_client_foreach, client);
	UNLOCK(ce_ctx_map);
}

void extension_buffer_message(client_extension_ctx *client, const mserve_message *msg)
{
	ExtendedMessage em={
		client->extension->ExtensionID,
		msg->type,
		msg->data,
		msg->length
	};
	mserve_message tmp;
	msg_create_ExtendedMessage(&tmp, &em);
	client_ctx_buffer_message(client->client, &tmp);
	mserve_free_message(&tmp);
}
