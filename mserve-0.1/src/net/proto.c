/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <glib.h>

#define _NETWORK_INTERNALS
#include "proto.h"
#include "queue.h"
#include "common.h"
#include "decode/decode.h"
#include "msched.h"
#include "cfg.h"
#include "socketdispatch.h"
#include "ext/extension.h"

void get_all_config_cb(const char *id, const char *value, void *data)
{
	log_printf("client %p: %s=\"%s\"", data, id, value);
	client_ctx *client=(client_ctx *)data;
	mserve_message tmp;
	ConfigValuePair cvp[]={{(char *)id, (char *)value}};
	SetConfig sc={1, cvp};
	msg_create_SetConfig(&tmp, &sc);
	client_ctx_buffer_message(client, &tmp);
}

int proto_make_status_message(int id, mserve_message *msg)
{
	decode_ctx *dec_ctx=get_current_decode_ctx();
	if (!dec_ctx) return 0;
	switch (id)
	{
		case 0:
			msg_create_Status0(msg, &dec_ctx->status0);
			return 1;
		case 1:
			msg->data=strdup(dec_ctx->input.url);
			msg->length=strlen(dec_ctx->input.url)+1;
			msg->type=MT_Status1;
			return 1;
		case 3:
			msg_create_Status3(msg, &dec_ctx->status3);
			return 1;
	}
	return 0;
}

void proto_sched_callback(client_ctx *client, extension_info *ext, int id)
{
	mserve_message msg;
	if (proto_make_status_message(id, &msg))
	{
		client_ctx_buffer_message(client, &msg);
		mserve_free_message(&msg);
	}
}

void proto_send_status_message(client_ctx *client, int status_msg)
{
	proto_sched_callback(client, NULL, status_msg);
}

void proto_message_callback(client_ctx *client, const mserve_message *msg)
{
	uint type=msg->type;
	uint length=msg->length;
	uint i;
	const char *data=(const char *)msg->data;
	decode_ctx *dec_ctx=get_current_decode_ctx();
	mserve_message tmp;
	log_printf("%p: type 0x%04x, length %d", client, type, length);
	switch (type)
	{
		case MT_QueueFile:
		{
			log_printf("QueueFile: \"%s\"", data);
			char *url=(char *)data;
			if (url[msg->length-1])
			{
				url=(char *)alloca(msg->length+1);
				strncpy(url, data, msg->length);
				url[msg->length]=0;
			}
			uint id=queue_push(url);
			QueueNumber qn={id, url};
			msg_create_QueueNumber(&tmp, &qn);
			client_ctx_buffer_message(client, &tmp);
			mserve_free_message(&tmp);
			break;
		}
		case MT_VersionInfo:
		{
			VersionInfo vi;
			UseProtocolVersion upv;

			msg_parse_VersionInfo(&vi, msg);
			upv.ProtocolVersion=min(MSERVE_PROTOCOL_VERSION, vi.ProtocolVersion);
			log_printf("Client version: \"%s\" [0x%x]. Protocol 0x%x", vi.SoftwareName, vi.SoftwareVersion, vi.ProtocolVersion);
			msg_free_VersionInfo(&vi);
			
			log_printf("Using Protocol Version: 0x%x", upv.ProtocolVersion);
			client->protocol_version=upv.ProtocolVersion;
			msg_create_UseProtocolVersion(&tmp, &upv);
			client_ctx_buffer_message(client, &tmp);
			mserve_free_message(&tmp);
			proto_init_status_update(client);
			break;
		}
		case MT_SetStatusUpdates:
		{
			SetStatusUpdates *ssu=(SetStatusUpdates *)data;
			int i;
			
			log_printf("SSU: %08x %d", ssu->StatusUpdateMask, ssu->Interval);
			sched_lock();
			for (i=0;i<NUM_STATUS_TYPES;i++)
			{
				if (ssu->StatusUpdateMask & (1 << i))
				{
					client->statusintervals[i]=ssu->Interval;
					if (ssu->Interval)
						sched_set_unsec(ssu->Interval, client, NULL, i, proto_sched_callback);
					else
						sched_clear_unsec(client, NULL, i);
				}
			}
			sched_unlock();
			break;
		}
		case MT_Pause:
		{
			if (dec_ctx)
			{
				dec_ctx->state=DS_Paused;
				dec_ctx->status0.PlaybackState=PS_Paused;
				decode_ctx_signal_event(dec_ctx);
			}
			break;
		}
		case MT_Resume:
		{
			if (dec_ctx && dec_ctx->state==DS_Paused)
			{
				dec_ctx->state=DS_Running;
				dec_ctx->status0.PlaybackState=PS_Playing;
				decode_ctx_signal_event(dec_ctx);
			}
			else if (!dec_ctx)
			{
				queue_resume();
			}
			break;
		}
		case MT_Stop:
			queue_stop();
			/* Intentional fall-through */
		case MT_Skip:
			if (dec_ctx)
			{
				dec_ctx->state=DS_Complete;
				memset(&dec_ctx->status0, 0, sizeof(typeof(dec_ctx->status0)));
				dec_ctx->status0.PlaybackState=PS_Stopped;
				decode_ctx_signal_event(dec_ctx);
				broadcast_status_msg(0);
			}
			break;
		case MT_Seek:
		{
			if (dec_ctx)
			{
				DecodeCommand cmd;
				Seek seekmsg;
				msg_parse_Seek(&seekmsg, msg);
				dec_ctx->decoder->seek(dec_ctx, seekmsg.PlaybackPosition);
				msg_free_Seek(&seekmsg);
			}
			break;
		}
		case MT_GetAllConfig:
		{
			cfg_iterate_values(&get_all_config_cb, client);
			msg_create_EndOfResponse(&tmp);
			client_ctx_buffer_message(client, &tmp);
			mserve_free_message(&tmp);
			break;
		}
		case MT_GetConfig:
		{
			GetConfig gc;
			msg_parse_GetConfig(&gc, msg);
			for (i=0;gc.NamesCount;i++)
			{
				char *id=gc.Names[i].Name;
				char *val=cfg_get_string_alloc(id);
				ConfigValuePair cvp[]={{id, val}};
				SetConfig sc={1, cvp};
				msg_create_SetConfig(&tmp, &sc);
				client_ctx_buffer_message(client, &tmp);
				mserve_free_message(&tmp);
				free(val);
			}
			msg_free_GetConfig(&gc);
			break;
		}
		case MT_SetConfig:
		{
			SetConfig sc;
			msg_parse_SetConfig(&sc, msg);
			for (i=0;i<sc.ValuesCount;i++)
			{
				cfg_set_string(sc.Values[i].Name, sc.Values[i].Value);
			}
			msg_free_SetConfig(&sc);
			break;
		}
		case MT_RequestStatusMsg:
		{
			RequestStatusMsg rsm;
			msg_parse_RequestStatusMsg(&rsm, msg);
			for (i=0;i<NUM_STATUS_TYPES;i++)
			{
				if (rsm.StatusUpdateMask & (1 << i))
					proto_sched_callback(client, NULL, i);
			}
			msg_free_RequestStatusMsg(&rsm);
			break;
		}
		case MT_GetAllMediaInfo:
		{
			if (dec_ctx)
			{
				attriblist_foreach(&dec_ctx->media_info, proto_media_info_callback, client);
			}
			break;
		}
		case MT_ExtendedMessage:
		{
			ExtendedMessage em;
			msg_parse_ExtendedMessage(&em, msg);
			log_printf("ExtMsg for 0x%04x: type 0x%04x, length %d", em.ExtensionID, em.ExtendedType, em.ExtendedDataLength);
			extension_dispatch_message(client, &em);
			msg_free_ExtendedMessage(&em);
			break;
		}
		case MT_ExtensionInfo:
		{
			ExtensionInfo ei;
			UseExtensions ue;
			msg_parse_ExtensionInfo(&ei, msg);
			ue.ExtensionsCount=ei.ExtensionsCount;
			ue.Extensions=(UseExtensionsElement *)alloca(sizeof(typeof(*ue.Extensions))*ue.ExtensionsCount);
			int n=0;
			for (i=0;i<ei.ExtensionsCount;i++)
			{
				extension_info *ext=get_extension_by_name(ei.Extensions[i].ExtensionName);
				if (ext == NULL)
					ue.ExtensionsCount--;
				else
				{
					ue.Extensions[n].ExtensionID=register_extension_newid(ext);
					memcpy(ue.Extensions[n].ExtensionName, ei.Extensions[i].ExtensionName, 16);
					ue.Extensions[n].ExtensionVersion=min(ei.Extensions[i].ExtensionVersion, ext->ExtensionVersion);
					extension_set_client_extension_version(client, ext, ue.Extensions[n].ExtensionVersion);
					n++;
				}
			}
			msg_create_UseExtensions(&tmp, &ue);
			client_ctx_buffer_message(client, &tmp);
			mserve_free_message(&tmp);
			msg_free_ExtensionInfo(&ei);
			break;
		}
		case MT_Ping:
		{
			tmp.type=MT_Pong;
			tmp.length=0;
			client_ctx_buffer_message(client, &tmp);
			break;
		}
		default:
			log_printf("Unhandled: 0x%04x, length %d on fd %n", type, length, client->socket->fd);
	}
}

void proto_send_VersionInfo(client_ctx *client, const char *SoftwareName, uint32 SoftwareVersion)
{
	VersionInfo vi;
	vi.SoftwareName=(char *)SoftwareName;
	vi.SoftwareVersion=SoftwareVersion;
	vi.ProtocolVersion=MSERVE_PROTOCOL_VERSION;
	mserve_message msg;
	msg_create_VersionInfo(&msg, &vi);
	client_ctx_buffer_message(client, &msg);
}

void broadcast_cb(int fd, socket_ctx *sock, void *data)
{
	if (sock->read == client_ctx_read_cb)
		client_ctx_buffer_message((client_ctx *)sock->userdata, (mserve_message *)data);
}

void broadcast_message(const mserve_message *msg)
{
	socket_ctx_foreach(broadcast_cb, (void *)msg);
}

void broadcast_status_msg(int id)
{
	mserve_message msg;
	if (proto_make_status_message(id, &msg))
		broadcast_message(&msg);
}

void proto_init_status_update(client_ctx *client)
{
	uint i;
	/* Send all status messages contained in the INITIAL_STATUS_MESSAGES mask
	   defined in proto.h */
	for (i=0;i<NUM_STATUS_TYPES;i++)
	{
		if (INITIAL_STATUS_MESSAGES & (1 << i))
		{
			log_printf("sending %d to %p", i, client);
			proto_sched_callback(client, NULL, i);
		}
	}
	proto_message_callback(client, &(mserve_message){MT_GetAllMediaInfo, 0, NULL});
}

void proto_media_info_callback(attriblist *al, void *data, const char *name, const char *value)
{
	MediaInfoPair miv[]={{(char *)name, (char *)value}};
	SetMediaInfo smi={1, miv};
	
	mserve_message msg;
	msg_create_SetMediaInfo(&msg, &smi);
	if (data == NULL)
		broadcast_message(&msg);
	else
		client_ctx_buffer_message((client_ctx *)data, &msg);
	mserve_free_message(&msg);
}
