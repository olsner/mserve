/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _mServe_mi_cache_H
#define _mServe_mi_cache_H

#include "attriblist.h"

struct mediainfo_cache_entry
{
	char *url;
	/**
		The time that was spent extracting the tags last time. Used to calculate
		removal priority.
	*/
	timeval extraction_time;
	timeval last_used;
	attriblist attribs;
};

typedef void (*AsyncGetCallback)(const char *url, attriblist *, void *data);

BEGIN_DECLS

void mi_cache_init();
void mi_cache_enable(int/*bool*/ enable);
attriblist *mi_cache_get(const char *url);
attriblist *mi_cache_async_get(const char *url, AsyncGetCallback cb, void *data);
int mi_cache_write(const char *url, attriblist *);

END_DECLS

#endif
