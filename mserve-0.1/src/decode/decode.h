/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _mServe_codec_H
#define _mServe_codec_H

#include "input/input.h"
#include "out/out_driver.h"
#include <libmserve/attriblist.h>
#include <libmserve/mserve.h>

enum DecodeCommandType
{
	DC_NONE=0,
	DC_SetPlaybackPosition
};

typedef struct _DecodeCommand
{
	enum DecodeCommandType type;
	union {
		uint32 PlaybackPosition;
	} data;
} DecodeCommand;

#define DC_QUEUELEN 10
typedef struct _DecodeCommandQueue
{
		DecodeCommand queue[DC_QUEUELEN];
		/* Read from head, write to tail
		   Both represent an index in the array */
		int head;
		int tail;
		int len;
		pthread_mutex_t mutex;
} DecodeCommandQueue;

enum _DecodeState
{
	DS_Running=1,
	/**
		Output is paused.
	*/
	DS_Paused,
	/**
		All input is processed; but there may be data left in buffer
	*/
	DS_DecodeComplete,
	/**
		All input and output is processed (or playback has been aborted by
		request)
	*/
	DS_Complete
};
typedef enum _DecodeState DecodeState;

enum _DecodeError
{
	DE_None=0,
	DE_Input,
	DE_Decode,
	DE_Output
};
typedef enum _DecodeError DecodeError;

struct _decoder_info
{
	char *name;
	char *description;
	
	/**
		Return true if the specified input context should be handled by this decoder
	*/
	int (*supports)(input_ctx *ctx);
	/**
		Initialize the decoder context for decoding with this decoder. The
		input context and the output driver are fully initialized before calling
		this function.
		
		When input from the input context is done, deinit() will be called
		by the framework
		
		Returns zero on success, non-zero on failure
	*/
	int (*init)(decode_ctx *);
	/**
		Run the stream in input_ctx and send output to the output plugin specified
		by the decoder context.
		
		The decoder has the responsibility to set the format of the output device
		- either in decode_run() or in init()
		
		Return values:
			Zero: all of the stream was successfully decoded
			Non-zero: decoding was aborted by an error
	*/
	int (*decode_run)(decode_ctx *);
	/**
		Deinitialize the decoder context - the memory allocation of the context
		pointer is handled by the caller
	*/
	void (*deinit)(decode_ctx *);
	/**
		Instruct the decoder to seek to the specified location (milliseconds)
		
		This function should not block, since it may be called from the network
		thread. A non-error return indicates that the decoder thread will seek
		to the new position "soon".
		
		Return values:
			Zero: No error
			Non-zero: For some reason, the command has not been executed
	*/
	int (*seek)(decode_ctx *, uint pos_millis);
	/**
		Inspect the input_ctx and extract all tags possible
		
		Returns:
			The number of tags (attriblist pairs) retrieved, or -1 on error
	*/
	int (*extract)(input_ctx *, attriblist *);
};
#define MAKE_DECODER_INFO(name, desc, prefix) \
	{name, desc, prefix##_supports, prefix##_init, prefix##_decode_run, \
	prefix##_deinit, prefix##_seek, prefix##_extract}

struct _decode_ctx
{
	input_ctx input;
	/**
		Internal decoder data.
	*/
	void *internal;
	decoder_info *decoder;
	out_driver_info *output;
	attriblist media_info;
	Status0 status0;
	Status3 status3;
	DecodeState state;
	DecodeError error;

/****PRIVATE****/
	
	pthread_mutex_t statusUpdateCondMutex;
	pthread_cond_t statusUpdateCond;
};

typedef decoder_info *(decoder_get_info_func)();

BEGIN_DECLS

extern decoder_info *get_decoder_info();

/**
	The input context should be initialized before calling this function
	The input_ctx pointer may point to the member in the decode_ctx pointer,
	if it doesn't, the input_ctx contents is copied into the decode ctx struct.
	
	Returns the return value of the decoders init function - zero means success
	and non-zero means error
*/
extern int decode_ctx_init(decode_ctx *, input_ctx *, decoder_info *, out_driver_info *);
extern void decode_ctx_destroy(decode_ctx *);
extern decoder_info *get_decoder(input_ctx *);
/**
	These are used for synchronization between the decode, output and control
	threads. wait_event blocks until another thread calls signal_event for the
	decode context. All threads suspended by wait_event will be awoken by one
	call to 
*/
extern void decode_ctx_signal_event(decode_ctx *);
extern void decode_ctx_wait_event(decode_ctx *);

/**
	Initialize the command queue. This function must be called before using the
	command queue with any of the other cmd_queue_ functions.
*/
extern void cmd_queue_init(DecodeCommandQueue *);
/**
	Push the command onto the command queue (the command is copied). If there is
	no available space in the command queue, the function returns -1, otherwise
	(success) it returns 0
*/
extern int cmd_queue_push(DecodeCommandQueue *, const DecodeCommand *cmd);
/**
	Pop a command from the command queue. Returns 0 if a command was
	successfully popped, -1 if the queue is empty.
*/
extern int cmd_queue_pop(DecodeCommandQueue *, DecodeCommand *cmd);

/**
	Basically cmd_queue_{pop,push} but these two also wakes up all threads
	suspended by decode_ctx_wait_event
*/
/*extern int decode_ctx_push_command(decode_ctx *, const DecodeCommand *cmd);
  extern int decode_ctx_pop_command(decode_ctx *, DecodeCommand *cmd);
*/

END_DECLS

#endif
