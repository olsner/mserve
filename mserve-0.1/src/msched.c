/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

/**
	Why this file needs C++:
		STL vector
			sched_buffer (all sched_items, inline): GArray
			schedule (pointer, heapified): GPtrArray
		STL {make,pop,push}_heap - rel. easy to reimplement
			We really only need push_heap and pop_heap
	
	Algorithm:
		One vector contains all sched_items (sched_buffer)
		One vector(heap) contains pointers to scheduled sched_items, sorted
		by scheduled time
		
		When sched_items are added/removed, the schedule is rebuilt
		When a scheduled item is executed, the schedule is updated (pop_heap
		and push_heap)
		
		The scheduler loop inspects the heap and waits for a queue change or
		the scheduled time to arrive.
	
	Caveats:
		When the buf_mutex is unlocked, all pointers in schedule must point
		to valid sched_items. The problem is that each time an item is added
		(and removed), all pointers are potentially invalidated. This means that
		the *_nosignal functions are all bogus (unless the buf_mutex can be held
		over several *_nosignal invokations)
			The _nosignal functions are replaced with _unsec functions, that do
			not perform any locking or signaling. The buf_mutex needs to be
			locked with sched_lock() and sched_unlock()
*/

#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <pthread.h>
#include <errno.h>
#include <stdio.h>

#include "common.h"
#include "msched.h"

/**
	Makes a timespec of a timeval
*/
#define TIMECONV(_ts, _tv) do { (_ts)->tv_sec=(_tv)->tv_sec; (_ts)->tv_nsec=(_tv)->tv_usec*1000; } while (0)

typedef struct _sched_item sched_item;

struct _sched_item 
{
	int sched_index;
	struct timeval scheduled_time;
	struct timeval last;
	uint interval;
	client_ctx *client;
	extension_info *extension;
	int id;
	SchedCallback callback;
	
/*	bool operator > (const sched_item &b) const
	{
		const timeval *t1=&scheduled_time,*t2=&b.scheduled_time;
		return (t1->tv_sec > t2->tv_sec)
			|| (t1->tv_sec == t2->tv_sec && t1->tv_usec > t2->tv_usec);
	}
*/
};

int sched_item_cmp(const sched_item *a, const sched_item *b)
{
	const struct timeval *t1=&a->scheduled_time,*t2=&b->scheduled_time;
	return timercmp(t1, t2, >);
}

//vector <sched_item> sched_buffer;
//vector<sched_item*> schedule;

GArray *sched_buffer;
/**
	Removes the index and updates the schedule.
*/
void sched_buffer_remove_index(uint i);

sched_item *next=NULL;
void schedule_heapify();
void schedule_push(sched_item *);
void schedule_pop();

/*GPtrArray *schedule;
uint sched_size=0; // The size of the heap contained in the GPtrArray
void schedule_set_size(uint sz);
void schedule_set(uint index, sched_item *);
void schedule_heapify();
void schedule_push(sched_item *);*/
/**
	Remove the reference to <item>
*/
/*void schedule_remove_ptr(sched_item *);
void schedule_pop();
sched_item *schedule_front();*/

pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t buf_mutex=PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond=PTHREAD_COND_INITIALIZER;

void sched_buffer_remove_index(uint i)
{
	/*sched_item *it=&g_array_index(sched_buffer, sched_item, i);*/
	g_array_remove_index_fast(sched_buffer, i);
}

sched_item *schedule_front()
{
	return (sched_item *)next/*schedule->pdata[0]*/;
}

/*void schedule_set_size(uint sz)
{
	if (sz > schedule->len)
		g_ptr_array_set_size(schedule, sz);
	sched_size=sz;
}

void schedule_set(uint i, sched_item *it)
{
	g_ptr_array_index(schedule, i)=it;
}

void siftdown(uint i)
{
	int large=i;
	do
	{
		i=large;
		int left=2*i+1;
		int right=left+1;

		if (left < sched_size)
		{
			large=sched_item_cmp(schedule->pdata[left], schedule->pdata[i])?i:left;

			if (right < sched_size)
				large=sched_item_cmp(schedule->pdata[right], schedule->pdata[large])?large:right;
		}

		if (large != i)
		{
			gpointer tmp=schedule->pdata[large];
			schedule->pdata[large] = schedule->pdata[i];
			schedule->pdata[i] = tmp;
		}
	}
	while (large != i);
}

void schedule_heapify()
{
	uint n=(sched_size/2)+1;
	while (n--)
	{
		siftdown(n);
	}
}*/

void schedule_heapify()
{
	uint i;
	sched_item *max=NULL;
	for (i=0;i<sched_buffer->len;i++)
	{
		sched_item *it=&g_array_index(sched_buffer, sched_item, i);
		if (!max || sched_item_cmp(max, it))
			max=it;
	}
	next=max;
}

void schedule_push(sched_item *it)
{
	/*g_ptr_array_add(schedule, schedule->pdata[0]);
	g_ptr_array_index(schedule, 0)=it;
	siftdown(0);
	sched_size++;*/
	/*if (next && sched_item_cmp(next, it))
		next=it;
	if (!next)*/
		schedule_heapify();
}

void schedule_pop()
{
	/*sched_size--;
	g_ptr_array_index(schedule, 0)=g_ptr_array_index(schedule, sched_size);
	siftdown(0);*/
	next=NULL;
}

/*void schedule_remove_ptr(sched_item *)
{
	
}*/

void reschedule()
{
	/*schedule_set_size(sched_buffer->len);
	uint i=0;
	for (;i<sched_buffer->len;i++)
	{
		schedule_set(i, &g_array_index(sched_buffer, sched_item, i));
	}*/
	schedule_heapify();
}

/*void sched_signal_reschedule()
{
	pthread_mutex_lock(&mutex);
	pthread_cond_broadcast(&cond);
	pthread_mutex_unlock(&mutex);
}*/

void sched_lock()
{
	pthread_mutex_lock(&buf_mutex);
}

void sched_unlock()
{
	reschedule();
	pthread_mutex_unlock(&buf_mutex);
	pthread_mutex_lock(&mutex);
	pthread_cond_broadcast(&cond);
	pthread_mutex_unlock(&mutex);
}

void sched_set_unsec(uint interval, client_ctx *client, extension_info *extension, int id, SchedCallback cb)
{
	struct timeval now;
	struct timeval onow;
	gettimeofday(&now, NULL);
	TIMESET(onow, now);
	now.tv_sec += interval / 1000;
	now.tv_usec+=(interval%1000)*1000;
	while (now.tv_usec > 1000000)
	{
		now.tv_sec++;
		now.tv_usec -= 1000000;
	}
	sched_item *item=NULL;
	if (sched_buffer->len)
	{
		uint i=0;
		while (i < sched_buffer->len)
		{
			item=&g_array_index(sched_buffer, sched_item, i);
			if (item->client==client && item->extension==extension && item->id==id)
				break;
			item=NULL;
			++i;
		}
	}
	if (!item)
	{
		g_array_set_size(sched_buffer, sched_buffer->len+1);
		item=&g_array_index(sched_buffer, sched_item, sched_buffer->len-1);
	}
	TIMESET(item->scheduled_time, now);
	TIMESET(item->last, onow);
	item->interval=interval;
	item->client=client;
	item->extension=extension;
	item->id=id;
	item->callback=cb;
	item->sched_index=-1;
	
	log_printf("client %p: id %d interval %u (cb %p)", client, id, interval, cb);
}

void sched_set(uint interval, client_ctx *client, extension_info *extension, int id, SchedCallback cb)
{
	sched_lock();
	sched_set_unsec(interval, client, extension, id, cb);
	sched_unlock();
}

void sched_clear_unsec(client_ctx *client, extension_info *extension, int id)
{
	uint i=0;
	while (i < sched_buffer->len)
	{
		sched_item *it=&g_array_index(sched_buffer, sched_item, i);
		if (it->client == client && it->extension==extension && it->id == id)
		{
			sched_buffer_remove_index(i);
			break;
		}
		++i;
	}
}

void sched_clear(client_ctx *client, extension_info *extension, int id)
{
	sched_lock();
	sched_clear_unsec(client, extension, id);
	sched_unlock();
}

void sched_clear_all_unsec(client_ctx *client)
{
	uint i=0;
	while (i < sched_buffer->len)
	{
		sched_item *it=&g_array_index(sched_buffer, sched_item, i);
		if (it->client == client)
		{
			sched_buffer_remove_index(i);
		}
		else
			++i;
	}
}

void sched_clear_all(client_ctx *client)
{
	log_printf("client %p", client);
	sched_lock();
	sched_clear_all_unsec(client);
	sched_unlock();
}

void dump_schedule()
{
	printf("schedule:\n");
	int i;
	/*for (i=0;i<3 && i<sched_size;i++)*/
	{
		sched_item *item=schedule_front()/*schedule->pdata[i]*/;
		uint64 t=((uint64)item->scheduled_time.tv_sec)*1000000+item->scheduled_time.tv_usec;
		printf("\t%p:%d:(cb %p):\t%d\t%Ld\n", item->client, item->id, item->callback, item->interval, t);
	}
}

void sched_init()
{
	sched_buffer=g_array_new(FALSE, FALSE, sizeof(sched_item));
	/*schedule=g_ptr_array_new();*/
}

void sched_run()
{
	pthread_mutex_lock(&buf_mutex);
	reschedule();
	pthread_mutex_unlock(&buf_mutex);
	
	while (1)
	{
		if (!next)
		{
			pthread_mutex_lock(&mutex);
			pthread_cond_wait(&cond, &mutex);
			pthread_mutex_unlock(&mutex);
			continue;
		}
		
		struct timespec ts;
		pthread_mutex_lock(&buf_mutex);
		TIMECONV(&ts, &schedule_front()->scheduled_time);
		pthread_mutex_unlock(&buf_mutex);
		pthread_mutex_lock(&mutex);
		int res=pthread_cond_timedwait(&cond, &mutex, &ts);
		pthread_mutex_unlock(&mutex);
		
		/** The Condition was signaled, the schedule has been changed, re-loop */
		if (res == 0)
		{
			/*reschedule();*/
			continue;
		}
		if (res == EINTR)
			continue;
		
		//dump_schedule();
		
		struct timeval tv;
		struct timeval now;
		gettimeofday(&now, NULL);
		
		pthread_mutex_lock(&buf_mutex);
		while (timercmp(&schedule_front()->scheduled_time, &now, <))
		{
			sched_item *item=schedule_front();
			TIMESET(tv, now);
			tv.tv_sec += (item->interval / 1000);
			tv.tv_usec += (item->interval%1000)*1000;
			while (tv.tv_usec >= 1000000)
			{
				tv.tv_sec++;
				tv.tv_usec-=1000000;
			}
			TIMESET(item->scheduled_time, tv);
			
			SchedCallback cb=item->callback;
			client_ctx *client=item->client;
			extension_info *ext=item->extension;
			int id=item->id;
			pthread_mutex_unlock(&buf_mutex);
			//printf("%p:%d:%d:(cb %p)\n", client, item->interval, id, cb);
			cb(client, ext, id);
			pthread_mutex_lock(&buf_mutex);
			
			schedule_pop();
			schedule_push(item);
		}
		pthread_mutex_unlock(&buf_mutex);
	}
}
