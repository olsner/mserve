/*
 * This file is part of mdfgen.
 * 
 * mdfgen is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mdfgen is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mdfgen; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Copyright (C) Simon Brenner 2003
 */

#ifndef istruct_hpp
#define istruct_hpp

struct Field;

struct IStruct
{
	vector <Field *> fields;
	
	virtual string getStructDef()=0;
	virtual string getName()=0;
};

#endif
