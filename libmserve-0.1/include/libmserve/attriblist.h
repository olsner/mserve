/*
 * This file is part of libmserve.
 * 
 * libmserve is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * libmserve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with libmserve; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _mServe_attriblist_H
#define _mServe_attriblist_H

#include <glib.h>
#include <pthread.h>
#include <libmserve/common.h>

BEGIN_DECLS

struct attriblist;
typedef struct attriblist attriblist;

extern attriblist *attriblist_init(attriblist *);
extern void attriblist_destroy(attriblist *);
/**
	Retrieve the value of <name>, and return a duplicate. The recipient is
	responsible for deallocating the returned string.
	If the name does not have a value, NULL is returned
*/
extern char *attriblist_get_dup(attriblist *, const char *name);
extern void attriblist_put(attriblist *, const char *name, const char *value);
extern int attriblist_remove(attriblist *, const char *name);

typedef void (*attriblist_callback)(attriblist *, void *data, const char *name, const char *value);
extern void attriblist_set_callback(attriblist *, attriblist_callback, void *);

extern void attriblist_foreach(attriblist *, attriblist_callback, void *);

struct attriblist
{
#ifdef __cplusplus
	inline char *get_dup(const char *name)
	{	return attriblist_get_dup(this, name); }
	inline void put(const char *name, const char *value)
	{	attriblist_put(this, name, value); }
	inline int remove(const char *name)
	{	return attriblist_remove(this, name); }
#endif
/****PRIVATE****/
	GHashTable *table;
	pthread_mutex_t mutex;
	attriblist_callback set_callback;
	void *set_callback_data;
};

END_DECLS

#endif
