/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>

#include <glib.h>

#define _NETWORK_INTERNALS

#include "common.h"
#include "socketdispatch.h"
#include "client.h"
#include "proto.h"
#include "net.h"

socket_ctx *main_socket;
pthread_t net_thread;

void *net_thread_main(void *arg)
{
	regsympid("NET");
	return (void *)net_main();
}

void net_thread_start()
{
	pthread_create(&net_thread, NULL, net_thread_main, NULL);
}

int net_thread_join()
{
	int ret;
	pthread_join(net_thread, (void **)&ret);
	return ret;
}

void net_accept_client(int fd)
{
	log_printf("We have children! (fd %d)", fd);
	socket_ctx *socket=socket_ctx_from_fd(fd);
	client_ctx *client=client_ctx_create(socket);
	client->message_cb=proto_message_callback;
	proto_send_VersionInfo(client, "mServe", MSERVE_SOFTWARE_VERSION);
}

void main_socket_read_cb(socket_ctx *ctx, SocketNotifyEvent event)
{
	int fd=accept(ctx->fd, NULL, NULL);
	if (fd == -1)
	{
		log_printf("Accepting client connection: %s", strerror(errno));
		return;
	}
	net_accept_client(fd);
}

int net_main()
{
	int fd=socket(PF_INET, SOCK_STREAM, 0);
	int res;
	struct sockaddr_in addr;
	
	if (fd == -1)
	{
		perror("Opening listening socket");
		return 0;
	}
	
	int data=-1;
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &data, sizeof(data));
	
	addr.sin_family=AF_INET;
	addr.sin_port=htons(4097);
	addr.sin_addr.s_addr=INADDR_ANY;
	res=bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	if (res == -1)
	{
		perror("Binding listening socket");
		return 0;
	}
	
	// We now have a created and bound server socket
	listen(fd, 1);
	log_printf("Server fd is %d", fd);
	
	socket_ctx *mainctx=socket_ctx_from_fd(fd);
	mainctx->read=main_socket_read_cb;
	main_socket=mainctx;
	
	while (socket_dispatch(NULL)==0);
	log_print(strerror(errno));
	return 1;
}
