/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include "decode/decode.h"
#include "out/buffer.h"
#include "net/proto.h"

#include <mad.h>
#include <id3tag.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#define PULL_INTERNAL() struct libmpg_internal *internal=(struct libmpg_internal *)ctx->internal
#define INBUFFER_SIZE 49152

extern int libmpg_supports(input_ctx *ctx);
extern int libmpg_init(decode_ctx *ctx);
extern int libmpg_decode_run(decode_ctx *ctx);
extern void libmpg_deinit(decode_ctx *ctx);
extern int libmpg_seek(decode_ctx *ctx, uint millipos);
extern int libmpg_extract(input_ctx *, attriblist *);
decoder_info libmpg_decoder_info=MAKE_DECODER_INFO("libmpg", "libMAD decoder", libmpg);
decoder_info *get_decoder_info()
{
	return &libmpg_decoder_info;
}

struct libmpg_internal
{
	int formatisset;
	uint lastbitrate;
	int bpf; /* Bytes per frame */
	int millispf; /* Milliseconds per frame */
	uint8 inbuffer[INBUFFER_SIZE+MAD_BUFFER_GUARD];
	uint8 *endptr; /* The current pointer to the end of buffered data */
	struct mad_stream stream;
	struct mad_frame frame;
	struct mad_synth synth;
	
	pthread_mutex_t mutex;
	int have_seek;
	uint seekpos;
};

enum mad_flow input_cb(void *data, struct mad_stream *stream)
{
	decode_ctx *ctx=(decode_ctx *)data;
	PULL_INTERNAL();
	
	uint8 *buf=internal->inbuffer;
	uint8 *bufend=internal->inbuffer+INBUFFER_SIZE;
	uint left=bufend-internal->endptr;
	int cnt;
	if (stream->next_frame)
	{
		/* tflen is the length of the last/this frame in the buffer */
		int tflen=stream->next_frame-internal->inbuffer;
		memmove(buf, stream->next_frame, (internal->endptr-internal->inbuffer)-tflen);
		left+=tflen;
		internal->endptr-=tflen;
	}
	cnt=ctx->input.driver->read(&ctx->input, internal->endptr, left);
	internal->endptr+=cnt;
	if (cnt == 0)
		return MAD_FLOW_STOP;
	mad_stream_buffer(stream, buf, internal->endptr-internal->inbuffer);
	return MAD_FLOW_CONTINUE;
}

enum mad_flow reset_stream(decode_ctx *ctx)
{
	enum mad_flow ret;
	PULL_INTERNAL();
	internal->endptr=internal->inbuffer;
	mad_stream_buffer(&internal->stream, internal->inbuffer, 0);
	ret=input_cb(ctx, &internal->stream);
	if (ret == MAD_FLOW_CONTINUE)
	{
		mad_stream_sync(&internal->stream);
		if (internal->formatisset)
		{
			char buf[144];
			memset(buf, 0, sizeof(buf));
			out_buffer_push(buf, 144);
		}
	}
	return ret;
}

/* scale() IS BORROWED FROM MAD */
static inline
int scale(mad_fixed_t sample)
{
  /* round */
  sample += (1L << (MAD_F_FRACBITS - 16));

  /* clip */
  if (sample >= MAD_F_ONE)
    sample = MAD_F_ONE - 1;
  else if (sample < -MAD_F_ONE)
    sample = -MAD_F_ONE;

  /* quantize */
  return sample >> (MAD_F_FRACBITS + 1 - 16);
}

enum mad_flow output_cb(void *data, const struct mad_header *header, struct mad_pcm *pcm)
{
	decode_ctx *ctx=(decode_ctx *)data;
	PULL_INTERNAL();
	
	int nchannels=pcm->channels;
	int nsamples=pcm->length;
	int pitch=2*nchannels;
	int length=pcm->length*pitch;
	uint8 *buffer=(uint8 *)alloca(length);
	int i=-1;
	int res;
	
	if (!internal->formatisset)
	{
		ctx->output->set_format(pcm->samplerate, SFMT_16LE_S, nchannels);
		internal->formatisset++;
	}
	
	ctx->status0.InputMillis+=(nsamples*1000)/44100;
	ctx->status0.PlaybackPosition+=(nsamples*1000)/44100;
	
	while (i++, nsamples--)
	{
		int s;
		mad_fixed_t sample;
		
		sample=pcm->samples[0][i];
		s=scale(sample);
		buffer[i*pitch]=s&0xff;
		buffer[i*pitch+1]=(s>>8)&0xff;
		if (nchannels == 2)
		{
			sample=pcm->samples[1][i];
			s=scale(sample);
			buffer[i*pitch+2]=s&0xff;
			buffer[i*pitch+3]=(s>>8)&0xff;
		}
	}
	
	while (1)
	{
		DecodeState oldstate=ctx->state;
		int had_seek=internal->have_seek;
		out_buffer_wait_for_push(ctx, length);
		if (ctx->state==DS_Complete)
		{
			internal->stream.error=MAD_ERROR_NONE;
			return MAD_FLOW_BREAK;
		}
		if (oldstate==ctx->state && internal->have_seek == had_seek)
			break;
	}
	res=out_buffer_push((char *)buffer, length);
	decode_ctx_signal_event(ctx);
	if (res < length)
	{
		log_printf("res %d, wanted %d", res, length);
	}
	return res<length?MAD_FLOW_BREAK:MAD_FLOW_CONTINUE;
}

void process_id3_tag(attriblist *atts, struct id3_tag *tag)
{
	char *tags[][2] = {
		{ID3_FRAME_TITLE, "title"},
		{ID3_FRAME_ARTIST, "artist"},
		{ID3_FRAME_ALBUM, "album"},
		{ID3_FRAME_YEAR, "year"},
		{ID3_FRAME_TRACK, "track"},
		{ID3_FRAME_GENRE, "genre"},
	};
	uint i;
	
	for (i=0;i<sizeof(tags)/sizeof(tags[0]);i++)
	{
		struct id3_frame *frame;
		union id3_field *field;
		const id3_ucs4_t *ucs4;
		id3_latin1_t *latin1;
		
		frame=id3_tag_findframe(tag, tags[i][0], 0);
		if (!frame) continue;
		
		field=&frame->fields[1];
		if (!id3_field_getnstrings(field)) continue;
		ucs4=id3_field_getstrings(field, 0);
		if (strcmp(tags[i][0], ID3_FRAME_GENRE)==0)
			ucs4=id3_genre_name(ucs4);
		latin1=id3_ucs4_latin1duplicate(ucs4);
		
		log_printf("\"%s\" = \"%s\"", tags[i][1], latin1);
		attriblist_put(atts, tags[i][1], (char *)latin1);
		
		free(latin1);
	}
}

enum mad_flow error_cb(decode_ctx *ctx, char *where, struct mad_stream *stream, struct mad_frame *frame)
{
	/*PULL_INTERNAL();*/
	
	if (stream->error == MAD_ERROR_LOSTSYNC)
	{
		id3_length_t bufsize=stream->bufend-stream->this_frame;
		id3_length_t tagsize=id3_tag_query(stream->this_frame, bufsize);
		id3_byte_t *data=NULL;
		id3_byte_t *allocated=NULL;
		if (tagsize > bufsize)
		{
			allocated=(id3_byte_t *)malloc(tagsize);
			memcpy(allocated, stream->this_frame, bufsize);
			mad_stream_skip(stream, bufsize);
			while (bufsize < tagsize)
			{
				int res=ctx->input.driver->read(&ctx->input, allocated+bufsize, tagsize-bufsize);
				if (res == -1)
				{ free(allocated); return MAD_FLOW_BREAK; }
				if (res == 0)
				{ log_print("EOF in tag"); free(allocated); return MAD_FLOW_STOP; }
				bufsize += res;
			}
			data=allocated;
		}
		else if (tagsize > 0)
		{
			data=(id3_byte_t *)stream->this_frame;
			mad_stream_skip(stream, tagsize);
		}
		if (tagsize > 0)
		{
			struct id3_tag *tag=id3_tag_parse(data, tagsize);
			if (tag)
			{
				process_id3_tag(&ctx->media_info, tag);
				id3_tag_delete(tag);
			}
			if (allocated)
				free(allocated);
		}
		return MAD_FLOW_CONTINUE;
	}
	else if (stream->error >= MAD_ERROR_LOSTSYNC)
	{
		return MAD_FLOW_IGNORE;
	}
	
	if (stream->error != MAD_ERROR_BUFLEN)
		log_printf("%s: decoding error 0x%04x (%s)", where, stream->error, mad_stream_errorstr(stream));
	return MAD_RECOVERABLE(stream->error)?MAD_FLOW_CONTINUE:MAD_FLOW_BREAK;
}

void header_cb(decode_ctx *ctx, struct mad_stream *stream, struct mad_header *header)
{
	PULL_INTERNAL();
	int64 media_bytes;
	int changed;
	
	if (!internal->lastbitrate || internal->lastbitrate == header->bitrate)
	{
		internal->bpf=stream->next_frame-stream->this_frame;
		internal->millispf=header->duration.seconds*1000+
			(header->duration.fraction)/(MAD_TIMER_RESOLUTION/1000);
		/*printf("Bytes per frame: %u, Milliseconds per frame: %u\n", internal->bpf, internal->millispf);*/
	}
	if (internal->lastbitrate!=header->bitrate)
	{
		/*printf("Bitrate: %ld\n", header->bitrate);*/
	}
	internal->lastbitrate=header->bitrate;
	
	media_bytes=ctx->input.driver->get_media_length(&ctx->input);
	changed=(media_bytes != ctx->status3.MediaBytes);
	if (media_bytes != -1)
		ctx->status3.MediaBytes=media_bytes;
	ctx->status3.MediaMillis=(media_bytes*internal->millispf)/internal->bpf;
	if (changed)
	{
		log_printf("Media length: %llu bytes (%u milliseconds)", ctx->status3.MediaBytes, ctx->status3.MediaMillis);
		broadcast_status_msg(3);
	}
}

int libmpg_supports(input_ctx *ctx)
{
	if (strcasecmp(strchr(ctx->url, 0)-4, ".mp3")==0)
		return TRUE;
	return FALSE;
}

int libmpg_init(decode_ctx *ctx)
{
	struct libmpg_internal *internal;
	ctx->internal=malloc(sizeof(struct libmpg_internal));
	internal=(struct libmpg_internal *)ctx->internal;
	
	memset(internal, 0, sizeof(*internal));
	internal->endptr=internal->inbuffer;
	internal->lastbitrate=0;
	internal->bpf=0;
	internal->millispf=0;
	
	pthread_mutex_init(&internal->mutex, NULL);
	
	return 0;
}

enum mad_flow check_seek(decode_ctx *ctx)
{
	PULL_INTERNAL();
	
	pthread_mutex_lock(&internal->mutex);
	if (internal->have_seek)
	{
		int cbr=internal->bpf?1:0;
		uint bytepos;
		
		internal->have_seek=0;
		if (cbr)
		{
			bytepos=(internal->bpf*internal->seekpos)/internal->millispf;
			bytepos+=internal->bpf-(bytepos%internal->bpf);
			log_printf("bytepos%%internal->Bpf == %d", bytepos%internal->bpf);
		}
		else
		{
			uint bytesproc=ctx->status0.InputBytes;
			uint millisproc=ctx->status0.InputMillis;
			float bpmilli=((float)bytesproc)/((float)millisproc);
			bytepos=(uint)(bpmilli*internal->seekpos);
		}

		ctx->status0.PlaybackPosition=internal->seekpos;
		pthread_mutex_unlock(&internal->mutex);
		ctx->input.driver->seek(&ctx->input, bytepos, SEEK_SET);
		return reset_stream(ctx);
	}
	pthread_mutex_unlock(&internal->mutex);
	
	if (ctx->state == DS_Complete)
	{
		internal->stream.error=MAD_ERROR_NONE;
		return MAD_FLOW_BREAK;
	}
	
	return MAD_FLOW_IGNORE;
}

#define GROK_RES switch (res) { \
			case MAD_FLOW_BREAK: \
				cur_err=stream->error; \
			case MAD_FLOW_STOP: \
				if (cur_err != MAD_ERROR_BUFLEN) \
					running=0; \
			} if (cur_err || !running) continue;

int libmpg_decode_run(decode_ctx *ctx)
{
	PULL_INTERNAL();
	struct mad_stream *stream=&internal->stream;
	struct mad_frame *frame=&internal->frame;
	struct mad_synth *synth=&internal->synth;
	int cur_err=0;
	int running=1;
	int res;
	
	mad_stream_init(stream);
	mad_frame_init(frame);
	mad_synth_init(synth);
	
	do
	{
		if (running) cur_err=0;
		res=check_seek(ctx);
		GROK_RES
		res=input_cb(ctx, stream);
		GROK_RES
		
		while (!cur_err && running)
		{
			res=check_seek(ctx);
			GROK_RES
			
			res=mad_header_decode(&frame->header, stream);
			if (res == -1)
			{
				res=error_cb(ctx, "mad_header_decode", stream, frame);
				GROK_RES
				if (res == MAD_FLOW_IGNORE)
				{
					mad_stream_skip(stream, stream->next_frame-stream->this_frame);
					continue;
				}
			}
			header_cb(ctx, stream, &frame->header);
			
			ctx->status0.InputBytes += stream->next_frame-stream->this_frame;
			
			res=mad_frame_decode(frame, stream);
			if (res == -1)
			{
				res=error_cb(ctx, "mad_frame_decode", stream, frame);
				GROK_RES
			}
			
			mad_synth_frame(synth, frame);
			
			res=output_cb(ctx, &frame->header, &synth->pcm);
			GROK_RES
		}
	}
	while ((running && !cur_err) || (running && cur_err==MAD_ERROR_BUFLEN));
	
	mad_synth_finish(synth);
	mad_frame_finish(frame);
	mad_stream_finish(stream);
	if (!cur_err)
		return 0;
	else
		return -1;
}

void libmpg_deinit(decode_ctx *ctx)
{
	free(ctx->internal);
}

int libmpg_seek(decode_ctx *ctx, uint millipos)
{
	PULL_INTERNAL();

	pthread_mutex_lock(&internal->mutex);
	internal->have_seek=1;
	internal->seekpos=millipos;
	pthread_mutex_unlock(&internal->mutex);
	
	decode_ctx_signal_event(ctx);
	
	return 0;
}

int libmpg_extract(input_ctx *in, attriblist *atts)
{
	if (strncmp(in->url, "http://", 7))
	{
		struct id3_file *file;
		struct id3_tag *tag;
		
		file=id3_file_open(in->url, ID3_FILE_MODE_READONLY);
		if (file)
		{
			tag=id3_file_tag(file);
			if (tag)
			{
				process_id3_tag(atts, tag);
			}
			id3_file_close(file);
			return 0;
		}
	}
	return -1;
}
