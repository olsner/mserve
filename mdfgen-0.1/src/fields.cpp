/*
 * This file is part of mdfgen.
 * 
 * mdfgen is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mdfgen is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mdfgen; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Copyright (C) Simon Brenner 2003
 */

#include <libxml/tree.h>

#include <vector>
#include <string>
#include <iostream>
using namespace std;

#include "fields.hpp"
#include "protocol.hpp"

Field::Field(xmlNodePtr nod)
{
	xmlChar *name=xmlGetProp(nod, (const xmlChar *)"name");
	this->name=string((char *)name);
	xmlFree(name);
}

string SimpleField::getFieldDef()
{
	return type+" "+name+";";
}

string SimpleField::getCreateStmts()
{
	return "create_"+type+"(out, msg->"+name+", &pos);";
}

string SimpleField::getDryCreateStmts()
{
	return "pos+=sizeof("+type+");";
}

string SimpleField::getParseStmts()
{
	return "parse_"+type+"(in, &out->"+name+", &pos);";
}

string SimpleField::getDryParseStmts()
{
	return "req_bytes(in, &pos, sizeof("+type+")); pos+=sizeof("+type+");";
}

ArrayField::ArrayField(xmlNodePtr nod): Field(nod)
{
	xmlChar *type=xmlGetProp(nod, (const xmlChar *)"typename");
	this->type=string((char *)type);
	xmlFree(type);

	nod=nod->xmlChildrenNode;
	while (nod)
	{
		Field::parseFieldNode(nod, this, NULL);
		nod=nod->next;
	}
}

string ArrayField::getStructDef()
{
	string ret="struct "+type+" {";
	foreach (i, fields)
		ret+="\n\t"+(*i)->getFieldDef();
	ret+="\n};";
	return ret;
}

string ArrayField::getName()
{
	return type;
}

string ArrayField::getFieldDef()
{
	return "uint "+name+"Count; "+type+" *"+name+";";
}

string ArrayField::getParseStmts()
{
	string ret;
	ret+="\n\tout->"+name+"=("+type+"*)alloc((out->"+name+"Count)*sizeof("+type+"));";
	ret+="\n\t{uint i; for (i=0;i<out->"+name+"Count;i++) {";
	ret+="\n\t\t"+type+" *o_out=out->"+name+";";
	ret+="\n\t\t"+type+" *out=&o_out[i];";
	foreach (i, fields)
		ret+="\n\t\t"+(*i)->getParseStmts();
	ret+="\n\t}}";
	return ret;	
}

string ArrayField::getDryParseStmts()
{
	string ret="";
	ret+="\n\t"+type+" tmp_"+name+";";
	ret+="\n\tout->"+name+"Count=0;";
	ret+="\n\twhile (pos < in->length) {";
	ret+="\n\t\tout->"+name+"Count++;";
	ret+="\n\t\t"+type+" *out;out=&tmp_"+name+";";
	ret+="\n\t\tdryAlloc(sizeof(tmp_"+name+"));";
	foreach (i, fields)
		ret+="\n\t\t"+(*i)->getDryParseStmts();
	ret+="\n\t}";
	ret+="\n\tpos=in->length;";
	return ret;	
}

string ArrayField::getDryCreateStmts()
{
	string ret="";
	ret+="{uint i;";
	ret+="\n\tfor (i=0;i<msg->"+name+"Count;i++) {";
	// The reason for doing the assignment in two steps is to remove any
	// "unused variable" warnings
	ret+="\n\t\ttypeof(msg) o_msg=msg;";
	ret+="\n\t\t"+type+" *msg; msg=&o_msg->"+name+"[i];";
	foreach (i, fields)
		ret+="\n\t\t"+(*i)->getDryCreateStmts();
	ret+="\n\t}}";
	return ret;
}

string ArrayField::getCreateStmts()
{
	string ret="";
	ret+="{uint i;";
	ret+="\n\tfor (i=0;i<msg->"+name+"Count;i++) {";
	// The reason for doing the assignment in two steps is to remove any
	// "unused variable" warnings - not required for functionality, but it does make
	// the compilation of generated code cleaner.
	ret+="\n\t\ttypeof(msg) o_msg=msg;";
	ret+="\n\t\t"+type+" *msg; msg=&o_msg->"+name+"[i];";
	foreach (i, fields)
		ret+="\n\t\t"+(*i)->getCreateStmts();
	ret+="\n\t}}";
	return ret;
}

string DataField::getDryParseStmts()
{
	return "dryAlloc(in->length-pos); pos=in->length;";
}

string DataField::getParseStmts()
{
	return
		"out->"+name+"Length=in->length-pos;\n" +
		"\tif(in->length-pos) {\n" +
		"\t\tout->"+name+"=alloc(in->length-pos);\n" +
		"\t\tmemcpy(out->"+name+", ((char *)in->data)+pos, in->length-pos);\n" +
		"\t} else\n" +
		"\t\tout->"+name+"=NULL;\n" +
		"\tpos=in->length;";
}

string DataField::getFieldDef()
{
	return "void *"+name+";\n\tuint "+name+"Length;";
}

string DataField::getCreateStmts()
{
	return "memcpy(((char *)out->data)+pos, msg->"+name+", msg->"+name+"Length); pos+=msg->"+name+"Length;";
}

string DataField::getDryCreateStmts()
{
	return "pos+=msg->"+name+"Length;";
}

StringField::StringField(xmlNodePtr nod): Field(nod)
{
	xmlChar *length=xmlGetProp(nod, (const xmlChar *)"length");
	xmlChar *term=xmlGetProp(nod, (const xmlChar *)"termination");
	
	if (length)
	{
		this->length=atoi((char *)length);
		this->lengthstr=string((char *)length);
		xmlFree(length);
	}
	else
		this->length=0;
	
	if (term && !length)
	{
		this->optionalTermination=xmlStrEqual(term, (const xmlChar *)"optional");
		xmlFree(term);
	}
	else
		this->optionalTermination=false;
}

string StringField::getFieldDef()
{
	return length?"char "+name+"["+lengthstr+"];" : "char *"+name+";";
}

string StringField::getParseStmts()
{
	if (length)
	{
		return 
			"req_bytes(in, &pos, "+lengthstr+");\n"
			"\tmemcpy(out->"+name+", ((char *)in->data)+pos, "+lengthstr+");\n"
			"\tpos+="+lengthstr+";";
	}
	else
	{
		return
		"{char *p=((char *)in->data)+pos;"
		"\n\tchar *begin=p;"
		"\n\tchar *end=((char *)in->data)+in->length;"
		"\n\twhile (*p && p < end) p++;" +
		(optionalTermination?
			string():
			string("\n\tif (p == end && *p) parse_error();")) +
		"\n\tout->"+name+"=(char *)alloc(p-begin+1);"
		"\n\tmemcpy(out->"+name+", begin, p-begin);"
		"\n\tout->"+name+"[p-begin]=0;"
		"\n\tpos+=p-begin+1;}";
	}
}

string StringField::getDryParseStmts()
{
	if (length)
	{
		return "req_bytes(in, &pos, "+lengthstr+"); pos+="+lengthstr+";";
	}
	else
	{
		string ret=
		"{"
			"char *p=((char *)in->data)+pos;"
			"char *begin=p;"
			"char *end=((char *)in->data)+in->length;"
			"while (*p && p<end) p++;";
			if (!optionalTermination)
				ret += "if (p==end && *p) parse_error();";
			ret+="pos+=p-begin+(*p?0:1);"
			"dryAlloc((p-begin+1));"
		"}";
		return ret;
	}
}

string StringField::getCreateStmts()
{
	if (length)
		return "memcpy(((char *)out->data)+pos, msg->"+name+", "+lengthstr+");"+getDryCreateStmts();
	else
		return "strcpy(((char *)out->data)+pos, msg->"+name+");"+getDryCreateStmts();
}

string StringField::getDryCreateStmts()
{
	if (length)
		return "pos+="+lengthstr+";";
	else
		return "pos+=strlen(msg->"+name+")+1;";
}

Field *Field::parseFieldNode(xmlNodePtr nod, IStruct *parent, Protocol *proto)
{
	if (!xmlStrncmp(nod->name, (const xmlChar *)"uint", 4) || 
		!xmlStrncmp(nod->name, (const xmlChar *)"int", 3))
	{
		const char *nname=(const char *)nod->name;
		int n=atoi(nname+(*nname=='u'?4:3));
		if (n == 64 || n==32 || n==16 || n==8)
			parent->fields.push_back(new SimpleField(nod));
	}
	else if (!xmlStrcmp(nod->name, (const xmlChar *)"string"))
	{
		parent->fields.push_back(new StringField(nod));
	}
	else if (!xmlStrcmp(nod->name, (const xmlChar *)"data"))
		parent->fields.push_back(new DataField(nod));
	else if (!xmlStrcmp(nod->name, (const xmlChar *)"array"))
	{
		ArrayField *af=new ArrayField(nod);
		parent->fields.push_back(af);
		proto->structs.push_back(af);
	}
}
