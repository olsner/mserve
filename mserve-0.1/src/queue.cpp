/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include "queue.h"
#include <queue>
#include <pthread.h>
#include <unistd.h>

using std::queue;

queue <qent> theQueue;
uint lastnumberassigned=0;
int isrunning=1;
pthread_mutex_t queueLock=PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t populateCondLock=PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t populateCond=PTHREAD_COND_INITIALIZER;

uint get_queuenumber()
{
	uint pid=getpid();
	if (lastnumberassigned & 0xff000000)
		lastnumberassigned=0;
	return (((pid&0xff)^((pid>>8)&0xff))<<24)+(lastnumberassigned++);
}

void queue_lock()
{
	pthread_mutex_lock(&queueLock);
}

void queue_unlock()
{
	pthread_mutex_unlock(&queueLock);
}

uint queue_push_unsec(const char *url)
{
	uint qn=get_queuenumber();
	theQueue.push((qent){strdup(url), qn});
	return qn;
}

uint queue_len()
{
	uint ret;
	ret=theQueue.size();
	return ret;
}

qent *queue_pop_unsec(qent *dst)
{
	qent &src=theQueue.front();
	dst->url=src.url;
	dst->number=src.number;
	theQueue.pop();
	return dst;
}

uint queue_push(const char *url)
{
	queue_lock();
	uint ret=queue_push_unsec(url);
	pthread_cond_broadcast(&populateCond);
	queue_unlock();
	return ret;
}

qent *queue_pop(qent *dst)
{
	queue_lock();
	qent *ret=queue_pop_unsec(dst);
	queue_unlock();
	return ret;
}

void queue_free_qent(qent *qe)
{
	free(qe->url);
}

void queue_wait()
{
	queue_lock();
	queue_wait_unsec();
	queue_unlock();
}

void queue_wait_unsec()
{
	// We only want to wait if the queue is unpopulated
	if (queue_len() && isrunning)
		return;
	while (!queue_len() || !isrunning)
	{
		queue_unlock();
		pthread_mutex_lock(&populateCondLock);
		pthread_cond_wait(&populateCond, &populateCondLock);
		pthread_mutex_unlock(&populateCondLock);
		queue_lock();
	}
}

int queue_isrunning()
{
	return isrunning;
}

void queue_stop()
{
	isrunning=0;
	pthread_mutex_lock(&populateCondLock);
	pthread_cond_broadcast(&populateCond);
	pthread_mutex_unlock(&populateCondLock);
}

void queue_resume()
{
	isrunning=1;
	pthread_mutex_lock(&populateCondLock);
	pthread_cond_broadcast(&populateCond);
	pthread_mutex_unlock(&populateCondLock);
}
