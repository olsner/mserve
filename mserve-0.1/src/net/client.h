/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _client_H
#define _client_H

#include <pthread.h>

#include "common.h"
#include "socketdispatch.h"
#include <libmserve/mserve.h>

enum _ClientDestroyReason
{
	CDR_CLIENT_EXIT,
	CDR_NET_ERROR,
	CDR_SECURITY,
	CDR_LAST
};
typedef enum _ClientDestroyReason ClientDestroyReason;

#define CLIENT_OUTBUFLEN 8192

typedef void (*ClientDeleteCallback) (client_ctx *, ClientDestroyReason);
typedef void (*ClientMessageCallback) (client_ctx *, const mserve_message *);

struct _client_ctx
{
	socket_ctx *socket;
	
	ClientDeleteCallback deleted;
	
	uint32 protocol_version;
	uint16 statusintervals[NUM_STATUS_TYPES];
	
	pthread_mutex_t outbuffer_mutex;
	uint8 outbuffer[CLIENT_OUTBUFLEN];
	uint outbufpos;
	
	msg_reader_ctx msg_reader;
	ClientMessageCallback message_cb;
};

BEGIN_DECLS

extern client_ctx *client_ctx_create(socket_ctx *);
extern socket_ctx *client_ctx_delete(client_ctx *, ClientDestroyReason);
/**
	Returns the number of bytes left in the client's sendbuffer
*/
extern uint client_ctx_get_sendbuffer_free(client_ctx *);
/**
	These two return 0 for success, -1 if data did not fit
*/
extern int client_ctx_buffer_message(client_ctx *, const mserve_message *);
extern int client_ctx_buffer_ext_message(client_ctx *, const ExtendedMessage *);

#ifdef _NETWORK_INTERNALS
extern void client_ctx_read_cb(socket_ctx *, SocketNotifyEvent);
extern void client_ctx_write_cb(socket_ctx *, SocketNotifyEvent);
extern void client_ctx_error_cb(socket_ctx *, SocketNotifyEvent);
extern int client_ctx_wants_write_cb(socket_ctx *);
extern void client_msg_reader_callback(msg_reader_ctx *, uint, uint, void *);
#endif

END_DECLS

#endif
