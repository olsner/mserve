/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _output_buffer_H
#define _output_buffer_H

#include "common.h"
#include "decode/decode.h"

BEGIN_DECLS

typedef struct _buffer_ctx
{
	char *buffer;
	char *bufferend;
	char *begin;
	char *end;
	uint length;
} buffer_ctx;

extern void buffer_init(buffer_ctx *, uint size);
extern void buffer_deinit(buffer_ctx *);
extern int buffer_push(buffer_ctx *, const char *buf, uint len);
extern int buffer_pop(buffer_ctx *, char *buf, uint len);
extern void buffer_clear(buffer_ctx *);
extern uint buffer_get_length(buffer_ctx *);
extern uint buffer_get_available(buffer_ctx *);

extern void out_buffer_init(uint size);
extern void out_buffer_deinit();
/**
	Push data to the end of the output buffer
	Returns the number of bytes that where pushed - it will only be less than
	the number of bytes specified if the buffer was filled
*/
extern int out_buffer_push(char *buffer, uint length);
/**
	Pop data from the beginning of the output buffer
	Returns the number of bytes successfully popped - it will only be less than
	the number of bytes specified if the buffer was emptied
*/
extern int out_buffer_pop(char *buffer, uint length);
/**
	Empty the buffer, discarding all data currently contained
*/
extern void out_buffer_clear();

/**
	Wait/sleep until the buffer has space for the specified number of bytes or
	the command/decode_status of the decode context has changed
*/
extern void out_buffer_wait_for_push(decode_ctx *ctx, uint available);
/**
	Wait/sleep until the buffer has been filled with the specified number of
	bytes or the command/decode_status of the decode context has changed
*/
extern void out_buffer_wait_for_pop(decode_ctx *ctx, uint length);

/**
	Return the number of bytes that are currently buffered.
	This is *not* the size set by out_buffer_init()
*/
extern uint out_buffer_length();
/**
	Return the number of bytes available for buffering. Use with out_buffer_push
*/
extern uint out_buffer_available();

END_DECLS

#endif
