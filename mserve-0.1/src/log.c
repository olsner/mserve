/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

#include "log.h"

pthread_mutex_t sympid_mutex=PTHREAD_MUTEX_INITIALIZER;
GTree *sympids;
int sympid_inited=0;

#define ensure_init() do { if (!sympid_inited) sympid_init(); } while (0)

gint sympid_compare_func(const void *a, const void *b, void *c)
{
	if ((int)a < (int)b)
		return -1;
	else if ((int)a > (int)b)
		return 1;
	else
		return 0;
}

void sympid_init()
{
	sympids=g_tree_new_full(sympid_compare_func, NULL, NULL, g_free);
	sympid_inited++;
}

char *getsympid(char *buf)
{
	pthread_mutex_lock(&sympid_mutex);
	ensure_init();
	char *val=g_tree_lookup(sympids, (gpointer)getpid());
	if (val)
	{
		strncpy(buf, val, 11);
		buf[11]=0;
	}
	else
	{
		snprintf(buf, 12, "%d", getpid());
	}
	pthread_mutex_unlock(&sympid_mutex);
	return buf;
}

void regsympid(const char *str)
{
	printf("[%d] regsympid(): Thread ID [%s]\n", getpid(), str);
	pthread_mutex_lock(&sympid_mutex);
	ensure_init();
	g_tree_insert(sympids, (gpointer)getpid(), g_strndup(str, 11));
	pthread_mutex_unlock(&sympid_mutex);
}
