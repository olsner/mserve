/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include "cfg.h"
#include "out_driver.h"
#include "buffer.h"
#include "decode/decode.h"

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include <sys/time.h>
#include <errno.h>

int OSS_initialize(const char *dev);
void OSS_deinit();
int OSS_set_format(int freq, SampleFormat fmt, int channels);
int OSS_output(decode_ctx *ctx, void *buffer, unsigned int size);
int OSS_get_preferred_chunk_size();
void OSS_set_volume(int volume);
int OSS_get_volume();

int loaded=0;
int dsp_fd=0;
int g_channels,g_freq,g_pitch;
uint bufsize;

//timeval lasttime={-1,-1};
//timeval thistime={-1,-1};
//int mixer_fd=0;

out_driver_info OSS_driver_info=
{
	"OSS",
	"OpenSoundSystem output driver for mServe - driver ver 0.1",
	OSS_initialize,
	OSS_deinit,
	OSS_set_format,
	OSS_output,
	OSS_get_preferred_chunk_size,
	OSS_set_volume,
	OSS_get_volume
};

out_driver_info * out_driver_get_info()
{
	return &OSS_driver_info;
}

int OSS_initialize(const char *dev)
{
	if (loaded)
		return -1;
	loaded=1;
	dsp_fd=open(dev?dev:"/dev/dsp", O_WRONLY);
	if (dsp_fd < 0)
	{
		perror("OSS_init, open dsp device");
		return -1;
	}
	/*long flags=fcntl(dsp_fd, F_GETFL);
	flags |= O_NONBLOCK;
	fcntl(dsp_fd, F_SETFL, flags);*/
	return 0;
}

void OSS_deinit()
{
	if (!loaded)
		return;
	loaded=0;
	close(dsp_fd);
	//close(mixer_fd);
}

int OSS_set_format(int freq, SampleFormat fmt, int channels)
{
	int stereo=0;
	if (channels == 2)
		stereo=2;
	else if (channels == 1)
		stereo=1;
	else
		return -1;
	
	// [16,8][be,le][us,s]
	int fmt_map[]={
		AFMT_U16_BE,
		AFMT_S16_BE,
		AFMT_U16_LE,
		AFMT_S16_LE,
		AFMT_U8,
		AFMT_S8,
		
	};
	
	log_printf("Setting: %s, %d Hz", stereo?"stereo":"mono", freq);
	
	ioctl(dsp_fd, SNDCTL_DSP_STEREO, &stereo);
	ioctl(dsp_fd, SNDCTL_DSP_SETFMT, &fmt_map[fmt]);
	ioctl(dsp_fd, SNDCTL_DSP_SPEED, &freq);

	log_printf("Was set: %s, %d Hz", stereo?"stereo":"mono", freq);
	
	g_channels=stereo?2:1;
	g_freq=freq;
	g_pitch=g_channels*(fmt<4?2:1);
	
	return 0;
}

int OSS_output(decode_ctx *dec_ctx, void *buf, unsigned int len)
{
	char *cbuf=(char *)buf;
	int wrt=0;
	while (len)
	{
		int res=write(dsp_fd, cbuf, len);
		if (res < 0)
		{
			return -1;
		}
		len-=res;
		cbuf+=res;
		wrt+=res;
	}
	dec_ctx->status0.OutputBytes+=wrt;
	dec_ctx->status0.OutputMillis+=(wrt*1000)/(g_pitch*44100);
	return wrt;
}

int OSS_get_preferred_chunk_size()
{
	ioctl(dsp_fd, SNDCTL_DSP_GETBLKSIZE, &bufsize);
	return bufsize;
}

void OSS_set_volume(int volume)
{}

int OSS_get_volume()
{
	return 0;
}
