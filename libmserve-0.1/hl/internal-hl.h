/*
 * This file is part of libmserve.
 * 
 * libmserve is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * libmserve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with libmserve; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _libmserve_internal_hl_H
#define _libmserve_internal_hl_H

#include <pthread.h>
#include <libmserve/mserve-hl.h>
#include <libmserve/attriblist.h>

/*template <class _T> struct status_value
{
	_T value;
	bool valid;
	int interval;
	timeval lastupdate;
	pthread_mutex_t mutex;
	
	inline status_value():
		//value(),
		valid(false),
		interval(0),
		lastupdate((timeval){0, 0}),
		mutex((pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER)
	{
	}
	
	inline int get_age()
	{
		timeval now;
		gettimeofday(&now, NULL);
		
		if (timerisset(&lastupdate))
		{
			return (now.tv_sec-lastupdate.tv_sec)*1000000+(now.tv_usec-lastupdate.tv_usec);
		}
		else
			return -1;
	}
	
	inline void lock()
	{
		pthread_mutex_lock(&mutex);
	}
	inline void unlock()
	{
		pthread_mutex_unlock(&mutex);
	}
};*/

struct mserve_hl
{
	mserve *ctx;
	char *client_name;
	uint client_version;
	
	int verbosity;
	
	PlaybackUpdateCallback playback_cb;
	MediaLengthUpdateCallback media_length_cb;
	URLUpdateCallback url_cb;
	QueueNumberUpdateCallback queue_number_cb;
	MediaInfoUpdateCallback media_info_cb;
	
	void *cb_data;
};

#endif
