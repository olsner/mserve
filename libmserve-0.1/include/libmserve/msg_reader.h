/*
 * This file is part of libmserve.
 * 
 * libmserve is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * libmserve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with libmserve; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#ifndef _libmserve_msg_reader_H
#define _libmserve_msg_reader_H

#include <libmserve/common.h>

struct _msg_reader_ctx;
typedef struct _msg_reader_ctx msg_reader_ctx;

typedef void (*msg_reader_cb) (msg_reader_ctx *, uint type, uint len, void *data);

struct _msg_reader_ctx
{
	msg_reader_cb msg_cb;
	void *userdata;

/****PRIVATE****/
	int readingdata;
	int readpos;
	uint msgbufferallocated;
	uint8 *msgbuffer;
	uint8 headerbuf[4];	
};

BEGIN_DECLS

extern void msg_reader_init(msg_reader_ctx *, msg_reader_cb callback, void *userdata);
extern void msg_reader_indata(msg_reader_ctx *, void *data, uint datalen);
extern void msg_reader_destroy(msg_reader_ctx *);

END_DECLS

#endif
