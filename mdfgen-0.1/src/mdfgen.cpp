/*
 * This file is part of mdfgen.
 * 
 * mdfgen is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mdfgen is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mdfgen; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Copyright (C) Simon Brenner 2003
 */

#include <libxml/parser.h>
#include <libxml/parserInternals.h>
#include <libxml/tree.h>
#include <popt.h>

#include <vector>
#include <string>
#include <fstream>
using namespace std;

#include <fields.hpp>
#include <istruct.hpp>
#include <protocol.hpp>

enum args
{
	ARG_VERSION_INFO=1024,
};

char *output_source="o.c";
char *output_header="o.h";
int verbose=0;

poptOption mdfgen_options[]={
	{"c-output", 'c', POPT_ARG_STRING, &output_source, 0, "Source output filename (def: o.cpp)", NULL},
	{"h-output", 'h', POPT_ARG_STRING, &output_header, 0, "Header output filename (def: o.h)", NULL},
	{"quiet", 'q', POPT_ARG_VAL, &verbose, 0, "Suppress most messages", NULL},
	{"verbose", 'v', POPT_ARG_VAL, &verbose, 1, "Don't suppress messages", NULL},
	{"version", 'V', POPT_ARG_NONE, NULL, ARG_VERSION_INFO, "Show version information", NULL},
	POPT_AUTOHELP
	POPT_TABLEEND
};

string safeGuardDef(const string &str)
{
	string ret="_"+str;
	for (int i=0;i<ret.size();i++)
	{
		switch (ret[i])
		{
			case '.':
			case '-':
			case '/':
				ret[i]='_';
		}
	}
	return ret;
}

int main(int argc, const char *argv[])
{
	poptContext pctx;
	int res;
	
	pctx=poptGetContext(NULL, argc, argv, mdfgen_options, 0);
	
	while ((res=poptGetNextOpt(pctx)) > 0)
	{
		switch (res)
		{
			case ARG_VERSION_INFO:
				fprintf(stderr, "mdfgen Version 1.00, Copyright (C) Simon Brenner 2003\n");
				break;
		}
	}
	
	if (res < -1)
	{
		fprintf(stderr, "%s: %s\n",
			poptBadOption(pctx, POPT_BADOPTION_NOALIAS),
			poptStrerror(res));
	}
	
	const char *input=poptGetArg(pctx);
	if (!input)
		return 0;
	
	xmlPedanticParserDefault(verbose);
	
	if (verbose)
	{
		fprintf(stderr, "mdfgen: Processing %s.\n", input);
		fprintf(stderr, "mdfgen: Going to generate %s and %s\n", output_source, output_header);
	}
	
	xmlDocPtr doc;
	xmlDtdPtr dtd;
	xmlNodePtr nod;
	xmlParserCtxtPtr parser;
	
	dtd=xmlParseDTD((const xmlChar *)"SYSTEM", (const xmlChar *)DTD_PATH);
	if (!dtd && verbose)
	{
		fprintf(stderr, "mdfgen: Warning: Failed parsing the DTD\n");
		fprintf(stderr, "mdfgen: Warning: DTD validation disabled\n");
	}
	
	parser=xmlCreateFileParserCtxt(input);
	
	res=xmlParseDocument(parser);
	if (res)
	{
		fprintf(stderr, "mdfgen: General Parse Failure [GPF]\n");
	}
	doc=parser->myDoc;
	if (doc == NULL)
	{
		fprintf(stderr, "mdfgen: General Parse Failure [GPF]\n");
		return 1;
	}
	
	if (dtd)
		res=xmlValidateDtd(&parser->vctxt, doc, dtd);
	else
		res=1;
	if (!res)
	{
		fprintf(stderr, "main(): Validation failed. Invalid document.\n");
		return 2;
	}
	
	nod=xmlDocGetRootElement(doc);
	
	if (nod == NULL)
	{
		fprintf(stderr, "Empty Document\n");
		xmlFreeDoc(doc);
		return 2;
	}
	
	if (xmlStrcmp(nod->name, (const xmlChar *)"mdf"))
	{
		fprintf(stderr, "Wrong Root Element, should be \"mdf\", was \"%s\"\n", nod->name);
		xmlFreeDoc(doc);
		return 2;
	}
	
	vector <Protocol *> protocols;
	
	xmlChar *typedefheader=xmlGetProp(nod, (const xmlChar *)"typedefheader");
	
	nod = nod->xmlChildrenNode;
	while (nod != NULL)
	{
		if (!xmlStrcmp(nod->name, (const xmlChar *)"protocol"))
		{
			protocols.push_back(new Protocol(nod, doc));
		}
		nod = nod->next;
	}
	
	ofstream out(output_header);
	string safeguard=safeGuardDef(output_header);
	out << "/* THIS IS A GENERATED FILE */\n";
	out << "#ifndef " << safeguard << '\n';
	out << "#define " << safeguard << '\n';
	out << "#include \"" << typedefheader << "\"\n";
	out << "#ifdef __cplusplus\nextern \"C\" {\n#endif\n";
	foreach (it, protocols)
	{
		(*it)->makeStructures(out);
		(*it)->makePublicDefines(out);
		(*it)->makeFunctionPrototypes(out);
	}
	out << "#ifdef __cplusplus\n};\n#endif\n";
	out << "#endif\n";
	out.close();
	
	out.open(output_source);
	out << "/* THIS IS A GENERATED FILE */\n";
	out << "#include \"" << output_header << "\"\n";
	out << "#include <stdlib.h>\n";
	out << "#include <string.h>\n";
	foreach (it, protocols)
	{
		(*it)->makePrivateDefines(out);
		(*it)->makeFunctions(out);
	}
	out.close();
	
	poptFreeContext(pctx);
}
