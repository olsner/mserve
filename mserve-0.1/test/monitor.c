/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <libmserve/common.h>
#include <libmserve/mserve.h>
#include <testbase.h>

mserve *ctx;
#define CLIENT_NAME "mServe_tests_monitor"
#define CLIENT_VERSION 1
Status0 status0;
int has_status0=0;
Status1 status1;
char *status1_basename=NULL;
int has_status1=0;
Status3 status3;
int has_status3=0;

#define STATUS_UPDATE_MASK 0xf
#define STATUS_UPDATE_INTERVAL 100

inline const char *pbs_str()
{
	char *pbs_strings[]={"--", ">>", "||"};
	return has_status0?pbs_strings[status0.PlaybackState]:"";
}

void handle_message(const mserve_message *msg)
{
	mserve_message tmp;
	switch (msg->type)
	{
		case MT_VersionInfo:
			mserve_handle_VersionInfo(ctx, msg, &(VersionInfo){CLIENT_NAME, CLIENT_VERSION, MSERVE_PROTOCOL_VERSION}, 1);
			break;
		case MT_UseProtocolVersion:
		{
			SetStatusUpdates ssu={STATUS_UPDATE_MASK, STATUS_UPDATE_INTERVAL};
			msg_create_SetStatusUpdates(&tmp, &ssu);
			mserve_send_message(ctx, &tmp);
			mserve_free_message(&tmp);
			break;
		}
		case MT_Status0:
		{
			if (has_status0)
				msg_free_Status0(&status0);
			else
				has_status0=1;
			msg_parse_Status0(&status0, msg);
			uint ib=status0.InputBytes;
			uint ims=status0.InputMillis;
			uint mb=has_status3?status3.MediaBytes:0;
			uint mms=has_status3?status3.MediaMillis:0;
			uint ob=status0.OutputBytes;
			uint oms=status0.OutputMillis;
			uint pbp=status0.PlaybackPosition;
			printf("\r%2s %s: %2u:%02u.%03u/%2u:%02u.%03u  I%2u:%02u.%03u (%u/%u)", 
				pbs_str(),
				has_status1?status1_basename:"",
				pbp/60000, (pbp/1000)%60, pbp%1000,
				mms/60000, (mms/1000)%60, mms%1000,
				ims/60000, (ims/1000)%60, ims%1000,
				//oms/60000, (oms/1000)%60, oms%1000,
				ib, mb/*, ob*/);
			fflush(stdout);
			break;
		}
		case MT_Status1:
		{
			Status1 nstatus1;
			msg_parse_Status1(&nstatus1, msg);
			if (!has_status1 || strcmp(nstatus1.FileName, status1.FileName))
			{
				printf("\n");
				if (has_status1)
					msg_free_Status1(&status1);
				else
					has_status1=1;
				memcpy(&status1, &nstatus1, sizeof(status1));
				status1_basename=strrchr(status1.FileName, '/');
				if (!status1_basename)
					status1_basename=status1.FileName;
				else
					++status1_basename;
			}
			else
				msg_free_Status1(&nstatus1);
			break;
		}
		case MT_Status3:
		{
			if (has_status3)
				msg_free_Status3(&status3);
			else
				has_status3=1;
			msg_parse_Status3(&status3, msg);
			break;
		}
		case MT_SetMediaInfo:
		{
			SetMediaInfo smi;
			uint i;
			
			msg_parse_SetMediaInfo(&smi, msg);
			//printf("SetMediaInfo used/alloced: %u/%u\n", smi.allocUsed, smi.allocated);
			for (i=0;i<smi.ValuesCount;i++)
			{
				printf("Media info %s: \"%s\"\n", smi.Values[i].Name, smi.Values[i].Value);
			}
			msg_free_SetMediaInfo(&smi);
			break;
		}
		default:
		{
			printf("Unhandled message: 0x%04x length %d\n", msg->type, msg->length);
			break;
		}
	}
	fflush(stdout);
}

int main()
{
	int res;
	mserve_message msg;
	
	printf("[Monitor] started\n");
	
	socket_init(&ctx, handle_message);
	
	msg_create_GetAllMediaInfo(&msg);
	x_send_message(ctx, &msg);
	mserve_free_message(&msg);
	
	while (mserve_wait_pop_message(ctx, &msg)==MSERVE_SUCCESS)
	{
		handle_message(&msg);
		mserve_free_message(&msg);
	}
	perror("mserve_wait_pop_message");
}
