/*
 * This file is part of libmserve.
 * 
 * libmserve is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * libmserve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with libmserve; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include <libmserve/attriblist.h>
#include <glib.h>
#include <pthread.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define LOCK(_objptr) pthread_mutex_lock(&(_objptr->mutex))
#define UNLOCK(_objptr) pthread_mutex_unlock(&(_objptr->mutex))

attriblist *attriblist_init(attriblist *ptr)
{
	ptr->table=g_hash_table_new_full(g_str_hash, g_str_equal, free, free);
	pthread_mutex_init(&ptr->mutex, NULL);
	ptr->set_callback=NULL;
	ptr->set_callback_data=NULL;
	return ptr;
}

void attriblist_destroy(attriblist *ptr)
{
	LOCK(ptr);
	g_hash_table_destroy(ptr->table);
	UNLOCK(ptr);
	pthread_mutex_destroy(&(ptr->mutex));
	ptr->table=NULL;
}

void attriblist_put(attriblist *thiz, const char *name, const char *value)
{
	LOCK(thiz);
	g_hash_table_insert(thiz->table, strdup(name), strdup(value));
	UNLOCK(thiz);
	if (thiz->set_callback)
	{
		thiz->set_callback(thiz, thiz->set_callback_data, name, value);
	}
}

int attriblist_remove(attriblist *thiz, const char *name)
{
	LOCK(thiz);
	int ret=g_hash_table_remove(thiz->table, name);
	UNLOCK(thiz);
	return ret;
}

char *attriblist_get_dup(attriblist *thiz, const char *name)
{
	LOCK(thiz);
	char *ret=(char *)g_hash_table_lookup(thiz->table, name);
	UNLOCK(thiz);
	return ret?strdup(ret):ret;
}

void attriblist_set_callback(attriblist *thiz, attriblist_callback cb, void *data)
{
	LOCK(thiz);
	thiz->set_callback=cb;
	thiz->set_callback_data=data;
	UNLOCK(thiz);
}

struct foreach_data
{
	attriblist *thiz;
	attriblist_callback cb;
	void *data;
};

void attriblist_foreach_cb(void *key, void *value, void *data)
{
	struct foreach_data *fdata=(struct foreach_data *)data;
	fdata->cb(fdata->thiz, fdata->data, (char *)key, (char *)value);
}

void attriblist_foreach(attriblist *thiz, attriblist_callback cb, void *data)
{
	struct foreach_data fdata={thiz, cb, data};
	LOCK(thiz);
	g_hash_table_foreach(thiz->table, attriblist_foreach_cb, &fdata);
	UNLOCK(thiz);
}
