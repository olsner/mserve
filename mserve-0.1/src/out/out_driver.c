/*
 * This file is part of mServe.
 * 
 * mServe is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * mServe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mServe; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Copyright (C) Simon Brenner 2003
 */

#include "out_driver.h"
#include "cfg.h"
#include "buffer.h"
#include "decode/decode.h"

#include <dlfcn.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <unistd.h>

#include <string.h>
#include <stdio.h>

//#include <map>
//using namespace std;

void out_driver_init()
{
	cfg_set_default("out_driver_dir", DEF_OUTPUT_DIR);
	// Index the output driver directory? Naah =)
}

out_driver_info *get_out_driver(const char *name)
{
	// The pointer is not null, so that cfg_get_string will not auto-alloc mem.
	uint namelen;
	char *t=strchr(name, ':');
	if (t == NULL)
		namelen=strlen(name);
	else
		namelen=t-name;
	int ret=cfg_get_string("out_driver_dir", NULL, 0);
	// null+slash+"lib"+base".so" = 8 extra
	uint bufsize=namelen+ret+8;
	char *so_fname=(char *)alloca(bufsize);
	ret=cfg_get_string("out_driver_dir", so_fname, bufsize);
	
	// so_fname should contain "<out_driver_dir>[/]<name>.so"
	char *ptr=so_fname+ret;
	if (*(ptr-1) != '/')
		*ptr++='/';
	*ptr++='l';
	*ptr++='i';
	*ptr++='b';
	memcpy(ptr, name, namelen);
	memcpy(ptr+namelen, ".so", 4);
	
	void *dl_ctx=dlopen(so_fname, RTLD_NOW);
	if (dl_ctx == NULL)
	{
		log_printf("dlopen(%s) failed: %s", so_fname, dlerror());
		return NULL;
	}
	out_driver_get_info_func *out_driver_get_info=(out_driver_get_info_func *)dlsym(dl_ctx, "out_driver_get_info");
	if (out_driver_get_info==NULL)
	{
		log_printf("Driver (%s) failed (dlsym): %s", name, dlerror());
		return NULL;
	}	
	
	return out_driver_get_info();
}

pthread_t buffered_output_thread;
//int buf_thread_pid;

void *buffered_main(void *data)
{
	regsympid("OUT");
	log_print("Output thread");
	decode_ctx *ctx=(decode_ctx *)data;
	int res=out_driver_run(ctx);
	log_printf("out_driver_run: %d\n", res);
	return NULL;
}

void out_driver_run_thread(decode_ctx *ctx)
{
	pthread_create(&buffered_output_thread, NULL, buffered_main, ctx);
}

void out_driver_join_thread()
{
	pthread_join(buffered_output_thread, NULL);
}

int out_driver_run(decode_ctx *dec_ctx)
{
	uint8 *buffer;
	int cnt;
	
	int chunk_size=dec_ctx->output->get_preferred_chunk_size();
	if (!chunk_size) chunk_size=512;
	buffer=(uint8*)alloca(chunk_size);

	out_buffer_wait_for_pop(dec_ctx, LOWWATER_MARK);
	while (dec_ctx->state != DS_Complete)
	{
		switch (dec_ctx->state)
		{
			case DS_Paused:
				decode_ctx_wait_event(dec_ctx);
				continue;
			case DS_DecodeComplete:
				/* Decode Complete and Empty Output Buffer => end output loop */
				if (!out_buffer_length())
				{
					dec_ctx->state=DS_Complete;
					decode_ctx_signal_event(dec_ctx);
					continue;
				}
				cnt=out_buffer_pop(buffer, chunk_size);
				decode_ctx_signal_event(dec_ctx);
				cnt=dec_ctx->output->output(dec_ctx, buffer, cnt);
				if (cnt == -1)
				{
					dec_ctx->state=DS_Complete;
					dec_ctx->error=DE_Output;
					decode_ctx_signal_event(dec_ctx);
				}
				continue;
			case DS_Running:
				cnt=out_buffer_pop(buffer, chunk_size);
				decode_ctx_signal_event(dec_ctx);
				cnt=dec_ctx->output->output(dec_ctx, buffer, cnt);
				if (cnt == -1)
				{
					dec_ctx->state=DS_Complete;
					dec_ctx->error=DE_Output;
					decode_ctx_signal_event(dec_ctx);
					continue;
				}
			// Unhandled enum value warning
			default:;
		}
		out_buffer_wait_for_pop(dec_ctx, chunk_size);
	}
	decode_ctx_signal_event(dec_ctx);
	if (dec_ctx->error == DE_Output)
		return -1;
	else
		return 0;
}
